
var version = "2.0";
var isWorkingThread=false;
var uiSettings = {
  "signUp":false,
  "estyTabId":'',
  "searchOverAction":false,
  "estySearchCount":0,
  "estyDefaultCount":10,
  "estyLicenceStatus":!1,
  "trendsKeywords":'',
  "currentLoginId":0,
  "loggedInUsers":[],
  "etsy_user_credential":{
    "etsy_access_token":'',
    "etsy_access_secret":'',
    "loginemail":'',
    "searchOverAction":false,
  }
};

E = '',
OBJECT = 'object',
STRING = 'string',
FUNCTION = 'function',
PROTOCOL_HTTPS = 'https',
PROTOCOL_HTTP = 'http',
HTTPS_PORT = '443',
HTTP_PORT = '80',
PATH_SEPARATOR = '/',
PATH_VARIABLE_IDENTIFIER = ':',
PORT_SEPARATOR = ':',
DOMAIN_SEPARATOR = '.',
PROTOCOL_SEPARATOR = '://',
AUTH_SEPARATOR = ':',
AUTH_CREDENTIALS_SEPARATOR = '@',
QUERY_SEPARATOR = '?',
SEARCH_SEPARATOR = '#',
DEFAULT_PROTOCOL = PROTOCOL_HTTP + PROTOCOL_SEPARATOR,
MATCH_1 = '$1',
regexes = {
    trimPath: /^\/((.+))$/,
    splitDomain: /\.(?![^{]*\}{2})/g
},


chrome.runtime.onInstalled.addListener(async function (details){
	if(details.reason == "install"){
		setTimeout(function () {
			chrome.tabs.query({ url: "https://www.etsy.com/*" }, function (tabs) {
				$.each(tabs, function (index, tab) {
					chrome.tabs.reload(tab.id);
				});
			});
		}, 500);

    await storageSet({key: 'userCurrency', value: 'USD'});

	} else if(details.reason == "update"){
		setTimeout(function () {
			chrome.tabs.query({ url: "https://www.etsy.com/*" }, function (tabs) {
				$.each(tabs, function (index, tab) {
					chrome.tabs.reload(tab.id);
				});
			});
		}, 500);
	}
});

var controller = new AbortController();
var signal = controller.signal;

function abortEtsyApi() {
  controller.abort();
};

async function callEtsyApi(message, sender, sendResponse) {
  var keyword = message.data;

  let result = await request();

  console.log(result);

  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve) => {
      $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}CallEtsyApi`,
              dataType: "json",
              data: JSON.stringify({
                Keyword: keyword,
              }),
              success: (result) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });

  }
}

async function callEtsyApi2(message, sender, sendResponse) {
  var keyword = message.data;

  let result = await request();

  console.log(result);

  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve) => {
        $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}CallEtsyApi2`,
              dataType: "json",
              data: JSON.stringify({
                Keyword: keyword,
              }),
              success: (result) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
          // fetch(`${config.base_url}CallEtsyApi2`, {
          //   data: JSON.stringify({
          //             Keyword: keyword,
          //           }),},{signal}).then((response) => {
          //     return response.json();
          //   }).then((apiResponse) => {
          //     resolve(apiResponse);
          //   }).catch(function(e) {
          //     console.log('request stopped: ' + e.message);
          //   });

      });
  }
}


// function callEtsyApi2(keyword, sender) {
//   return new Promise((resolve) => {
//     fetch(`https://openapi.etsy.com/v2/listings/active?api_key=${consts.api_key}&limit=100&keywords=${keyword}&sort_on=score&region=US`).then((response) => {
//       return response.json();
//     }).then((apiResponse) => {
//       resolve({data : apiResponse});
//       chrome.tabs.sendMessage(sender.tab.id, new ExtensionMessage('callEtsyApi2', apiResponse))
//       console.log('from bg', apiResponse);
//     }).catch(function(e) {
//       console.log('request stopped: ' + e.message);
//     });
//   });
// };

var myApp = {
  onExtMessage: function(message, sender, sendResponse){
    myApp.message = message;
    switch (message.command) {
      case config.keys.callEtsyApi:
        callEtsyApi(message, sender, sendResponse);
        break;
      case 'callEtsyApi2':
        callEtsyApi2(message, sender, sendResponse);
        break;
      case config.keys.signUp:
        signUp(message, sender, sendResponse);
        break;
      case config.keys.login:
        login(message, sender, sendResponse);
        break;  
      case config.keys.saveTokens:
        saveTokens(message, sender, sendResponse);
        break;
      case config.keys.saveFavorite:
        saveFavorite(message, sender, sendResponse);
        break;
      case config.keys.removeFavorite:
        removeFavorite(message, sender, sendResponse);
        break;
      case config.keys.addNewFolder:
        addNewFolder(message, sender, sendResponse);
        break;
      case config.keys.checkInFav:
        checkInFav(message, sender, sendResponse);
        break;
      case config.keys.checkInFolders:
        checkInFolders(message, sender, sendResponse);
        break;
      case config.keys.deleteFavFolder:
        deleteFavFolder(message, sender, sendResponse);
        break;
      case "addListingsToFavFolder":
        addListingsToFavFolder(message, sender, sendResponse);
        break;
      case "getFolderListings":
        getFolderListings(message, sender, sendResponse);
        break;
      case "getAllFavListings":
        getAllFavListings(message, sender, sendResponse);
        break;
      case "checkSubscription":
        checkSubscription(message.data, sender, sendResponse);
        break;
      case "checkoutPage":
        checkoutPage(message.data, sender, sendResponse);
        break;
      case "cancelSubscription":
        cancelSubscription(message.data, sender, sendResponse);
      // case "getUSDRate":
      //   getUSDRate(sendResponse);
      //   break;
      case "saveUISettings":
        uiSettings = message.data;
        myApp.saveUISettings(message.data, sender, sendResponse);
        break;
      case config.keys.getFavoritesIdsFromServer:
        console.log(message.data.userEmail);
        getFavoritesIdsFromServer(message, sender, sendResponse);
        break;
      case "closeIframePopup":
        myApp.closeIframePopup(message.data, sender, sendResponse);
        break;
      case "openPremiumEverBeeTab":
        myApp.openPremiumEverBeeTab(message.data, sender, sendResponse);
        break;
      case "openKeywordResearch":
        myApp.openKeywordResearch(message.data, sender, sendResponse);
        break;
      case "openProductpageUrl":
        myApp.openProductpageUrl(message.data, sender, sendResponse);
        break;
      case "injectMemberStack":
        myApp.injectMemberStack(message.data, sender, sendResponse);
        break;
      case config.keys.getSettingsDB:
        myApp.getSettingsDB(message.data, sender, sendResponse);
        break;
      case "callEtsyOpenApi":
        console.log('from bg', message.data, sender, sendResponse);
        callEtsyOpenApi(message.data, sender, sendResponse);
        break;
      case "postSaveEverBeeProccessLogsToFile":
        myApp.postSaveEverBeeProccessLogsToFile(message.log_body,message.user_email);
        break;
      case "connectEtsyTab":
        myApp.connectEtsyTab(message.data, sender, sendResponse);
        break;
      case config.keys.openUpgradeModalFromPopover:
        sendMessage(sender.tab.id, message);
        break;
      case "checkMainPageAction":
        myApp.checkMainPageAction(message.data, sender, sendResponse);
        break;   
      case "goEverBee":
        chrome.tabs.create(
          { url: 'https://www.everbee.io/' }
        );   
        break;
      case 'getEmail':
        console.log('getEmail');
        getEmail(message);
        break;
      case "getSettings":
        if(message.callback == "yes") {
          sendResponse({
            "uiSettings" : uiSettings
          });
        } else {
          sendMessage(sender, {
            "command": "rec_getSettings",
            "data" : {
              "uiSettings" : uiSettings
            }
          });
        }
        break;
    }
    switch (message.context) {
      // case config.keys.callEtsyApi:
      //   callEtsyApi(message, sender, sendResponse);
      //   break;
      // case 'callEtsyApi2':
      //   console.log('callEtsyApi2')
      //   callEtsyApi2(message.data, sender);
      //   break;
      case 'abortEtsyApi':
        console.log('abortEtsyApi')
        abortEtsyApi();
        break;

    }
    return true;
  },
  load:function(){
    myApp.initStorage();

  },
  initStorage: function(_sender){
    var storedVersion = localStorage["version"];
    if(storedVersion != version){
      localStorage["version"]      = version;
      localStorage['uiSettings']   = JSON.stringify(uiSettings);
    }
    uiSettings  = JSON.parse(localStorage["uiSettings"]);
  },
  saveUISettings : function(data, _sender, sendResponse){
    localStorage["uiSettings"] = JSON.stringify(uiSettings);
    if(typeof(sendResponse)=="function") {
      sendResponse({uiSettings:uiSettings});
    }
  },
  getSettingsDB:function(data, sender, sendResponse){
    chrome.tabs.sendMessage(sender.tab.id, { command: config.keys.getSettingsDB},function (res){
      sendResponse(res)

    });

  },
  openProductpageUrl:function(productUrl){
    chrome.tabs.create({ url: productUrl });
  },
  openPremiumEverBeeTab:function(){
    var action_url = "#";
    chrome.tabs.create({ url: action_url });
  },
  openKeywordResearch:function(data, _sender){
    console.log('almost here')
    console.log(_sender);
    chrome.tabs.sendMessage(_sender.tab.id, { command: config.keys.openResearchKeyword, data: data});
  },
  closeIframePopup : function(message,sender, sendResponse){
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, { command: 'closeIframePopup',data:message});
    });

  },
  checkMainPageAction:function(data, sender, sendResponse){
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      var obj={};
      obj['estyTabId']=tabs[0].id;
      obj['estyTabUrl']=tabs[0].url;
      sendResponse(obj);
    });
  },
  connectEtsyTab:function(){
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      var estyTabId=tabs[0].id;
      uiSettings['estyTabId'] = estyTabId;
      myApp.saveUISettings();
      var authUrl = "https://proud-luxurious-wandflower.glitch.me/LoginEtsy";
      chrome.tabs.create({ url: authUrl });
    });


  },
//   callEtsyOpenApi:function(request_data, sender, sendResponse) {
//     $.ajax({
//      url: request_data.url,
//      type: request_data.method,
//      data: request_data.data,
//    }).done((apiResponse) => {
//      sendResponse(apiResponse.results[0]);
//      console.log('apiResponse >> ', apiResponse);
//    });
// },
 getMembershipDetail:function(){
  var form = new FormData();
  var settings = {
    "url": "https://api.memberstack.io/member/get-profile",
    "method": "POST",
    "timeout": 0,
    "headers": {
      "Authorization": uiSettings.authorization
    },
    "processData": false,
    "mimeType": "multipart/form-data",
    "contentType": false,
    "data": form
  };

  $.ajax(settings).done(function (response) {
    var responseData=JSON.parse(response);
    if (responseData.membershipDetails != undefined) {
     var successRedirect = responseData.membershipDetails.successRedirect;
     var newUrl="#"+successRedirect;
     myApp.redirectUserAfterUpgrade(newUrl);

   }
 });
},
 // Save user Process Logs To File on the server
 postSaveEverBeeProccessLogsToFile : function(log_body,user_email) {

  var log_time = getNowLogTime();
  var log_body_header = 'User Email : '+ user_email + "\n\n";
  log_body_header += 'PC time : '+ log_time;
  log_body_header += "\n";
  log_body_header += 'Version : '+ chrome.runtime.getManifest().version;

  log_body += 'PC time : '+ log_time;
  log_body += "\n";
  log_body += 'Version : '+ chrome.runtime.getManifest().version;
},
 injectMemberStack:function(){/*
   chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    var srcA="https://api.memberstack.io/static/memberstack.js?custom";
    chrome.tabs.executeScript(tabs[0].id, {code: "(function(){var sc=document.createElement('script');sc.src='"+srcA+"';sc.setAttribute('data-memberstack-id','aaf3b8a7beb500ea6d0e051f66746b05');document.body.appendChild(sc);})();"}, function(){

    });
  });

*/}
};

async function callEtsyOpenApi(message, sender, sendResponse) {
    console.log('message', message);

    const userCurrency = await storageGet('userCurrency');

    console.log('userCurrency', userCurrency)

    let result = await request();
  
    console.log('result', result);
  
    if (result.success) {
      sendResponse({status: 'good', result: result});
    } else {
      sendResponse({status: 'danger', result: result});
    }
  
    async function request() {
      return new Promise((resolve) => {
      $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}CallEtsyOpenApi`,
              dataType: "json",
              data: JSON.stringify({
                Token: message.token,
                Listing_id: message.listing_id,
                UserCurrency: userCurrency
              }),
              success: (result) => {
                  console.log(result.data);
                  resolve(result.data);
              },
              error: (result) => {
                  console.log(result.data);
                  resolve(result.data);
              },
          });
      });
    }
}


async function getEmail(message, sender, sendResponse) {
  var dataObj = message.data;
  let result = await request();

  console.log(result);

  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve) => {
      $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}GetEmail`,
              dataType: "json",
              data: JSON.stringify({
                DataObj: dataObj,
              }),
              success: (result) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
}

// function getEmail(dataObj) {
//   console.log(dataObj);
//   return new Promise((resolve) => {
//     let {keys, oauth_data} = dataObj;
//     let myHeaders = {

//       'Accept': '*/*',
//     }

//     var requestOptions = {
//       method: 'GET',
//       headers: myHeaders,
//       redirect: 'follow',	
//     };
    

//     fetch(`https://openapi.etsy.com/v2/users/__SELF__?oauth_consumer_key=${oauth_data.oauth_consumer_key}&oauth_token=${keys.key}&oauth_signature_method=${oauth_data.oauth_signature_method}&oauth_timestamp=${oauth_data.oauth_timestamp}&oauth_nonce=${oauth_data.oauth_nonce}&oauth_version=${oauth_data.oauth_version}&oauth_signature=${oauth_data.oauth_signature}`, requestOptions)
//     .then(response => response.text())
//     .then((result) => {
//       resolve({data: result})
//       console.log('from getEmail', result);
//     })
//     .catch((error) => {
//       console.log('error', error);
//     });

//   }) 
// }

function getNowLogTime() {
  var hour = new Date().getHours();
  var timeAmPm = hour >= 12 ? 'pm' : 'am';
  hour = hour % 12;
  hour = hour ? hour : 12; // the hour '0' should be '12'
  return pad(new Date().getDate()) + '-' + pad(new Date().getMonth() + 1) + '-' + new Date().getUTCFullYear() + ' ' + pad(hour) + ':' + pad(new Date().getMinutes()) + ':' + pad(new Date().getSeconds()) + ' ' + timeAmPm;
}

function pad(number) {
  if (number < 10) {
    return '0' + number;
  }
  return number;
}

chrome.runtime.onMessage.addListener(myApp.onExtMessage);

myApp.load();

function sendMessage1(msg, callbackfn) {
  if(callbackfn!=null) {
    callback.push(callbackfn);
    msg.callback = "yes";
  }
  chrome.runtime.sendMessage(msg,callbackfn);
}

function sendMessage(tabId, msg){
  console.log(msg);
  if(tabId) chrome.tabs.sendMessage(tabId, msg);
  else chrome.runtime.sendMessage(sender.id, msg);
}

function sendAjax(config) {
  $.ajax(config);
};


chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.sendMessage(tab.id, { 'command': config.keys.getHeadlines });
});

chrome.tabs.onActivated.addListener(function (info) {
  chrome.tabs.get(info.tabId, function (tab) {
    var current_url = tab.url;
    var icon_disable = 'images/logo1.png';
    var icon_enable = 'images/logo1.png';
    if (current_url != undefined && current_url.indexOf('www.etsy.com') !== -1) {
      chrome.browserAction.setIcon({ path: icon_enable });
    } else {
      chrome.browserAction.setIcon({ path: icon_disable });
      chrome.browserAction.setPopup({
        tabId: tab.id,
        popup: 'websiteError.html'
      });
    }
  });
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  var current_url = tab.url
  var icon_disable = 'images/logo1.png';
  var icon_enable = 'images/logo1.png';

  if (current_url!=undefined && current_url.indexOf('www.etsy.com') !== -1) {
    chrome.browserAction.setIcon({ path: icon_enable });
  } else {
    chrome.browserAction.setIcon({ path: icon_disable });
    chrome.browserAction.setPopup({
      tabId: tabId,
      popup: 'websiteError.html'
    });
  }
});

chrome.webRequest.onBeforeSendHeaders.addListener(
  function(details) {
    //console.log(details)
    if (details.method == "GET" && details.url.indexOf('/app/extension')!==-1 ) {
      chrome.tabs.update(details.tabId,{url:"https://www.etsy.com"});
    }
    for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === "Authorization") {
        uiSettings.authorization = details.requestHeaders[i].value;
        myApp.saveUISettings();
      }
    }

  },
  {urls: ["https://www.etsy.com/*","https://api.memberstack.io/*"]},
  ["requestHeaders","extraHeaders"]);


// web Request handle on Completed
chrome.webRequest.onCompleted.addListener(
  function(details){
    if (details.method == "POST" && details.statusCode == 200 && details.url.indexOf('change-membership')!==-1 ) {
     myApp.getMembershipDetail();
   }
 },
 {urls:["https://api.memberstack.io/member/change-membership"]
},
[]
);


chrome.webRequest.onBeforeRequest.addListener(
  function(details) {
    try{
      if(details.url.includes("login")){
        var obj = JSON.parse(decodeURIComponent(String.fromCharCode.apply(null,
          new Uint8Array(details.requestBody.raw[0].bytes))));
          console.log(obj);
          console.log(details);
        }
      }
      catch (ex){
        //console.log(ex)
      }
  },
  {urls: ["https://api.memberstack.io/*"]},
  ['requestBody']);

  async function signUp(message, sender, sendResponse) {
    var data = message.data;
    console.log(data);

    let result = await request();
    if (result.success) {
      sendResponse({status: 'good', result: JSON.parse(result)});
    } else {
      sendResponse({status: 'danger', result: JSON.parse(result)});
    }

    async function request() {

        return new Promise((resolve, reject) => {
            $.ajax({
                type: "POST",
                traditional: true,
                contentType: "application/json; charset=utf-8",
                url: `${config.base_url}User/Register`,
                dataType: "json",
                data: JSON.stringify({
                  Name: data.name,
                  Email: data.email,
                  Password: data.password
                }),
                success: (result, textStatus, xhr) => {
                    console.log(result);
                    resolve(result);
                },
                error: (result) => {
                    console.log(result);
                    resolve(result);
                },
            });
        });
    }
}


async function login(message, sender, sendResponse){
  var data = message.data.oauth_data;
  console.log(data);

  let result = await request();
  if (result.success) {
    sendResponse({status: 'good', result: JSON.parse(result)});
  } else {
    sendResponse({status: 'danger', result: JSON.parse(result)});
  }

  async function request() {

      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}User/Login`,
              dataType: "json",
              data: JSON.stringify(data),
              success: (result) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
}

async function saveTokens(message, sender, sendResponse){
  console.log(message);
  var email = message.data.email;
  var token = message.data.token;
  var secret = message.data.secret;

  let result = await request();
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}User/SaveTokens`,
              dataType: "json",
              data: JSON.stringify({
                Email: email,
                Token: token,
                AccessSecret: secret
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
}

async function saveFavorite(message, sender, sendResponse){

  console.log(message);
  var listingId = message.data.listingId;
  var userEmail = message.data.userEmail;
  var currencyCode = message.data.currencyCode;
  var category = message.data.category;

  let result = JSON.parse(await request());

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/SaveFavorite`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
                Caregory: category,
                CurrencyCode: currencyCode,
                ListingId: listingId
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
}

async function removeFavorite(message, sender, sendResponse){

  console.log(message);
  var listingId = message.data.listingId;
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/RemoveFavorite`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
                ListingId: listingId
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function addNewFolder(message, sender, sendResponse) {
  console.log(message);

  var folderName = message.data.folderName;
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());
  
  console.log(result);
  
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: `${config.base_url}Favorite/AddNewFolder`,
            dataType: "json",
            data: JSON.stringify({
              UserEmail: userEmail,
              FolderName: folderName
            }),
            success: (result, textStatus, xhr) => {
                console.log(result);
                resolve(result);
            },
            error: (result) => {
                console.log(result);
                resolve(result);
            },
        });
    });
  }
}

async function checkInFav(message, sender, sendResponse){
  var listingId = message.data.listingId;
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: `${config.base_url}Favorite/CheckFavorite`,
            dataType: "json",
            data: JSON.stringify({
              UserEmail: userEmail,
              ListingId: listingId
            }),
            success: (result, textStatus, xhr) => {
                console.log(result);
                resolve(result);
            },
            error: (result) => {
                console.log(result);
                resolve(result);
            },
        });
    });
  }
}

async function checkInFolders(message, sender, sendResponse) {
  console.log(message);
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());

  console.log(result);

  if (result.success) {
    sendResponse({status: 'good', result: result.data});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: `${config.base_url}Favorite/CheckFolders`,
            dataType: "json",
            data: JSON.stringify({
              UserEmail: userEmail,
            }),
            success: (result, textStatus, xhr) => {
                console.log(result);
                resolve(result);
            },
            error: (result) => {
                console.log(result);
                resolve(result);
            },
        });
    });
  }
};

async function deleteFavFolder(message, sender, sendResponse){

  console.log(message);
  var folderName = message.data.folderName.trim();
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/DeleteFavFolder`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
                FolderName: folderName
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function addListingsToFavFolder(message, sender, sendResponse){

  console.log(message);
  var folderId = message.data.folderId;
  var userEmail = message.data.userEmail;
  var arrOfListingsId = message.data.arrOfListingsId;

  let result = JSON.parse(await request());

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/AddListingsToFavFolder`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
                FolderId: folderId,
                ArrOfListingsId: arrOfListingsId,

              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function getFolderListings(message, sender, sendResponse){

  console.log(message);
  var folderName = message.data.folderName;
  var userEmail = message.data.userEmail;

  let result = JSON.parse(await request());

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/GetFolderListings`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
                FolderName: folderName,
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function getAllFavListings(message, sender, sendResponse){

  console.log(message);
  var userEmail = message.data.userEmail;
  let result = JSON.parse(await request());

  console.log(result);

  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Favorite/GetAllFavListings`,
              dataType: "json",
              data: JSON.stringify({
                UserEmail: userEmail,
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function getFavoritesIdsFromServer(message, sender, sendResponse) {
  var userEmail = message.data.userEmail;
  var folderName = message.data.folderName;

  let result = await request();

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {

        if (folderName) {
          $.ajax({
            type: "POST",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: `${config.base_url}Favorite/GetFavoritesIdsFromServer`,
            dataType: "json",
            data: JSON.stringify({
              UserEmail: userEmail,
              FolderName: folderName,
            }),
            success: (result, textStatus, xhr) => {
                console.log(result);
                resolve(result);
            },
            error: (result) => {
                console.log(result);
                resolve(result);
            },
        });

        } else {
          $.ajax({
            type: "POST",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: `${config.base_url}Favorite/GetFavoritesIdsFromServer`,
            dataType: "json",
            data: JSON.stringify({
              UserEmail: userEmail,
            }),
            success: (result, textStatus, xhr) => {
                console.log(result);
                resolve(result);
            },
            error: (result) => {
                console.log(result);
                resolve(result);
            },
        });
        }
      });
  }
};

async function checkoutPage(data, sender, sendResponse) {
  var email = data.userEmail;
  var planName = 'simple plan';
  var interval = 'month';

  let result = await request();

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Stripe/CheckoutPage`,
              dataType: "json",
              data: JSON.stringify({
                email: email,
                planName: planName,
                interval: interval,
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function checkSubscription(data, sender, sendResponse) {
  console.log(data);
  var email = data.userEmail;
  var planName = 'simple plan';

  let result = await request();

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Stripe/CheckSubscription`,
              dataType: "json",
              data: JSON.stringify({
                email: email,
                planName: planName,
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

async function cancelSubscription(data, sender, sendResponse) {
  console.log(data);
  var email = data.userEmail;
  var planName = 'simple plan';

  let result = await request();

  console.log(result);
  if (result.success) {
    sendResponse({status: 'good', result: result});
  } else {
    sendResponse({status: 'danger', result: result});
  }

  async function request() {
      return new Promise((resolve, reject) => {
          $.ajax({
              type: "POST",
              traditional: true,
              contentType: "application/json; charset=utf-8",
              url: `${config.base_url}Stripe/CancelSubscription`,
              dataType: "json",
              data: JSON.stringify({
                email: email,
                planName: planName,
              }),
              success: (result, textStatus, xhr) => {
                  console.log(result);
                  resolve(result);
              },
              error: (result) => {
                  console.log(result);
                  resolve(result);
              },
          });
      });
  }
};

// async function getUSDRate(sendResponse) {

//   let result = await request();

//   console.log(result);
//   if (result.success) {
//     sendResponse({status: 'good', result: result});
//   } else {
//     sendResponse({status: 'danger', result: result});
//   }

//   async function request() {
//       return new Promise((resolve, reject) => {
//           $.ajax({
//               type: "GET",
//               traditional: true,
//               contentType: "application/json; charset=utf-8",
//               url: `https://v6.exchangerate-api.com/v6/18a9c396a005b0fd3f4e7d50/latest/USD`,
//               dataType: "json",
//               success: (result) => {
//                   console.log(result);
//                   resolve(result);
//               },
//               error: (result) => {
//                   console.log(result);
//                   resolve(result);
//               },
//           });
//       });
//   }
// };