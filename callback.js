var callback=[];
var request_token_secret = localStorage.getItem("request_token_secret") || "";
var oauth_verifier = getParameterByName('oauth_verifier');
var request_token = getParameterByName('oauth_token');

console.log('oauth_verifier', oauth_verifier);

let data = {};

// Initialize
// const oauth = OAuth({
// 	consumer: {
// 		key: config.keys.app_key,
// 		secret: config.keys.app_secret,
// 	},
// 	signature_method: 'HMAC-SHA1',
// 	hash_function(base_string, key) {
// 		data.signature = CryptoJS.HmacSHA1(base_string, key).toString(CryptoJS.enc.Base64);
// 		console.log(CryptoJS.HmacSHA1(base_string, key).toString(CryptoJS.enc.Base64));
// 		return CryptoJS.HmacSHA1(base_string, key).toString(CryptoJS.enc.Base64)
// 	},
// })

const request_data = {
	url: 'https://openapi.etsy.com/v2/oauth/access_token',
	method: 'GET',
	data: { oauth_verifier : oauth_verifier },
}

// Note: The token is optional for some requests
const token = {
	key: request_token,
	secret: request_token_secret,
}

data.oauth = oauth.authorize(request_data, token);

$.ajax({
	url: request_data.url,
	type: request_data.method,
	data: data.oauth,
}).done(function(apiResponse) {
	apiResponse = unescape(apiResponse);
	console.log('apiResponse >> ', apiResponse);

	var oauth_token_secret = "";
	var oauth_token = "";	
	if (apiResponse && apiResponse.indexOf("oauth_token=") != -1) {
		oauth_token = getParameterByName('oauth_token','&'+apiResponse);
		oauth_token_secret = getParameterByName('oauth_token_secret',apiResponse);
	}
	if (oauth_token) {
		const keys = {
			key: oauth_token,
			secret: oauth_token_secret,
		}

		request_data.url = "https://openapi.etsy.com/v2/users/__SELF__"
		data.oauth = oauth.authorize(request_data, keys);
		$.ajax({
			url: request_data.url,
			type: request_data.method,
			data: data.oauth,
		}).done((resp) => {
			sendMessage({"command": "getSettings", "data": {keys: keys, oauth_data: data.oauth}},async function(dataResponse){
				var crede_obj={};
	
				var estyTabId = dataResponse.uiSettings.estyTabId;
				crede_obj['email']=resp.results[0]?.primary_email;
				crede_obj['etsy_access_token']=oauth_token;
				crede_obj['etsy_access_secret']=oauth_token_secret;

				console.log('crede_obj', crede_obj);

				await storageSet({key: 'userEmail', value: crede_obj.email});
				sessionStorage.setItem('userEmail', crede_obj.email);



				sendMessage({"command": "login", "data": {keys: keys, oauth_data: crede_obj}})
				chrome.tabs.sendMessage(estyTabId, { command: config.keys.updateEtsyCredential, data:crede_obj },function(res){
					chrome.tabs.sendMessage(estyTabId, { command: config.keys.successLoginEtsy });
					chrome.tabs.get(estyTabId, function(tab) {
						chrome.tabs.highlight({'tabs': tab.index}, function() {
							chrome.tabs.getCurrent(function(tab) {
								console.log(tab.id);
								chrome.tabs.remove(tab.id, function() {
								});
							});
						});
					});
				});
			});	    	
		})
	}
});

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, '\\$&');
	var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function sendMessage(msg, callbackfn) {
	if (callbackfn != null) {
		callback.push(callbackfn);
		msg.callback = "yes";
	}
	chrome.runtime.sendMessage(msg, callbackfn);
}