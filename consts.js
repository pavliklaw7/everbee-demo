var consts = {
  esty_api_v2_listing:'https://openapi.etsy.com/v2/listings/',
  esty_api_v2_shop:'https://openapi.etsy.com/v2/shops/',
  esty_api_v2_shop_listing:'https://openapi.etsy.com/v2/shops/listing/',
  _AnalyticsCode:'UA-151395649-1',
};
