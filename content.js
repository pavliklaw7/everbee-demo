
var callback=[];
var myContentPage = {
  uiSettings : {
    'scrollingIndex':0,
    'total_checked_arr':[]
  },
  load:function(){
    var dx = document.createElement('script');
    dx.src = chrome.extension.getURL('libs/dexie.js');
    (document.head || document.documentElement).appendChild(dx);
    var idb = document.createElement('script');
    idb.src = chrome.extension.getURL('js/idb.js');
    (document.head || document.documentElement).appendChild(idb);
    var srcA="https://api.memberstack.io/static/memberstack.js?custom";
    // var sc=document.createElement('script');
    // sc.src=srcA;
    // sc.setAttribute('data-memberstack-id','aaf3b8a7beb500ea6d0e051f66746b05');
    // document.body.appendChild(sc);
    var configJS = document.createElement('script');
    configJS.src = chrome.extension.getURL('js/config.js');
    (document.head || document.documentElement).appendChild(configJS);

    var s = document.createElement('script');
    var s2 = document.createElement('script');
    s2.src = chrome.extension.getURL('core.js');
    (document.head || document.documentElement).appendChild(s2);
    s.src = chrome.extension.getURL('js/main.js');
    (document.head || document.documentElement).appendChild(s);


    get(chrome.runtime.getURL('index.html'), null, null).then(function (htmlStr) {
      var relativepath=chrome.extension.getURL('');
      var html=htmlStr.replace(/#relativepath#/g, relativepath);
      const modal = document.createElement('custum-everBee');
      modal.setAttribute('class',"modal-dilog-everBee1");
      modal.id = "main-everbee-window";
      modal.innerHTML = html;
      document.body.appendChild(modal);

      var j = document.createElement('script');
      j.src = chrome.extension.getURL('js/webflow.js');
      (document.head || document.documentElement).appendChild(j);
    });
  },
  onExtMessage: function(message, sender, sendResponse){
    myContentPage.message = message;
    switch (message.command) {
      case config.keys.getElementListingIds:
      myContentPage.getElementListingIds(message.data, sender, sendResponse);
      break;
    }
  },
  getElementListingIds:function(category, sender, sendResponse){
    return new Promise((resolve, reject) => {

      var totalSearchesFound = false;
      var totalSearches = '';
      if ($('.wt-pt-lg-2.wt-nudge-b-1').length!=0) {
        totalSearchesFound=true;
        totalSearches=$('.wt-pt-lg-2.wt-nudge-b-1').find('span.wt-text-caption.wt-text-link-no-underline').text().trim();
      } else if($('.wt-display-flex-xs.wt-flex-wrap.wt-justify-content-flex-start').length!=0){
        totalSearchesFound=true;
        totalSearches=$('.wt-display-flex-xs.wt-flex-wrap.wt-justify-content-flex-start').find('span.wt-mt-xs-1').text()
      } else if($('.wt-list-inline.wt-display-inline.wt-mr-xs-1').length!=0){
        totalSearchesFound=true;
        totalSearches=$('.wt-list-inline.wt-display-inline.wt-mr-xs-1').next().text().replace('items','Results')
      } else if($('.wt-text-caption .wt-display-inline-flex-lg span:eq(0)').length!=0){
        totalSearchesFound = true;
        totalSearches = $('.wt-text-caption .wt-display-inline-flex-lg span:eq(0)').text().replace('items','Results')
      } else if($('.text-smaller .wt-display-inline-flex-lg span:eq(1)').length!=0){
        totalSearchesFound = true;
        totalSearches = $('.text-smaller .wt-display-inline-flex-lg span:eq(1)').text().replace('items','Results')
      }

      myContentPage.uiSettings.scrollingIndex=0;

      var listingLoadLimit = "64";

      if (listingLoadLimit != "All") {
        listingLoadLimit = parseInt(listingLoadLimit);
        listingLoadLimit = listingLoadLimit + 1;
      }

      var listingIdsArr=[];

      if (category == 'shop') {
        if ($('ul.listing-cards li').length > 0) {
          $('ul.listing-cards li').each(function(key, value){
            var dataListingId=$(this).attr('data-listing-id');
            var isChecked=$(this).attr('isChecked')
            if (dataListingId!=undefined && !!dataListingId && isChecked!=='true') {
              $(this).attr('isChecked',true);
              myContentPage.uiSettings.scrollingIndex++;
              if(myContentPage.uiSettings.scrollingIndex === listingLoadLimit) {
                return false;
              }
              var proObj={};
              var currencyValue=$(this).find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
              var currencySymbol=$(this).find('span.n-listing-card__price').find('.currency-symbol:eq(0)').text().trim();
              if (currencySymbol=="US$") {
                currencySymbol = "$";
              }else{
                currencySymbol = currencySymbol;
              }
              var uniqeIdStr=myContentPage.uniqeId();
              proObj['uniqeIdStr']=uniqeIdStr;
              proObj['listingId']=dataListingId;
              proObj['currencyType']=currencySymbol+' '+currencyValue;
              proObj['currencySymbol']=currencySymbol;
              proObj['productDetailUrl']=$(this).find('a[data-listing-id="'+dataListingId+'"]').attr('href');
              proObj['isChecked']=false;
              listingIdsArr.push(proObj);
              myContentPage.uiSettings.total_checked_arr.push(proObj);
            }
          });
        } else if ($('.shop-home-wider-items .v2-listing-card').length > 0) {
          $('.shop-home-wider-items .v2-listing-card').each(function(key, value){
            var dataListingId=$(this).attr('data-listing-id');
            var isChecked=$(this).attr('isChecked')
            if (dataListingId!=undefined && !!dataListingId && isChecked!=='true') {
              $(this).attr('isChecked',true);
              myContentPage.uiSettings.scrollingIndex++;
              if(myContentPage.uiSettings.scrollingIndex === listingLoadLimit) {
                return false;
              }
              var proObj={};
              var currencyValue = $(this).find('div.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
              var currencySymbol = $(this).find('div.n-listing-card__price').find('.currency-symbol:eq(0)').text().trim();
              if (currencySymbol == "US$") {
                currencySymbol = "$";
              }else{
                currencySymbol = currencySymbol;
              }
              var uniqeIdStr = myContentPage.uniqeId();
              proObj['uniqeIdStr'] = uniqeIdStr;
              proObj['listingId'] = dataListingId;
              proObj['currencyType'] = currencySymbol+' '+currencyValue;
              proObj['currencySymbol'] = currencySymbol;
              proObj['productDetailUrl'] = $(this).find('a[data-listing-id="'+dataListingId+'"]').attr('href');
              proObj['isChecked']=false;
              listingIdsArr.push(proObj);
              myContentPage.uiSettings.total_checked_arr.push(proObj);
            }
          });
        }
      } else if (category=='trending_now') {
       $('ul#search-results li a').each(function(key, value) {
        var dataListingId=$(this).attr('data-listing-id');
        var isChecked=$(this).attr('isChecked')
        if (dataListingId!=undefined && !!dataListingId && isChecked!=='true') {
          $(this).attr('isChecked',true);
          myContentPage.uiSettings.scrollingIndex++;
          if(myContentPage.uiSettings.scrollingIndex === listingLoadLimit) {
            return false;
          }
          var proObj={};
          var currencyValue=$(this).find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
          var currencySymbol=$(this).find('span.n-listing-card__price').find('.currency-symbol:eq(0)').text().trim();
          if (currencySymbol=="US$") {
            currencySymbol="$";
          }else{
            currencySymbol=currencySymbol;
          }
          var uniqeIdStr=myContentPage.uniqeId();
          proObj['uniqeIdStr']=uniqeIdStr;
          proObj['listingId']=dataListingId;
          proObj['currencyType']=currencySymbol+' '+currencyValue;
          proObj['currencySymbol']=currencySymbol;
          proObj['productDetailUrl']=$(this).closest('li').find('a[data-listing-id="'+dataListingId+'"]').attr('href');
          proObj['isChecked']=false;
          listingIdsArr.push(proObj);
          myContentPage.uiSettings.total_checked_arr.push(proObj);
        }
      });
     }
     else if (category=='homePage') {
      $('#content ul.block-grid-no-whitespace li').each(function(key,value){
        var dataListingId=$(this).attr('data-listing-id');
        var isChecked=$(this).attr('isChecked');
        if (dataListingId!=undefined && !!dataListingId && isChecked!=='true') {
          $(this).attr('isChecked',true);
          myContentPage.uiSettings.scrollingIndex++;
          if(myContentPage.uiSettings.scrollingIndex === listingLoadLimit) {
            return false;
          }
          var proObj={};
          var currencyValue=$(this).find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
          var currencySymbol=$(this).find('span.n-listing-card__price').find('.currency-symbol:eq(0)').text().trim();
          if (currencySymbol=="US$") {
            currencySymbol="$";
          }else{
            currencySymbol=currencySymbol;
          }
          var uniqeIdStr=myContentPage.uniqeId();
          proObj['uniqeIdStr']=uniqeIdStr;
          proObj['listingId']=dataListingId;
          proObj['currencyType']=currencySymbol+' '+currencyValue;
          proObj['currencySymbol']=currencySymbol;
          proObj['productDetailUrl']=$(this).find('a[data-listing-id="'+dataListingId+'"]').attr('href');
          proObj['isChecked']=false;
          listingIdsArr.push(proObj);
          myContentPage.uiSettings.total_checked_arr.push(proObj);
        }
      });
    } else {
      var $products_list = $('div.wt-grid.wt-grid--block li');
      if ($products_list.length == 0) {
        $products_list = $('ul.wt-grid.wt-grid--block.wt-pl-xs-0.tab-reorder-container li');
      }

      $products_list.each(function(key,value){
        let isAd = false;
        try{
          isAd = ($(this).find(".screen-reader-only")[0]?.innerHTML).includes('Ad from') ?? false;
        }
        catch{}
        var dataListingId=$(this).find('.v2-listing-card').attr('data-listing-id');
        var isChecked=$(this).attr('isChecked');
        if (dataListingId!=undefined && !!dataListingId && isChecked!=='true') {
          $(this).attr('isChecked',true);
          myContentPage.uiSettings.scrollingIndex++;
          if(myContentPage.uiSettings.scrollingIndex === listingLoadLimit) {
            return false;
          }
          var proObj={};
          var currencyValue=$(this).find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
          var currencySymbol=$(this).find('span.n-listing-card__price').find('.currency-symbol:eq(0)').text().trim();
          if (currencySymbol=="US$") {
            currencySymbol="$";
          }else{
            currencySymbol=currencySymbol;
          }
          var uniqeIdStr=myContentPage.uniqeId();
          proObj['uniqeIdStr']=uniqeIdStr;
          proObj['listingId']=dataListingId;
          proObj['currencyType']=currencySymbol+' '+currencyValue;
          proObj['currencySymbol']=currencySymbol;
          proObj['productDetailUrl']=$(this).find('a[data-listing-id="'+dataListingId+'"]').attr('href');
          proObj['isChecked']=false;
          proObj['isAd']=isAd;
          listingIdsArr.push(proObj);
          myContentPage.uiSettings.total_checked_arr.push(proObj);
        }
      });

    }

    if (listingIdsArr.length != 0) {
      var total_checked=$('[isChecked="true"]').length;
      var objFound={};
      var serchTerm=$('input[id="global-enhancements-search-query"]').val();

      objFound['totalSearches']=totalSearches;
      objFound['serchTerm']=serchTerm;
      objFound['listingIdsArr']=listingIdsArr;
      objFound['average_count_length']=myContentPage.uiSettings.total_checked_arr.length;
      console.log('1', objFound);
      resolve(objFound)
    } else {
     var objFound= {};
     console.log('2', objFound);
     objFound['listingIdsArr'] = 0;
     resolve(objFound)
   }
 });
},
uniqeId:function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
  }
  return s4() + '-' + s4();
}
};

chrome.runtime.onMessage.addListener(myContentPage.onExtMessage);

myContentPage.load();

function get(url, params, headers) {
  return Promise.resolve(
    $.ajax({
      url: url,
      type: "GET",
      data: params,
      headers: headers
    })
    );
}
function sendMessage(msg, callbackfn) {
  if(callbackfn!=null) {
    callback.push(callbackfn);
    msg.callback = "yes";
  }
  chrome.runtime.sendMessage(msg,callbackfn);
}

document.addEventListener('keydown', eventMap);

function eventMap(evt) {
  evt = evt || window.event;
  var isEscape = false;
  if ("key" in evt) {
    isEscape = (evt.key === "Escape" || evt.key === "Esc");
  } else {
    isEscape = (evt.keyCode === 27);
  }
  if (isEscape) {
    if ($('#main-everBee-ext').length!=0) {
      applyFadeOutAction();
      // enableScroll();
      $('#main-everBee-ext').removeClass('everbee-extension');
      $('#main-everBee-ext').attr('data-show','false');
      if ($('#memberstack-checkout-portal').css('display')=='block') {
        $('#memberstack-checkout-portal').css('display','none')
      }
    }
    if ($('#lightboxFrame').length!=0){
      $('#lightboxFrame').remove();
      $('#lightboxFrame2').remove();
      $('.everBee-hover-img').removeAttr('status');
      $('.everBee-hover-img').find('.active-everBee').removeClass('display-block').addClass('display-none');
    }

    openHomePopup = true;
    document.getElementById("goAnalytics").classList.remove('active');

  }
}

// Add native 'click' and 'change' events to be triggered using jQuery
jQuery.fn.extend({
  'vchange': function () {
    var change_event = document.createEvent('HTMLEvents')
    change_event.initEvent('change', false, true)
    return $(this).each(function () {
      $(this)[0].dispatchEvent(change_event)
    })
  },
  'vclick': function () {
    var click_event = document.createEvent('HTMLEvents')
    click_event.initEvent('click', false, true)
    return $(this).each(function () {
      $(this)[0].dispatchEvent(click_event)
    })
  },
  'vblur': function () {
    var click_event = document.createEvent('HTMLEvents')
    click_event.initEvent('blur', false, true)
    return $(this).each(function () {
      $(this)[0].dispatchEvent(click_event)
    })
  },
  'vkeyup': function () {
    var keyup_event = document.createEvent('HTMLEvents')
    keyup_event.initEvent('keyup', false, true)
    return $(this).each(function () {
      $(this)[0].dispatchEvent(keyup_event)
    })
  },
  'vmouseup': function () {
    var mouseEvent= document.createEvent ('MouseEvents');
    mouseEvent.initEvent ('mouseup', true, true);
    return $(this).each(function () {
      $(this)[0].dispatchEvent(mouseEvent)
    })
  },
  'vmousedown': function () {
    var mouseEvent= document.createEvent ('MouseEvents');
    mouseEvent.initEvent ('mousedown', true, true);
    return $(this).each(function () {
      $(this)[0].dispatchEvent(mouseEvent)
    })
  },
  'vTextEvent': function () {
    var evt = new Event('input', {
      bubbles: true
    });
    return $(this).each(function () {
      $(this)[0].focus();
      $(this)[0].dispatchEvent(evt)
    })
  }
})