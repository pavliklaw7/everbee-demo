/* Global Variables */

var general = new DB("general");
var idb = new DB("MsUsers");
var firstName = '';
var membership = {};
var userMeta = {};
var popOver_action = 'none';
var animation_action = 'show';
var reviewForStdArr = [];
var count, fav, averagesale;
var allSearchesLength = '';
var favResponse, Vectorsvg;
var listingtitle, ListingTags, ListingTitle, formattedString, shopResponse, shopId, shopname, shopheadline, rating, ratingcount, output;
var searchedKeyword;
var shopregionFound = false
var nbsp;
var isScroll = false;
var callback = [];
var message = '';
var openHomePopup = true;
// var logoURL = chrome.runtime.getURL("images/everbee_logo-frame.png");
var textlogoURL = chrome.runtime.getURL("images/text-logo2.png");
let slidersOverDisplayed = 0;
var selected_option_val = 50;
var logRecord = config.values.logIn;

const closeAnalyticsBtnURL = chrome.runtime.getURL("images/close-colored-new.png");
const closeKeywordsBtnURL = chrome.runtime.getURL("images/close-colored-new.png");
const closeFavoritesBtnURL = chrome.runtime.getURL("images/close-colored-new.png");
const everbeeMenu = document.createElement('div');
const logoURLwithText = chrome.runtime.getURL("images/everbee_logo-with-text.png");
const imgURLWindowBg = chrome.runtime.getURL("images/main-window-bg.png");
const imgURLWindowMinimizedBg = chrome.runtime.getURL("images/minimized-bg.png");
const imgURLKeyWordIllustration = chrome.runtime.getURL("images/keyword_illustration.png");
const imgURKAnalyticsIllustration = chrome.runtime.getURL("images/analytics-illustration.png");
const imgURKFavoritesIllustration = chrome.runtime.getURL("images/favorites-illustration.png");
const reziseMinimizeArrows = `<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M15.8046 1.13939C15.9297 1.01422 16.0001 0.844454 16.0001 0.667439C16.0001 0.490423 15.9297 0.320657 15.8046 0.195488C15.6794 0.070319 15.5096 0 15.3326 0C15.1556 0 14.9858 0.070319 14.8607 0.195488L10.6664 4.39105V2.00063C10.6664 1.82384 10.5962 1.65429 10.4712 1.52928C10.3462 1.40427 10.1766 1.33403 9.99985 1.33403C9.82306 1.33403 9.6535 1.40427 9.52849 1.52928C9.40348 1.65429 9.33325 1.82384 9.33325 2.00063V5.86689C9.33325 6.07904 9.41753 6.2825 9.56754 6.43252C9.71755 6.58253 9.92102 6.66681 10.1332 6.66681H13.9994C14.1762 6.66681 14.3458 6.59658 14.4708 6.47157C14.5958 6.34655 14.666 6.177 14.666 6.00021C14.666 5.82342 14.5958 5.65387 14.4708 5.52886C14.3458 5.40384 14.1762 5.33361 13.9994 5.33361H11.609L15.8046 1.13939Z" fill="#043872"/>
<path d="M6.00021 14.6658C5.82342 14.6658 5.65387 14.5955 5.52886 14.4705C5.40384 14.3455 5.33361 14.176 5.33361 13.9992V11.6088L1.13939 15.8043C1.07741 15.8663 1.00383 15.9155 0.922856 15.949C0.841879 15.9826 0.755088 15.9998 0.667439 15.9998C0.579789 15.9998 0.492998 15.9826 0.412021 15.949C0.331043 15.9155 0.257465 15.8663 0.195488 15.8043C0.133511 15.7423 0.0843477 15.6688 0.0508058 15.5878C0.0172639 15.5068 0 15.42 0 15.3324C0 15.2447 0.0172639 15.1579 0.0508058 15.077C0.0843477 14.996 0.133511 14.9224 0.195488 14.8604L4.39105 10.6662H2.00063C1.82384 10.6662 1.65429 10.596 1.52928 10.471C1.40427 10.3459 1.33403 10.1764 1.33403 9.9996C1.33403 9.82281 1.40427 9.65326 1.52928 9.52825C1.65429 9.40324 1.82384 9.33301 2.00063 9.33301H5.86689C6.07904 9.33301 6.2825 9.41728 6.43252 9.5673C6.58253 9.71731 6.66681 9.92077 6.66681 10.1329V13.9992C6.66681 14.176 6.59658 14.3455 6.47157 14.4705C6.34655 14.5955 6.177 14.6658 6.00021 14.6658Z" fill="#043872"/>
</svg>`
const reziseMaximizeArrows = `<svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7 0C6.86739 0 6.74021 0.0526785 6.64645 0.146447C6.55268 0.240215 6.5 0.367392 6.5 0.5C6.5 0.632608 6.55268 0.759785 6.64645 0.853553C6.74021 0.947321 6.86739 1 7 1H10.793L1.5 10.293V6.5C1.5 6.36739 1.44732 6.24021 1.35355 6.14645C1.25979 6.05268 1.13261 6 1 6C0.867392 6 0.740215 6.05268 0.646447 6.14645C0.552679 6.24021 0.5 6.36739 0.5 6.5V11.4C0.5 11.5591 0.563214 11.7117 0.675736 11.8243C0.788258 11.9368 0.94087 12 1.1 12H6C6.13261 12 6.25979 11.9473 6.35355 11.8536C6.44732 11.7598 6.5 11.6326 6.5 11.5C6.5 11.3674 6.44732 11.2402 6.35355 11.1464C6.25979 11.0527 6.13261 11 6 11H2.207L11.5 1.707V5.5C11.5 5.63261 11.5527 5.75979 11.6464 5.85355C11.7402 5.94732 11.8674 6 12 6C12.1326 6 12.2598 5.94732 12.3536 5.85355C12.4473 5.75979 12.5 5.63261 12.5 5.5V0.6C12.5 0.44087 12.4368 0.288258 12.3243 0.175736C12.2117 0.0632141 12.0591 0 11.9 0H7Z" fill="#043872"/>
</svg>`;
const imgURLCategoriesIllustration = chrome.runtime.getURL("images/illustrations-categories.svg");

// var imgURL = chrome.runtime.getURL("images/output-onlinepngtools.png");
// var logo_images = document.querySelectorAll('.ebe-page-left-img');

// logo_images.forEach(element => {
//     element.src = imgURL;
// });

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    console.log(message);
    switch (message.command) {
        case config.keys.successLoginEtsy:
            successLoginEtsy();
            break;
        case config.keys.updateEtsyCredential:
            updateEtsyCredential(message.data, sender, sendResponse);
            break;
        case config.keys.getSettingsDB:
            getSettingsDB(message.data, sender, sendResponse);
            break;
        case config.keys.openUpgradeModalFromPopover:
            addIframeToContent('upgradePop');
            break;
        case "getHeadlines":
            getHeadlines();
            break;
    }
});

// init functions
getToolBar();

document.querySelector("body").appendChild(everbeeMenu);
document.querySelector("body").classList.add('everbee-left-ghost');
document.getElementById("goAnalytics").addEventListener('click', (e) => openAnalytics(e));
// document.getElementById("keywordResearch").onclick = openKeywordResearch;
document.getElementById("goFavorites").onclick = openFavorites;
// document.getElementById('goCategories').onclick = openCategories;
document.getElementById("goContact").onclick = () => {
    sendMessage({ "command": "goEverBee" });
};
document.getElementById("menuLogOut").addEventListener('click', () => logOut());
document.getElementById("cancel_subscription_btn").addEventListener('click', (e) => cancelSubscription(e));


async function cancelSubscription(e) {
    e.preventDefault();
    const userEmail = await storageGet('userEmail');

    if (!!userEmail) {
        sendMessage({"command": "cancelSubscription", "data": {userEmail: userEmail}}, async (response) => {
            console.log('cancelSubscription', response);
            await storageSet({key: 'isSubsribed', value: false});
        });

        await storageSet({key: config.keys.etsy_access_token, value: ''});
        await storageSet({key: config.keys.etsy_access_secret, value: ''});
        await storageSet({key: config.keys.userEmail, value: ''});

        location.reload();

        getHeadlines();
        openHomePopup = false;
        localStorage.setItem(config.keys.everbeeWindowIsOpened, true);
    }
};


isLogged();

async function isLogged() {
    const keyEmail = await storageGet('userEmail');

    if (keyEmail && keyEmail.length > 0 && keyEmail != undefined) {
        // localStorage.setItem('isLoginUser', true);
        await storageSet({ key: 'isLoginUser', value: true })
        document.querySelector('#menuLogOut .nav-text').innerText = config.values.logOut;
        document.getElementById("signedInEmail").innerText = `Signed in account: ${await storageGet(config.keys.userEmail)}`;
    } else {
        isLogin = false;
        // openEtsyLoginModal();
    }
};

async function activeSubscription(e) {
    e.preventDefault();
    const userEmail = await storageGet('userEmail');
    location.href = `${config.base_url}Subscribe?email=${userEmail}&plan=simple plan`;
}

async function isSubscriptionActive() {
    return new Promise(async (resolve) => {
        const userEmail = await storageGet('userEmail');

        if (userEmail) {
            sendMessage({ "command": "checkSubscription", "data": { userEmail: userEmail } }, async (response) => {
                console.log('checkSubscription', response.result.active);
                if (!response.result.active) {
                    document.getElementById("active_subscription_btn").addEventListener('click', (e) => activeSubscription(e));
                } else if (response.result.active) {
                  await storageSet({key: 'isSubsribed', value: true});
                  document.querySelector('#cancelSubscription').classList.add('isActive');
                }
                resolve()
            });
        } 
    })
};

async function getToolBar() {
    // var img1 = document.createElement("img");
    // img1.classList.add('imgTest')
    let logoSrc = chrome.runtime.getURL("images/everBee-logo.svg");

    // img1.src = logoSrc;

    // document.body.append(img1);

    // await elementAppear('imgTest')

    everbeeMenu.innerHTML = `
  <div class="main-menu">

  <div class="scrollbar" id="style-1">

  <ul>
  <li id="logo-li">
  <div class="logo-image-container">
    <img src="${logoSrc}" id="menu-logo-img"/>
    <h2 id="menu-text-logo">${config.values.logoText}</h2>
    </div>
  </li>

	<!-- <li id="keywordResearch">
	<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M8.85721 0.714279C11.0168 0.714279 13.088 1.57218 14.6151 3.09927C16.1422 4.62635 17.0001 6.69752 17.0001 8.85714C17.0001 10.6417 16.4258 12.2917 15.4529 13.634L20.9095 19.0906C21.1454 19.3231 21.2825 19.6376 21.2922 19.9687C21.3019 20.2998 21.1835 20.6219 20.9616 20.8678C20.7396 21.1137 20.4314 21.2644 20.101 21.2886C19.7707 21.3128 19.4438 21.2086 19.1884 20.9977L19.0906 20.9094L13.6341 15.4529C12.2456 16.4611 10.5731 17.0028 8.85721 17C6.69759 17 4.62642 16.1421 3.09934 14.615C1.57226 13.0879 0.714355 11.0168 0.714355 8.85714C0.714355 6.69752 1.57226 4.62635 3.09934 3.09927C4.62642 1.57218 6.69759 0.714279 8.85721 0.714279ZM8.85721 3.28571C7.37958 3.28571 5.96246 3.8727 4.91762 4.91754C3.87277 5.96239 3.28578 7.3795 3.28578 8.85714C3.28578 10.3348 3.87277 11.7519 4.91762 12.7967C5.96246 13.8416 7.37958 14.4286 8.85721 14.4286C10.3348 14.4286 11.752 13.8416 12.7968 12.7967C13.8417 11.7519 14.4286 10.3348 14.4286 8.85714C14.4286 7.3795 13.8417 5.96239 12.7968 4.91754C11.752 3.8727 10.3348 3.28571 8.85721 3.28571Z" fill="white"/>
	</svg>
  <span class="nav-text">Keyword Research</span>
  </li>-->

  <li id="goAnalytics">
  <svg viewBox="0 0 21 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M20.25 16.125C20.25 16.4234 20.1315 16.7095 19.9205 16.9205C19.7095 17.1315 19.4234 17.25 19.125 17.25H1.125C0.826631 17.25 0.540483 17.1315 0.329505 16.9205C0.118526 16.7095 0 16.4234 0 16.125V1.125C0 0.826631 0.118526 0.540483 0.329505 0.329505C0.540483 0.118526 0.826631 0 1.125 0C1.42337 0 1.70952 0.118526 1.9205 0.329505C2.13147 0.540483 2.25 0.826631 2.25 1.125V10.4093L6.32972 6.32953C6.54069 6.11857 6.82683 6.00005 7.12519 6.00005C7.42354 6.00005 7.70968 6.11857 7.92066 6.32953L10.1252 8.53397L14.9093 3.75H13.8752C13.5768 3.75 13.2907 3.63147 13.0797 3.4205C12.8687 3.20952 12.7502 2.92337 12.7502 2.625C12.7502 2.32663 12.8687 2.04048 13.0797 1.8295C13.2907 1.61853 13.5768 1.5 13.8752 1.5H17.6252L17.6294 1.50019C17.665 1.50037 17.7006 1.50219 17.7361 1.50563C17.7533 1.50731 17.7698 1.5105 17.7869 1.51294C17.8063 1.51575 17.8257 1.518 17.845 1.52175C17.8639 1.5255 17.8822 1.53075 17.9008 1.53544C17.9179 1.53975 17.935 1.54341 17.9518 1.54856C17.9698 1.554 17.9872 1.56075 18.0048 1.56703C18.0218 1.57313 18.0391 1.57875 18.0559 1.58578C18.0722 1.59253 18.0877 1.60041 18.1037 1.60791C18.1211 1.61616 18.1387 1.62384 18.1556 1.63294C18.1706 1.641 18.185 1.65028 18.1997 1.659C18.2166 1.66903 18.2336 1.67869 18.25 1.68956C18.2659 1.70016 18.2807 1.71197 18.2959 1.72331C18.3101 1.73391 18.3247 1.74384 18.3385 1.75519C18.3652 1.77713 18.3907 1.80038 18.4153 1.82456C18.4171 1.82625 18.4191 1.82775 18.4207 1.82953C18.4226 1.83141 18.424 1.83328 18.4258 1.83516C18.45 1.85953 18.4732 1.88512 18.4952 1.91175C18.5064 1.92562 18.5164 1.94016 18.527 1.95431C18.5382 1.9695 18.55 1.98431 18.5605 2.00006C18.5718 2.01684 18.5815 2.03409 18.5917 2.05134C18.6002 2.06569 18.6093 2.07966 18.6172 2.09447C18.6265 2.11181 18.6344 2.12962 18.6427 2.14734C18.6501 2.16291 18.6578 2.17828 18.6644 2.19422C18.6716 2.21147 18.6773 2.22909 18.6835 2.24653C18.6897 2.26378 18.6963 2.28066 18.7016 2.29809C18.707 2.31581 18.7109 2.33372 18.7153 2.35162C18.7198 2.36934 18.7248 2.38688 18.7284 2.40488C18.7325 2.42559 18.735 2.4465 18.7379 2.46741C18.7402 2.48288 18.7431 2.49816 18.7446 2.51381C18.7483 2.55094 18.7502 2.58816 18.7502 2.62537V6.375C18.7502 6.67337 18.6317 6.95952 18.4207 7.1705C18.2097 7.38147 17.9236 7.5 17.6252 7.5C17.3268 7.5 17.0407 7.38147 16.8297 7.1705C16.6187 6.95952 16.5002 6.67337 16.5002 6.375V5.34094L10.9208 10.9205C10.7098 11.1314 10.4236 11.2499 10.1253 11.2499C9.82693 11.2499 9.54079 11.1314 9.32981 10.9205L7.12519 8.71603L2.25 13.5913V15H19.125C19.4234 15 19.7095 15.1185 19.9205 15.3295C20.1315 15.5405 20.25 15.8266 20.25 16.125Z" fill="white"/>
  </svg>
  <span class="nav-text">Analytics</span>
  </li>

  <li id="goFavorites">
  <svg viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11 1L13.2451 7.90983H20.5106L14.6327 12.1803L16.8779 19.0902L11 14.8197L5.12215 19.0902L7.36729 12.1803L1.48944 7.90983H8.75486L11 1Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>
  <span class="nav-text">Favorites</span>
  </li>

  <li id="goContact">
  <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M19 5H5C3.89543 5 3 5.89543 3 7V17C3 18.1046 3.89543 19 5 19H19C20.1046 19 21 18.1046 21 17V7C21 5.89543 20.1046 5 19 5Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  <path d="M3 7L12 13L21 7" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>
  <span class="nav-text">Contact Us</span>
  </li>
  <li id="cancelSubscription">
    <a href="#" id="cancel_subscription_btn">Cancel
    Subscription</a>
  </li>

  <li id="menuLogOut">
  <svg viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M0 10C0 10.2652 0.105357 10.5196 0.292893 10.7071C0.48043 10.8946 0.734784 11 1 11H8.59L6.29 13.29C6.19627 13.383 6.12188 13.4936 6.07111 13.6154C6.02034 13.7373 5.9942 13.868 5.9942 14C5.9942 14.132 6.02034 14.2627 6.07111 14.3846C6.12188 14.5064 6.19627 14.617 6.29 14.71C6.38296 14.8037 6.49356 14.8781 6.61542 14.9289C6.73728 14.9797 6.86799 15.0058 7 15.0058C7.13201 15.0058 7.26272 14.9797 7.38458 14.9289C7.50644 14.8781 7.61704 14.8037 7.71 14.71L11.71 10.71C11.801 10.6149 11.8724 10.5028 11.92 10.38C12.02 10.1365 12.02 9.86346 11.92 9.62C11.8724 9.49725 11.801 9.3851 11.71 9.29L7.71 5.29C7.61676 5.19676 7.50607 5.1228 7.38425 5.07234C7.26243 5.02188 7.13186 4.99591 7 4.99591C6.86814 4.99591 6.73757 5.02188 6.61575 5.07234C6.49393 5.1228 6.38324 5.19676 6.29 5.29C6.19676 5.38324 6.1228 5.49393 6.07234 5.61575C6.02188 5.73757 5.99591 5.86814 5.99591 6C5.99591 6.13186 6.02188 6.26243 6.07234 6.38425C6.1228 6.50607 6.19676 6.61676 6.29 6.71L8.59 9H1C0.734784 9 0.48043 9.10536 0.292893 9.29289C0.105357 9.48043 0 9.73478 0 10ZM13 0H3C2.20435 0 1.44129 0.316071 0.87868 0.87868C0.316071 1.44129 0 2.20435 0 3V6C0 6.26522 0.105357 6.51957 0.292893 6.70711C0.48043 6.89464 0.734784 7 1 7C1.26522 7 1.51957 6.89464 1.70711 6.70711C1.89464 6.51957 2 6.26522 2 6V3C2 2.73478 2.10536 2.48043 2.29289 2.29289C2.48043 2.10536 2.73478 2 3 2H13C13.2652 2 13.5196 2.10536 13.7071 2.29289C13.8946 2.48043 14 2.73478 14 3V17C14 17.2652 13.8946 17.5196 13.7071 17.7071C13.5196 17.8946 13.2652 18 13 18H3C2.73478 18 2.48043 17.8946 2.29289 17.7071C2.10536 17.5196 2 17.2652 2 17V14C2 13.7348 1.89464 13.4804 1.70711 13.2929C1.51957 13.1054 1.26522 13 1 13C0.734784 13 0.48043 13.1054 0.292893 13.2929C0.105357 13.4804 0 13.7348 0 14V17C0 17.7956 0.316071 18.5587 0.87868 19.1213C1.44129 19.6839 2.20435 20 3 20H13C13.7956 20 14.5587 19.6839 15.1213 19.1213C15.6839 18.5587 16 17.7956 16 17V3C16 2.20435 15.6839 1.44129 15.1213 0.87868C14.5587 0.316071 13.7956 0 13 0Z" fill="white"/>
  </svg>
  <span class="nav-text">${logRecord}</span>
  </li>
  </ul>
    <div class="copyright scrollbar__copyright">
    <p class="copyright__text">
      The term 'Etsy' is a trademark of Etsy, Inc.
      This application uses the Etsy API but is not 
      endorsed or certified by Etsy, Inc.
    </p>
    <p id="signedInEmail" class="copyright__signed-user"></p>
    </div>
  </div>`;
};

// <i class="fa fa-bar-chart fa-4x"></i>

async function openAnalytics(e) {
    const keyEmail = await storageGet('userEmail');
    applyFadeOutAction();

    // if (keyEmail == '') {
    //     applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');
    // } else {
        document.getElementById("closeAnalyticsBtn").src = closeAnalyticsBtnURL;
        document.getElementById("closeAnalyticsBtn").onclick = closeEverBee;
        document.querySelector('.main-menu .active')?.classList.remove('active');

        if (location.href.includes("https://www.etsy.com/listing/") && $(".everbee-extension-content.active").css("display") !== "none") {
            document.querySelector("#closeAnalyticsBtn").click()
        }

        getHeadlines();

        document.getElementById('currencyAnalytics').addEventListener('change', (e) => changeCurrency(e));
        
        checkUserCurrecy();

        if (location.href.includes('search') || location.href.includes('trending_now') || !keyEmail) {
            e.target.classList.add("active");
            document.querySelector(".main-window.active")?.classList.add('d-none');
            document.querySelector(".main-window.active")?.classList.remove('active');
            document.getElementById("everBee-ext-analytics").classList.remove('d-none');
            document.getElementById("everBee-ext-analytics").classList.add('active');
            document.getElementById("analytics-logo").src = logoURLwithText;
            document.getElementById("analytics-bg").src = imgURLWindowBg;
            document.getElementById("analytics-minimized-bg").src = imgURLWindowMinimizedBg;
            document.querySelector('.analytics__illustration').src = imgURKAnalyticsIllustration;
            openHomePopup = !openHomePopup;
            localStorage.setItem(config.keys.everbeeWindowIsOpened, openHomePopup);
        }       
}

// function openKeywordResearch() {
//   applyFadeOutAction();
//   document.getElementById("closeKeywordSearchBtn").src = closeKeywordsBtnURL;
//   document.getElementById("closeKeywordSearchBtn").onclick = closeEverBee;
//   addIframeToContent('normal');
//   document.querySelector('.main-menu .active')?.classList.remove('active');
//   this.classList.add("active");
//   document.querySelector(".main-window.active").classList.add('d-none');
//   document.querySelector(".main-window.active").classList.remove('active');
//   document.getElementById("everBee-ext-keyword").classList.remove('d-none');
//   document.getElementById("everBee-ext-keyword").classList.add('active');
//   document.getElementById("keyword-logo").src = logoURLwithText;
//   document.getElementById("keyword-bg").src = imgURLWindowBg;
//   document.getElementById("keyword-minimized-bg").src = imgURLWindowMinimizedBg;
//   document.querySelector('.search-bar__illustration').src = imgURLKeyWordIllustration;
//   initButtons();            
// }

async function openFavorites() {
    const keyEmail = await storageGet('userEmail');

    if (keyEmail == '') {
        applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');
    } 
    
    applyFadeOutAction();
    document.getElementById("closeFavoritesBtn").src = closeFavoritesBtnURL;
    document.getElementById("closeFavoritesBtn").onclick = closeEverBee;
    document.querySelector('.main-menu .active')?.classList.remove('active');

    addIframeToContent('normal');
    document.getElementById('currencyFavorites').addEventListener('change', (e) => changeCurrency(e));

    checkUserCurrecy();

    document.querySelector('#closeiframeBtn2')?.click();
    this.classList.add("active");
    document.querySelector(".main-window.active")?.classList.add('d-none');
    document.querySelector(".main-window.active")?.classList.remove('active');
    document.getElementById("everBee-ext-favorites").classList.remove('d-none');
    document.getElementById("everBee-ext-favorites").classList.add('active');
    document.getElementById("favorites-logo").src = logoURLwithText;
    document.getElementById("favorites-bg").src = imgURLWindowBg;
    document.getElementById("favorites-minimized-bg").src = imgURLWindowMinimizedBg;
    document.querySelector('.favorites__illustration').src = imgURKFavoritesIllustration;
    addExistingFavFolder();
}

// function openCategories() {
//  applyFadeOutAction();
//   document.getElementById("closeKeywordSearchBtn").src = closeKeywordsBtnURL;
//   document.getElementById("closeKeywordSearchBtn").onclick = closeEverBee;
//   addIframeToContent('normal');
//   document.querySelector('.main-menu .active')?.classList.remove('active');
//   this.classList.add("active");
//   document.querySelector(".main-window.active").classList.add('d-none');
//   document.querySelector(".main-window.active").classList.remove('active');
//   document.getElementById("everBee-ext-categories").classList.remove('d-none');
//   document.getElementById("everBee-ext-categories").classList.add('active');
//   document.getElementById("categories-logo").src = logoURLwithText;
//   document.getElementById("categories-bg").src = imgURLWindowBg;
//   document.getElementById("categories-minimized-bg").src = imgURLWindowMinimizedBg;
//   document.querySelector('.categories__illustration').src = imgURLCategoriesIllustration;
//   initButtons();
// }

function closeEverBee() {
    if ($('#main-everBee-ext').length != 0) {
        $('#main-everBee-ext').removeClass('everbee-extension');
        $('#main-everBee-ext').attr('data-show', 'false');
        if ($('#memberstack-checkout-portal').css('display') == 'block') {
            $('#memberstack-checkout-portal').css('display', 'none')
        }
    }
    if ($('#lightboxFrame').length != 0) {
        $('#lightboxFrame').remove();
        $('#lightboxFrame2').remove();
        $('.everBee-hover-img').removeAttr('status');
        $('.everBee-hover-img').find('.active-everBee').removeClass('display-block').addClass('display-none');
    }

    document.querySelector(".main-menu .active")?.classList.remove('active');
    localStorage.setItem(config.keys.everbeeWindowIsOpened, true);
}

async function logOut() {

    applyFadeOutAction();

    document.querySelector('#closeiframeBtn2')?.click();
    document.getElementById("analytics-logo").src = logoURLwithText;
    document.getElementById("analytics-bg").src = imgURLWindowBg;
    document.getElementById("analytics-minimized-bg").src = imgURLWindowMinimizedBg;
    document.querySelector('.analytics__illustration').src = imgURKAnalyticsIllustration;
        
    openEtsyLoginModal();

    document.querySelector('#signedInEmail').innerText = '';
    document.querySelector('#menuLogOut .nav-text').innerText = 'Log In';

    await storageSet({ key: 'userEmail', value: '' });
    await storageSet({ key: 'isLoginUser', value: false });

    popOver_action = 'login';

    await storageSet({key: config.keys.etsy_access_token, value: ''});
    await storageSet({key: config.keys.etsy_access_secret, value: ''});
    await storageSet({key: config.keys.userEmail, value: ''});

    getHeadlines();
    openHomePopup = false;
    localStorage.setItem(config.keys.everbeeWindowIsOpened, true);
}

function getDataByCategory(isLogin, load_from) {
    var fullUrl = window.location.href; 

    if (fullUrl.indexOf('/shop/') !== -1) {
        getData('shop', isLogin, load_from);
    } else if (fullUrl.indexOf('/c/') !== -1) {
        getData('pCategory', isLogin, load_from);
    } else if (fullUrl.indexOf('search?') !== -1 || fullUrl.indexOf('/search/') !== -1) {
        getData('keywords', isLogin, load_from);
    } else if (fullUrl.indexOf('market/') !== -1) {
        getData('trending_now', isLogin, load_from);
    } else if (fullUrl.indexOf("www.etsy.com") !== -1 || fullUrl.indexOf('?ref=lgo') !== -1) {
        getData('homePage', isLogin, load_from);
    } else {
        $.fancybox.close();
    }
}

function getSettingsDB(data, sender, sendResponse) {
    (async() => {
        var etsy_user_credential_obj = await getUserSetting();
        sendResponse(etsy_user_credential_obj);
    })();
};


function successLoginEtsy() {
    if (isLogin) {
        // $('#firstNameSucess').text(firstName)
        // applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-success')
    } else {
        // applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-signup')
        // signUp();
    }
}

async function getHeadlines() {
    let fullUrl = window.location.href;
    const isLogged = await storageGet('userEmail');

    if (fullUrl.indexOf('/listing/') !== -1 && !!isLogged) {
        var current_productId = fullUrl.split('/listing/')[1].split('/')[0];
        everBeeHover.checkMSForDetailPage(current_productId);
    } else if (window.location.href.includes('search') || window.location.href.includes('trending_now') || !isLogged) {
        addIframeToContent('normal');
    } else if (!!isLogged) {
        addIframeToContent('noSearch');
    }
}

$(document).off('mouseover', 'a#btn-g-trends').on('mouseover', 'a#btn-g-trends', () => {
    $(this).find('.ebe-header-icon').hide();
    $(this).find('.ebe-header-icon-active').show();
});
$(document).off('mouseout', 'a#btn-g-trends').on('mouseout', 'a#btn-g-trends', () => {
    $(this).find('.ebe-header-icon').show();
    $(this).find('.ebe-header-icon-active').hide();
});

$(document).off('mouseover', 'a#btn-g-help').on('mouseover', 'a#btn-g-help', () => {
    $(this).find('.ebe-header-icon').hide();
    $(this).find('.ebe-header-icon-active').show();
});
$(document).off('mouseout', 'a#btn-g-help').on('mouseout', 'a#btn-g-help', () => {
    $(this).find('.ebe-header-icon').show();
    $(this).find('.ebe-header-icon-active').hide();
});

/* Setting Page events */
$(document).off('click', '#settings-btn').on('click', '#settings-btn', (e) => {
    e.preventDefault();

    document.getElementById("saveListingNumBtn").onclick = () => {
        localStorage.setItem(config.keys.listingLoadLimit, selected_option_val);

        /* Call again dor data instant load */
        var $this = $(".ebe-table-body")
        var $results = $(".ebe-table-body");
        if (!$results.data("loading")) {
            $results.data('loading', true);
            isScroll = true;
            animation_action = 'show';
            getDataByCategory(isLogin, 'scroll');
            // $('.desc-demand-values').hide();
        }

        simpleNotify.notify("Changes applied ✅", "good", 3);
        applyFadeOutAction();
    };

    trackButtonClickEverBeeExt(e);

    var listingLoadLimit = localStorage.getItem(config.keys.listingLoadLimit) || "32";

    $(".ebe-page-settings").find('.w-dropdown-toggle div:eq(0)').text(listingLoadLimit);
    applyFadeAction('ebe-modals', 'ebe-page-settings');

    document.getElementById('logoutBtn').addEventListener('click', async() => {
        // localStorage.setItem('isLoginUser', false);
        await storageSet({ key: 'isLoginUser', value: false });
        // document.querySelector('#menuLogOut .nav-text').innerText = config.values.logIn;
        popOver_action = 'login';
        await storageSet({key: config.keys.etsy_access_token, value: ''});
        await storageSet({key: config.keys.etsy_access_secret, value: ''});
        await storageSet({key: config.keys.userEmail, value: ''});
        getHeadlines();
        openHomePopup = false;
        localStorage.setItem(config.keys.everbeeWindowIsOpened, true);
        //localStorage.setItem(config.keys.everbeeWindowSize, 'max');
        document.getElementById("goAnalytics").classList.add('active');
    });

    $('.ebe-page-settings').find('.ebe-dropdown').find('.ebe-dropdown-list').removeClass('w--open');
});

$(document).off('click', '.ebe-page-settings .w-dropdown-link').on('click', '.ebe-page-settings .w-dropdown-link', (e) => {
    e.preventDefault();
    var $dropdown_link = $(this);
    var selected_option = $dropdown_link.text().trim();
    selected_option_val = $dropdown_link.attr('data-value').trim();
    var $dropdown_row = $dropdown_link.closest('.w-dropdown');
    $dropdown_row.find('.w-dropdown-toggle div:eq(0)').text(selected_option);
    $dropdown_row.find('.w-dropdown-toggle').click();

    if (document.querySelector(".w-dropdown-toggle div").innerHTML == "All listings") {
        document.querySelector(".w-dropdown-toggle div").innerHTML = "All";
    }

    // localStorage.setItem('listingLoadLimit',selected_option_val);

    // // Call again dor data instant load

    // var $this = $(".ebe-table-body")
    // var $results = $(".ebe-table-body");
    // if(!$results.data("loading")){
    //   $results.data('loading',true);
    //   isScroll = true; animation_action = 'show';
    //   getDataByCategory(isLogin, 'scroll');
    //   $('.desc-demand-values').hide();
    // }
    // applyFadeOutAction();
});

$(document).off('mouseover', '.ebe-page-settings .ebe-info-icon-container').on('mouseover', '.ebe-page-settings .ebe-info-icon-container', () => {
    $('.ebe-tooltip-opened').show();
});

$(document).off('mouseout', '.ebe-page-settings .ebe-info-icon-container').on('mouseout', '.ebe-page-settings .ebe-info-icon-container', () => {
    $('.ebe-tooltip-opened').hide();
});

$(document).off('click', '.ebe-page-settings .ebe-page-trending-exit').on('click', '.ebe-page-settings .ebe-page-trending-exit', (e) => {
    e.preventDefault();
    applyFadeOutAction();
    $('.ebe-page-settings').find('.ebe-dropdown').find('.ebe-dropdown-list').removeClass('w--open');
});

$(document).off('click', '.ebe-page-new-folder .ebe-page-trending-exit').on('click', '.ebe-page-new-folder .ebe-page-trending-exit', (e) => {
    e.preventDefault();
    applyFadeOutAction();
});

$(document).off('click', '.ebe-page-add-to-folder .ebe-page-trending-exit').on('click', '.ebe-page-delete-folder .ebe-page-trending-exit', (e) => {
    e.preventDefault();
    applyFadeOutAction();
});

$(document).off('click', '.ebe-page-settings .ebe-dropdown-toggle').on('click', '.ebe-page-settings .ebe-dropdown-toggle', (e) => {
    e.preventDefault();
    $('.ebe-page-settings').find('.ebe-dropdown').find('.ebe-dropdown-list').toggleClass('w--open');
});


/* End Setting Page events */
function addIframeToContent(type) {

    (async() => {
        var etsy_user_credential_obj = await getUserSetting();
        var searchOverAction = etsy_user_credential_obj.searchOverAction;
        if ($('#lightboxFrame').length != 0) {
            $('#lightboxFrame').remove();
            $('#lightboxFrame2').remove();
        }

        if (type === 'noSearch') {
            sendMessageCustom(type, 1, type, searchOverAction);
        } else {
            sendMessageCustom('getMember', 1, type, searchOverAction);
        }
    })();
};

document.addEventListener('checkMemberstackMember', async (e) => {
    isLogin = await storageGet('isLoginUser');
    // const isSubsribed = await storageGet('isSubsribed');

    if (isLogin == undefined) {
        isLogin = false;
    }
    firstName = e.detail[0]['firstName'];
    popOver_action = 'none';
    if (e.detail[0]['membership'] != undefined) {
        membership = e.detail[0]['membership'];
    }
    if (e.detail[0]['userMeta'] != undefined) {
        userMeta = e.detail[0]['userMeta'];
    }

    start(isLogin);
    // $(".toggle-password").click(() => {
    //   $(this).toggleClass("fa-eye fa-eye-slash");
    //   var input = $('[login="login-pass"]');

    //   if (input.attr("type") == "password") {
    //     input.attr("type", "text");
    //   } else {
    //     input.attr("type", "password");
    //   }
    // });
});

// Add native 'click' and 'change' events to be triggered using jQuery
jQuery.fn.extend({
    'vchange': function() {
        var change_event = document.createEvent('HTMLEvents')
        change_event.initEvent('change', false, true)
        return $(this).each(() => {
            $(this)[0].dispatchEvent(change_event)
        })
    },
    'vclick': function() {
        var click_event = document.createEvent('HTMLEvents')
        click_event.initEvent('click', false, true)
        return $(this).each(() => {
            $(this)[0].dispatchEvent(click_event)
        })
    },
    'vblur': function() {
        var click_event = document.createEvent('HTMLEvents')
        click_event.initEvent('blur', false, true)
        return $(this).each(() => {
            $(this)[0].dispatchEvent(click_event)
        })
    },
    'vkeyup': function() {
        var keyup_event = document.createEvent('HTMLEvents')
        keyup_event.initEvent('keyup', false, true)
        return $(this).each(() => {
            $(this)[0].dispatchEvent(keyup_event)
        })
    },
    'vmouseup': function() {
        var mouseEvent = document.createEvent('MouseEvents');
        mouseEvent.initEvent('mouseup', true, true);
        return $(this).each(() => {
            $(this)[0].dispatchEvent(mouseEvent)
        })
    },
    'vmousedown': function() {
        var mouseEvent = document.createEvent('MouseEvents');
        mouseEvent.initEvent('mousedown', true, true);
        return $(this).each(() => {
            $(this)[0].dispatchEvent(mouseEvent)
        })
    },
    'vTextEvent': function() {
        var evt = new Event('input', {
            bubbles: true
        });
        return $(this).each(() => {
            $(this)[0].focus();
            $(this)[0].dispatchEvent(evt)
        })
    }
});

let dataLoadCounter = 0;
let currentFavData = [];
let currentPageData = [];
let currentPageKeyword = '';
let arrCount = 0;
let currCategory = '';

async function getData(category, isLogin, load_from) {
    console.log('getData Func', category);
    var fullUrl = window.location.href;
    // isLogin = localStorage.getItem(config.keys.isLoginUser);
    isLogin = await storageGet('isLoginUser');

    (async() => {
        // var estySearchCount = await chekRemainSearch.chekNewSearchByUser(userMeta['estySearchCount']);
        var data = await myContentPage.getElementListingIds(category); // take lisitngsArr from page
        var favData = await getFavoritesIdsFromServer();
        var etsy_user_credential_obj = await getUserSetting();
        // var searchOverAction = etsy_user_credential_obj.searchOverAction;
        // var estyDefaultCount = etsy_user_credential_obj.estyDefaultCount;
        // var loginEmail = etsy_user_credential_obj.loginEmail;
        var etsy_user_credential = etsy_user_credential_obj.etsy_user_credential;

        if (etsy_user_credential.etsy_access_token != undefined && etsy_user_credential.etsy_access_token != '' && isLogin) {
            let s = await storageGet('isSubsribed');

            if(s){
                $('#search-remain').text("100");
                userMeta['estySearchCount'] = 100;
                $('.ebe-upgrade-btn.w-inline-block').hide();
                $('.ebe-premium-btn.w-inline-block').css('display', 'flex')
                $('.ebe-searches-btn-2.w-inline-block').css('display', 'none');
    
                if (category == 'pCategory') {
                    var searchedKeywordcategoryName = fullUrl.substr(fullUrl.indexOf("/c/") + 3).split('?')[0].replace(/\-/g, ' ');
    
                    if (searchedKeywordcategoryName.indexOf('/') !== -1) {
                        searchedKeywordcategoryName = searchedKeywordcategoryName.split('/')[0]
                    }
    
                    searchedKeywordG = searchedKeywordcategoryName;
                    await saveKeywordsTrends(searchedKeywordcategoryName);
                } else if (category == 'shop') {
                    var searchedKeywordShop = fullUrl.substr(fullUrl.lastIndexOf("/") + 1).split('?')[0].replace(/\-/g, ' ');
                    await saveKeywordsTrends(searchedKeywordShop);
                    searchedKeywordG = searchedKeywordShop;
                } else if (category == 'homePage') {
                    await saveKeywordsTrends('Etsy');
                } else if (category == 'trending_now') {
                    var trendKey = location.href.split('market/')[1];
                    searchedKeywordG = trendKey.replace(/\_/g, ' ');
                    await saveKeywordsTrends(searchedKeywordG);
                } else {
                    var searchedKeyword = fullUrl.substr(fullUrl.indexOf("?") + 3).replace(/\-/g, ' ');
                    if (searchedKeyword.indexOf('&') !== -1) {
                        searchedKeyword = searchedKeyword.split('&')[0].replace(/\-/g, ' ');
                    }
    
                    searchedKeywordG = $('#global-enhancements-search-query').val();
                    await saveKeywordsTrends(searchedKeywordG);
                }

                currCategory = category

                if (data.average_count_length > 0) {
                    currentPageData = data;
                    currentPageKeyword = searchedKeyword
                }

                if (favData.length > 0) {
                    currentFavData = favData;
                }
    
                handleResponseAsyncAnalyticsIds(data, category, searchedKeywordG);

                if (!dataLoadCounter > 0) { // to prevent additional data loading to favorites
                    handleResponseAsyncFavoritesIds(favData, category, searchedKeywordG);
                }
    
                dataLoadCounter++;
            }
            else{
                applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-subscribe');
            }
        } else {
            applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-login');
            // document.getElementById('loginBtn').addEventListener('click', (e) => {
            //   e.preventDefault();
            //   login(e);
            // });
        }
    })();
};

async function changeCurrency(e) {
    await storageSet({key: 'userCurrency', value: e.target.value});
  
    checkUserCurrecy()
    
    let avgpriceText = document.querySelector('#Avgprice').innerText;
  
    avgpriceText = avgpriceText.slice(0, 3) + ` ${e.target.value}`;

    // applyFadeInActionForLoader('metric-loading-animation', 'ebe-metric-value');
  
    changeActive = true;
    changeActiveFav = true;
  
    const analyricsRowCount = document.querySelectorAll('.analytics .ebe-table-expandable-row').length;
    const favoritesRowCount = document.querySelectorAll('.favorites .ebe-table-expandable-row').length;

    debugger;
  
    if (!inLoop) {
      clearTable();
      handleResponseAsyncAnalyticsIds(currentPageData, currCategory, currentPageKeyword);  
    }
    
    if (favoritesRowCount == currentFavData.length) {
        clearTable('favorites');
        handleResponseAsyncFavoritesIds(currentFavData, currCategory, currentPageKeyword);
    }
  }

  async function checkUserCurrecy() {
    const userCurrency = await storageGet('userCurrency');
    if (userCurrency) {
      const optionsA = Array.from(document.querySelector('#currencyAnalytics').children);
      const optionsF = Array.from(document.querySelector('#currencyFavorites').children);
  
  
      optionsA.forEach(opt => {
        if (opt.value === userCurrency) {
          opt.selected = true;
        }
      });
  
      optionsF.forEach(opt => {
        if (opt.value === userCurrency) {
          opt.selected = true;
        }
      });
    } else {
      return;
    }
  }

  function clearTable(table) {
    if (table) {
      Array.from(document.querySelectorAll(`.${table} .ebe-table-expandable-row`)).forEach(el => el.remove());
    } else {
      Array.from(document.querySelectorAll(`.ebe-table-expandable-row`)).forEach(el => el.remove());
    }
  }