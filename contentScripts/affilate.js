init()

async function init(){
    function getClientReferenceId() {
        return window.Rewardful && window.Rewardful.referral || ('checkout_'+(new Date).getTime());
    }
    
    var s = document.createElement('script');
    s.setAttribute("async", "")
    s.setAttribute("src", "https://r.wdfl.co/rw.js")
    s.setAttribute("data-rewardful", "605017")
    document.getElementsByTagName('head')[0].appendChild(s);

    let url = new URL(location.href)
    let userEmail = url.searchParams.get("email")
    var s2 = document.createElement('script');
    s2.text = `
        let ref = window.Rewardful.referral
        location.href = "${config.base_url}Subscribe?email=${userEmail}&plan=simple plan&referral=" + ref;
    `
    document.getElementsByTagName('head')[0].appendChild(s2); 
}