var pushProductDetailForExpandOptionArr = [];
var allCopidTagsGlobalArr = [];
var shopregion;
var shopcity;
var shopsale;
var listingtitle;
var listingtags;
var NotAvailable = "N/A";
var searchedKeywordG;
var avgsalesum = 0;
var userMeta = {};
var averageprice, estrevenues;
let alllength = 0;
var avgsalesForStdArr = [];
var priceForStdArr = [];
var avgfavForStdArr = [];


let changeActive = false;
let inLoop = false;

function GetProductInfoDetail(id, category, eleParent) {
  var prodId = id;

  for (var i = 0; i < pushProductDetailForExpandOptionArr.length; i++) {
    if (pushProductDetailForExpandOptionArr[i]['listing_id'] == prodId) {
      listingtitle = pushProductDetailForExpandOptionArr[i]['listingtitle'];
      var productUrl = pushProductDetailForExpandOptionArr[i]['productUrl'];
      var arrTags = pushProductDetailForExpandOptionArr[i]['listingtags'];
      var arrLenHalf = pushProductDetailForExpandOptionArr[i]['arrLenHalf'];
      var ImagelistingUrl = pushProductDetailForExpandOptionArr[i]['prodImg'];
      var TagsC;
      allCopidTagsGlobalArr = [];
      var arr = arrTags.slice(0, 13);
      var TagsC;
      allCopidTagsGlobalArr = [];

      TagsC = '<div class="ebe-tags-section">';
      arr.forEach(function (item, i) {
        if (i % 7 == 0 && i != 0) {
          TagsC += '</div><div class="ebe-tags-section _4margin">';
        }
        TagsC += (`<div class='listing-tag-div' id='t${i}'><div class='listing-tag-text'>${item}</div><a class="listing-tag-search-link">
        open “${item}" in a keyword search
    </a></div>`);
        allCopidTagsGlobalArr.push(item);
      });


      TagsC += '</div>';


      if (arr.length != 0) {
        $(eleParent).find('.ebe-tags').html(TagsC);
      } else {
        $(eleParent).find('.ebe-tags').html('');
      }


      var tagClass = document.querySelectorAll(".listing-tag-text");

      tagClass.forEach(function (element) {
        element.addEventListener("click", function () {
          copySingleTag(element);
        });
      });

      const listingTagSearchLink = document.querySelectorAll('.listing-tag-search-link');

      listingTagSearchLink.forEach(link => {
        link.addEventListener('click', (e) => {
          e.preventDefault();
          getKeywordSearch(link.previousSibling.innerText);
        });
      });


      $('body').off('click', '.copyallButton').on('click', '.copyallButton', function () {
        copyToClipboardComaSeparted()
        var from = $(this);
        var ff = $(this).html();
        $(this).html('<div style="display:flex;opacity:1" class="copied-clipboard-text">Copied to clipboard!</div>');
        var _this = $(this);
        setTimeout(function () {
          $(_this).html(ff);
        }, 1500);
      });

      $(".tooltip-wrap").hover(

        function () {
          $(this).find('.assistive-text__wrap').css("display", "flex");
        },
        function () {
          $(this).find('.assistive-text__wrap').fadeOut(300);
        }
      );

      console.log('setProdImage', ImagelistingUrl, productUrl, eleParent);

      setProdImage(ImagelistingUrl, productUrl, eleParent);
      $(eleParent).find('.ebe-table-row-expanded-listing-link').attr('href', productUrl);
      $(eleParent).find('.ebe-table-row-expanded-listing-link').attr('target', '_blank');
      $(eleParent).find('.ebe-table-row-expanded-listing-link').find('.listing-title-text').html(listingtitle);
      applyFadeActionForExpandRow('loaderHide', eleParent, '');
    }
  }
}

function copyToClipboardComaSeparted() {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val(allCopidTagsGlobalArr.join(',')).select();
  document.execCommand("copy");
  $temp.remove();
}

async function handleResponseAsyncAnalyticsIds(data, category, searchedKeyword) {

  if (data.average_count_length > 0) {
    currentPageData = data;
    currentPageKeyword = searchedKeyword
  }

  if (data != undefined && data.listingIdsArr.length > 0) {
    applyFadeInActionForLoader('metric-loading-animation', 'ebe-metric-value');

    var hasSearchCount = 0;
    let dResponse = data.listingIdsArr;
    alllength = data.average_count_length;
    allSearchesLength = data.totalSearches != '' ? data.totalSearches : alllength;

    if (category == 'keywords' || category == 'trending_now' || category == 'pCategory') {
      $('.ebe-score').show();
      var keywords = searchedKeyword;
      $('#search_keyword').text(keywords);
      var countKws = String(allSearchesLength).replace('(', '').replace(')', '').replace(' Results', '').replace(/[^\d]/g, '');
      arrCount = +countKws
      $('#list-length').text(numberWithCommas(countKws));
      hasSearchCount = countKws;
    }
    // else {
    // $('.ebe-score').hide();
    // $('.ebe-table-footer').addClass('alignRight');
    // $('.ebe-footer-total-results').hide()
    // }

    var isExistLoader = $('.ebe-table-body.analytics').find('.ebe-table-expandable-row').find('.ebe-table-row-parent-dummy');

    if (isExistLoader.length != 0) {
      $('.ebe-table-body.analytics').addClass('overflowRemove');
    }

    // var listingLoadLimit = localStorage.getItem(config.keys.listingLoadLimit) || "32";

    console.log('dResponse >', dResponse); // before filter

    dResponse = dResponse.filter((v, i, a) => a.findIndex(t => (t.listingId === v.listingId)) === i);

    console.log('UniqdResponse >', dResponse); // after filter


    for (var obj = 0; obj < dResponse.length; obj++) {
      inLoop = true;

      if (changeActive) {
        changeActive = false;
        clearTable('analytics');
        obj = 0;
      }

      var productObjDetail = await getProductInfoAsync(dResponse[obj]['listingId']);

      console.log('productObjDetail', productObjDetail);

      var NotAvailableClass = '';

      var itemReviewId = dResponse[obj]['uniqeIdStr']; //unused

      await pushProductDetailForExpandOption(productObjDetail);

      console.log('productObjDetail', productObjDetail);

      if (productObjDetail.state == 'active') {
        var uniqPrice = productObjDetail['price'];
        var currencyCode = productObjDetail['currency_code'];
        var priceInt = uniqPrice.replace(/[^\d.]/g, '');
        uniqPrice = +uniqPrice;
        var currencySymbol = dResponse[obj]['currencySymbol'];
        id = productObjDetail.listing_id;
        var productTitleStr = !!productObjDetail.title ? productObjDetail.title : '';
        var title = productTitleStr.length > 25 ? productTitleStr.substr(0, 24) + '...' : productTitleStr;
        listingtitle = productTitleStr;
        tags = productObjDetail.tags.toString();
        var prodTags = tags.length > 10 ? tags.substr(0, 9) + '...' : tags;
        var thisAve = parseFloat(priceInt) || 0;
        priceForStdArr.push(thisAve);

        sum = (parseFloat(sum) + parseFloat(thisAve));
        averageprice = parseFloat((sum / alllength)).toFixed(2);

        var original_creation_tsz = productObjDetail.original_creation_tsz;
        var d = new Date();
        var secondsEpoch = d.getTime() / 1000;
        var differenceInSeconds = parseInt(secondsEpoch - original_creation_tsz);
        var viewsrange = (parseFloat(productObjDetail.views) / 100);

        var avgfav = parseFloat(productObjDetail.num_favorers) || 0;
        avgfavForStdArr.push(avgfav);
        avgfavsum = avgfavsum + avgfav;

        averagefav = parseFloat(Math.round((avgfavsum / alllength))).toFixed(2)

        var lsViews = parseFloat(productObjDetail.views) || 0;
        var lsFavorers = avgfav;

        var newFormula = parseFloat(((-0.1152) + lsViews) * (0.031)) + parseFloat(lsFavorers * 0.047);
        var avgsalesDivi = (parseFloat(newFormula) / differenceInSeconds) || 0;
        var avgsales = parseFloat(avgsalesDivi * 86400 * 30).toFixed(2) || 0;

        if (avgsales >= 59) {
          avgsales = Math.log2(avgsales) * 10;
        }



        var estimated_total_sales = ((lsViews || 1) * 0.031 + lsFavorers * 0.047) || 0;
        estimated_total_sales = Math.round(estimated_total_sales) > 0 ? numberWithCommas(Math.round(estimated_total_sales)) : 0;

        avgsalesum = (parseFloat(avgsalesum) + parseFloat(avgsales));
        averagesale = parseFloat(Math.round((avgsalesum / alllength))).toFixed(2);
        estrevenues = (parseFloat(priceInt) * Math.round(avgsales)).toFixed(2);
        var prodImg = productObjDetail['Images'][0]['url_170x135'];
      
        
      } else {
        NotAvailableClass = 'NotAvailableClass';
        title = 'Product Unavailable';
        productObjDetail.url = '';
        productObjDetail.taxonomy_path = [];
        productObjDetail.category_path = [];
        uniqPrice = '';
        productObjDetail.num_favorers = '';
        productObjDetail.views = '';
        itemReview = '';
        estrevenues = 0;
        prodTags = '';
        tags = '';
        currencySymbol = ''
        prodImg = chrome.extension.getURL('broken.png');
      }
      var shopObjDetail = {};
      // var shopname = productObjDetail['Shop']['shop_name'].length > 18 ? productObjDetail['Shop']['shop_name'].substr(0, 17) + '...' : productObjDetail['Shop']['shop_name']; //unused
      shopObjDetail['shopId'] = productObjDetail['Shop']['shop_id'];
      shopObjDetail['shopname'] = productObjDetail['Shop']['shop_name'];
      shopObjDetail['shopheadline'] = productObjDetail['Shop']['title'];
      var estSalesNumber = Math.round(avgsales) > 0 ? numberWithCommas(Math.round(avgsales)) : 0;
      var estRevNumber = Math.round(estrevenues) > 0 ? numberWithCommas(Math.round(estrevenues)) : 0;
      var uniqeIdForRow = myContentPage.uniqeId();
      // var relativepath = chrome.extension.getURL('');//unused

      avgsalesForStdArr.push(+estSalesNumber);

      let ad = document.createElement("div");
      if (dResponse[obj]["isAd"]) {
        let p = document.createElement('p');
        p.textContent = "AD";
        p.classList = 'ad-sign';
        ad.appendChild(p);
      }

      if (+estimated_total_sales < +estSalesNumber) {
        estimated_total_sales = estSalesNumber;
      }

      productObjDetail.url = productObjDetail.url.replace('everBeechromeextension', 'everbee');

      let resultInFav = await checkInFav(productObjDetail.listing_id, await storageGet(config.keys.userEmail));

      const resultListing = getExpandableRow(
        productObjDetail,
        uniqeIdForRow,
        resultInFav,
        shopObjDetail,
        prodImg,
        title,
        ad,
        listingtitle,
        tags,
        currencyCode,
        uniqPrice.toFixed(2),
        estimated_total_sales,
        estSalesNumber,
        estRevNumber
      );

      var dummyRow = $('.ebe-table-body.analytics').find('.ebe-table-expandable-row:eq(' + obj + ')').find('.ebe-table-row-parent-dummy');

      console.log('dummyRow', dummyRow.length);

      if (dummyRow.length != 0) {
        dummyRow.parent().after(resultListing);
        dummyRow.parent().remove();

        await applyFadeToExpandRows(uniqeIdForRow, dummyRow.parent());
      } else {
        $('.ebe-table-body.analytics').removeClass('overflowRemove');
        $('.ebe-table-body.analytics').append(resultListing);

        await applyFadeToExpandRows(uniqeIdForRow, dummyRow.parent())
      }
      
      $('.ebe-table-body.analytics .favoritesBtn:not(.processed)').on('click', async (event) => {
        let listingId = event.target.closest(".ebe-table-row-parent").getAttribute('pId');
        // let userEmail = localStorage.getItem(config.keys.userEmail);
        let userEmail = await storageGet(config.keys.userEmail);

        if (event.target.classList.contains('inFavChecked')) {
          sendMessage({
            "command": config.keys.removeFavorite,
            "data": {
              listingId: listingId,
              userEmail: userEmail
            }
          }, function (dataResponse) {
            if (dataResponse.result.success) {
              document.querySelector(`.ebe-table-body.favorites div[pid='${listingId}']`)?.parentElement?.remove();
            } else {
              simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
            }
          });
        } else {
          sendMessage({
            "command": config.keys.saveFavorite,
            "data": {
              listingId: listingId,
              currencyCode: currencyCode,
              category: category,
              userEmail: userEmail
            }
          }, function (dataResponse) {
            if (dataResponse.result.success) {
              let clone = event.target.closest(".ebe-table-expandable-row").cloneNode(true);
              if(document.querySelector(".add-favorites__slide.active")){
                clone.style.display = "none"
              }

              $(clone.querySelector(".favoritesBtn")).on('click', async (event) => {
                let listingId = event.target.closest(".ebe-table-row-parent").getAttribute('pId');
                let userEmail = await storageGet(config.keys.userEmail);
                
        
                if (event.target.classList.contains('inFavChecked')) {
                  sendMessage({
                    "command": config.keys.removeFavorite,
                    "data": {
                      listingId: listingId,
                      userEmail: userEmail
                    }
                  }, function (dataResponse) {
                    if (dataResponse.result.success) {
                      console.log(dataResponse.result);
                      let star = document.querySelector(".analytics").querySelector(`div.ebe-table-row-parent[pid="${listingId}"] .inFavChecked`)
                      star?.classList.remove("inFavChecked")
                      star?.classList.add("inFavUnchecked")
                      event.target.closest(".ebe-table-expandable-row").remove()
                    } else {
                      simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
                      console.log(dataResponse.result);
                    }
                  });
                }
              });

              document.querySelector(`.ebe-table-body.favorites`).appendChild(clone);

            } else {
              simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
            }
          });
        }

        $(event.target).toggleClass("inFavChecked");
        $(event.target).toggleClass("inFavUnchecked");
      });

      $('.ebe-table-body.analytics .favoritesBtn:not(.processed)').addClass("processed")

      if ((obj + 1) == (dResponse.length)) {
        $('.ebe-table-body.analytics').find('.ebe-table-expandable-row').find('.ebe-table-row-parent-dummy').parent().remove();
      }

      $('body').off('click', `.ebe-table-body.analytics [pid="${productObjDetail.listing_id}"]`).on('click', `.ebe-table-body.analytics [pid="${productObjDetail.listing_id}"]`, function (elm) {
        if (elm.target.className == 'premetagTd' || elm.target.classList.contains('favoritesBtn')) {
          return false;
        }
        if (elm.target.classList.contains('w-checkbox-input') == true || elm.target.className == "ch-elem") {
          return;
        }

        if ($(this).attr("alt") == "Expand") {
          $(this).attr("alt", "Collapse");
          if ($('.analytics .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]').length > 0) {
            var expandedelem = $('.analytics .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
            applyFadeActionForExpandRow('', expandedelem, 'show');
          } else {
            $(this).next().attr('summary-pid', $(this).attr("pid"));
            var expandedelem = $('.analytics .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
            applyFadeActionForExpandRow('loader', expandedelem, 'show');
            getShopInfo($(this).attr("pId"), $(this).attr("shopName"), expandedelem, category);
          }
        } else {
          var expandedelem = $('.analytics .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
          applyFadeActionForExpandRow('', expandedelem, 'hide');
          $(this).attr("alt", "Expand");
        }
      });
    } // end of loop

    inLoop = false;

    applyFadeOutActionForLoader('metric-loading-animation', 'ebe-metric-value');

    var log_body = 'Finished process for showing data in table';
    log_body += "\n";
    log_body += 'Current location = ' + window.location.href;
    log_body += "\n";
    log_body += 'Done listing count = ' + dResponse.length;
    log_body += "\n";

    postSaveEverBeeProccessLogsToFile(log_body);

    var $results = $(".ebe-table-body.analytics");
    $results.removeData("loading");
    $('.ebe-table-body.analytics').removeClass('overflowRemove');

    $('.ebe-table-body.analytics [open-node="true"]').hover(
      function () {
        // $( this ).find('.ebe-table-row-arrow-icon').css("display", "flex");
      },
      function () {
        $(this).find('.ebe-table-row-arrow-icon').hide()
      }
    );


    $(".ebe-table-body.analytics").scroll(function (e) {
      var $this = $(this);
      var $results = $(".ebe-table-body.analytics");
      if (!$results.data("loading")) {
        $results.data('loading', true);
        isScroll = true;
        animation_action = 'show';
        getDataByCategory(isLogin, 'scroll');
        // $('.desc-demand-values').hide();
      }
    });

    debugger;

    animation_action = 'none';
    applySTDForProducts(priceForStdArr, avgsalesForStdArr, avgfavForStdArr, hasSearchCount);
  }
};

async function saveKeywordsTrends(trendsKeywords) {
  return new Promise((resolve) => {
    sendMessage({
      "command": "getSettings"
    }, function (dataResponse) {
      var uiSettings = dataResponse.uiSettings;
      uiSettings['trendsKeywords'] = decodeURI(trendsKeywords).replace(/\s/g, '+');;
      sendMessage({
        "command": "saveUISettings",
        "data": uiSettings
      }, function (dataResponse) {
        var uiSettings = dataResponse.uiSettings;
        console.log(uiSettings);
        resolve(true);
      });
    });
  });
};
