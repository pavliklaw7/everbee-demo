init()

async function init(){
    let url = new URL(location.href);
    var token = url.searchParams.get("token"), secret = url.searchParams.get("secret");

    fetch(`https://proud-luxurious-wandflower.glitch.me/getEmail?token=${token}&secret=${secret}`).then(async (resp) => {
        let data = await resp.json()
        await storageSet({key: 'userEmail', value: data.data.results[0]?.primary_email});
        sessionStorage.setItem('userEmail', data.data.results[0]?.primary_email);
        await storageSet({key: 'isLoginUser', value: true})

        await storageSet({key: config.keys.etsy_access_token, value: token});
        await storageSet({key: config.keys.etsy_access_secret, value: secret});

        var crede_obj={};
	
        crede_obj['email']=data.data.results[0]?.primary_email;
        crede_obj['etsy_access_token']=token;
        crede_obj['etsy_access_secret']=secret;
        let keys = {
            key: token,
            secret: secret
        }

        sendMessage({"command": "login", "data": {keys: keys, oauth_data: crede_obj}})
        location.href = "https://www.etsy.com/market/trending_now"
    })
}

function sendMessage(msg, callbackfn) {
    if(callbackfn!=null) {
      callback.push(callbackfn);
      msg.callback = "yes";
    }
    chrome.runtime.sendMessage(msg,callbackfn);
  }
  