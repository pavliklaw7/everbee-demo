function getShopInfo(id, shop_name, eleParent, category) {
  return new Promise((resolve, reject) => {
    var listingId = id;
    var shopName = shop_name;
    $.ajax({
      url: "https://www.etsy.com/shop/"+shopName,
      success: function( data ) {
        var headline=$(data).find('.trust-signals').find('[data-key="headline"]').get(0).innerText.trim();
        var location=$(data).find('.trust-signals').find('[data-key ="user_location"]').text().trim();
        var totalSales = $(data).find('.trust-signals').find('a[rel ="nofollow"]').text().trim();
        var estySince = $(data).find('.trust-signals').find('.etsy-since').text().trim();
        var totalReview =$(data).find('.trust-signals').find('.reviews-wrapper .total-rating-count').text().trim();
        var ratingval=$(data).find('.trust-signals').find('input[name="initial-rating"]').val();
        var shopImage=$(data).find('.trust-signals').find('img:eq(0)').attr('src');
        shopname = shopName;
        shopheadline = headline;
        rating = ratingval;
        var totalReview=findtotalReviewOfshop(data);
        if (totalReview.length) {
          ratingcount = totalReview[0].replace('(','').replace(')','').replace(' Results','').replace ( /[^\d]/g, '' );
        }else{
          ratingcount='';
        }
        if (location.indexOf(',')!==-1) {
          shopregion = location.split(',')[0];
          shopcity = location.split(',')[1];
        }else{
          shopcity = location;
          shopregion = '';
        }

        // totalSales=totalSales?totalSales:$(data).find('.trust-signals span:contains( Sales)').get(0).textContent.trim();
        shopsale = totalSales!=undefined?totalSales.split('Sales')[0]:'';
        ExactYear=estySince!=undefined?estySince.replace ( /[^\d.]/g, '' ):'';

        $(eleParent).find('a.shop-card.w-inline-block').attr('href',"https://www.etsy.com/shop/" + shopname);

           // $(eleParent).find('.rating-link').attr('href',"https://www.etsy.com/shop/"+shopname+"#reviews")

           if (!shopname == '') {

            $(eleParent).find('.shop-name').attr('shopUrl',"https://www.etsy.com/shop/" + shopname);

            $(eleParent).find('.shop-name').text(shopname)
          } else {
            $(eleParent).find('.shop-name').text('');
          }

          $(eleParent).find('.shop-reviews').attr('shopUrl',"https://www.etsy.com/shop/"+shopname+"#reviews")

          if (!ratingcount == '') {
            $(eleParent).find('.shop-review-count-text').text('('+String(numberWithCommas(parseInt(ratingcount)))+')');
          }
          else {
            $(eleParent).find('.shop-review-count-text').text('');
          }

          if (!shopheadline == '') {
            $(eleParent).find('.shop-title').text(shopheadline);
          }
          else {
            $(eleParent).find('.shop-title').text('');
          }

          if (!rating == '') {
            $(eleParent).find('.ShopRating').html(getStars(rating));
          }
          else {
            $(eleParent).find('.ShopRating').html('');
          }

          if (!ExactYear == '') {
            $(eleParent).find('.shop-details-text.year').text('On Etsy since '+ExactYear);
          }
          else {
            $(eleParent).find('.shop-details-text.year').text('');
          }

          if (!shopregion == '') {
            $(eleParent).find('.shop-details-text').find('.ShopRegion').text(shopregion + ", ");
          }else{
            $(eleParent).find('.shop-details-text').find('.ShopRegion').text('');
          }

          if (!shopcity == '') {
            $(eleParent).find('.shop-details-text').find('.ShopCity').text(shopcity);
          }else{
            $(eleParent).find('.shop-details-text').find('.ShopCity').text('');
          }
          if (shopregion=='' && shopcity=='' ) {
            $('.location-icon').hide();
            $('.location-seperator').hide();

          }else{
            $('.location-icon').show();
            $('.location-seperator').show();
          }
          if (!shopsale == '') {

            $(eleParent).find('.shop-details-text.ShopSale').text(shopsale+' Sales');
          }
          else {

            $(eleParent).find('.shop-details-text.ShopSale').text('');
          }
          GetProductInfoDetail(listingId, category, eleParent);
          resolve(true);

        },
        cache: true
      });
  });
}
