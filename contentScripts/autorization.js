var isLogin = false;
let minimize = true;

function openEtsyLoginModal() {
  // e.preventDefault();
  // trackButtonClickEverBeeExt(e);
  // applyFadeAction('ebe-onboarding','ebe-page-onbarding-signup');
  insertLogoImg();
  applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');

  // signUp();
  // login();
};

//   function signUp() {
//     document.getElementById('signupBtn').addEventListener('click', function(e){

//       var nameInput = document.getElementById('Name-3');
//       var emailInput = document.getElementById('Email-5');
//       var passwordInput = document.getElementById('Password-4');

//       var name = document.getElementById('Name-3').value;
//       var email = document.getElementById('Email-5').value;
//       var password = document.getElementById('Password-4').value;

//       if (
//         nameInput.validity.patternMismatch === false
//         && emailInput.validity.patternMismatch === false
//         && passwordInput.validity.patternMismatch === false
//         && name.trim() !== ''
//         && email.trim() !== ''
//         && password.trim() !== ''
//       ) {
//         console.log('start');
//         e.preventDefault();
//         sendMessage({"command": config.keys.signUp ,"data":{name: name, email: email, password: password}},function(dataResponse){
//           if(dataResponse.result.success){
//             isLogin = true;
//             localStorage.setItem(config.keys.userEmail, email);
//             document.getElementById('overlay').style.display = 'none';
//             localStorage.setItem('isLoginUser', true);
//             start(isLogin);
//           } else {
//             simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
//           }
//         });
//       } else {
//         console.log('non')      }
        
//     })
//  }

//  function login() {

//     document.getElementById('loginBtn').addEventListener('click', function log(e) {
//       var email = document.getElementById('Email-4').value;
//       var password = document.getElementById('password-3').value;

//       if (email.trim() !== '' && password.trim() !== '') {
//         e.preventDefault();
//         console.log('login');
//         sendMessage({"command": config.keys.login ,"data":{email: email, password: password}},function(dataResponse){
//           console.log(dataResponse.result);
//           if(dataResponse.result.success){
//             isLogin = true;
//             document.getElementById('overlay').style.display = 'none';
      
//             localStorage.setItem('isLoginUser', true);
//             localStorage.setItem(config.keys.etsy_access_token, dataResponse.result.data.token);
//             localStorage.setItem(config.keys.etsy_access_secret, dataResponse.result.data.secret);
//             localStorage.setItem(config.keys.userEmail, email);
//             document.querySelector('#menuLogOut span').innerText = config.values.logOut;
      
//             start(isLogin);
//             e.target.removeEventListener('click', log, false);
            
//           } else {
//             simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
//           }
//         });
//       }
//   });
// }

  
 
 function openLoginModal(e) {
  e.preventDefault();
  
  if (userEmail > 0) {
    return
  } else {
    applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-etsyconnect');
  }


  // applyFadeAction('ebe-onboarding','ebe-page-onbarding-login');
  // document.getElementById('loginBtn').addEventListener('click', function(e){
  //   e.preventDefault();
  //   login(e);
  // });
  // login();
}

// function openSignUpModal(e){
//   e.preventDefault()
//   applyFadeAction('ebe-onboarding','ebe-page-onbarding-signup');
//   signUp();
// }

function startEtsyTool(e) {
  e.preventDefault();
  document.querySelector('#menuLogOut .nav-text').innerText = config.values.logOut;

  document.querySelector('#main-everBee-ext').setAttribute('data-show', false);

  // sendMessageCustom('setSessionValue', 'trending_now');
}

// async function isSubscriptionActive() {
//   const userEmail = await storageGet('userEmail');

//   if (userEmail) {
//       sendMessage({ "command": "checkSubscription", "data": { userEmail: userEmail } }, (response) => {
//           console.log('checkSubscription', response.result.active);
//           if (!response.result.active) {
//               applyFadeAction('ebe-onboarding', 'ebe-page-onbarding-subscribe');
//               document.getElementById("active_subscription_btn").addEventListener('click', (e) => activeSubscription(e));
//           }
//       });
//   } 
// };


function connectEtsy(e) {
  e.preventDefault();

  if (e != undefined) {
    trackButtonClickEverBeeExt(e);
  }

 location.href = "https://proud-luxurious-wandflower.glitch.me/LoginEtsy"
};

async function getUserSetting() {
  return new Promise((resolve) => {
    (async () => {
      var token = await storageGet(config.keys.etsy_access_token);
      var secret = await storageGet(config.keys.etsy_access_secret);
      var email = await storageGet(config.keys.userEmail);

      var etsyObj={};
      if(token == undefined || token == '' || token == null){
        etsyObj['etsy_user_credential']={etsy_access_token:'',etsy_access_secret:''};
        etsyObj['estyDefaultCount']= 10;
        etsyObj['searchOverAction']= true;
        etsyObj['loginEmail']='';
      }
      else{
        etsyObj['etsy_user_credential']={etsy_access_token:token,etsy_access_secret:secret};
        etsyObj['estyDefaultCount']= 100;
        etsyObj['searchOverAction']= true;
        etsyObj['loginEmail']=email;
      }
      resolve(etsyObj);
    })();
  });
};

function insertLogoImg() {
  var imgURL = chrome.extension.getURL("images/output-onlinepngtools.png");
  var logo_images = document.querySelectorAll('.ebe-page-left-img');

  logo_images.forEach(element => {
    element.src = imgURL;
  });
};

let onEvent = true;

function start(isLogin) {
  // document.getElementById("signedInEmail").textContent = localStorage.getItem(config.keys.userEmail);

  // document.getElementById("connect_etsy_btn").removeEventListener('click', connectEtsy, false);
  // document.getElementById("strat_now_btn").removeEventListener('click', startEtsyTool, false);

  insertLogoImg();

  checkEtsyCredentialFromUserMeta().then(() => {

    (async () => {
      var member = '';
      var etsy_user_credential_obj = await getUserSetting();
      console.log('etsy_user_credential_obj', etsy_user_credential_obj);
      var searchOverAction = etsy_user_credential_obj.searchOverAction;
      var estyDefaultCount = etsy_user_credential_obj.estyDefaultCount;
      var loginEmail = etsy_user_credential_obj.loginEmail;
      if(loginEmail){
        await isSubscriptionActive()
      }
      var etsy_user_credential = etsy_user_credential_obj.etsy_user_credential;

      if ((etsy_user_credential.etsy_access_token != undefined || etsy_user_credential.etsy_access_token != null) && etsy_user_credential.etsy_access_token.length > 0) {

        var obj = {};
        obj['isLogin'] = isLogin;
        obj['action' ]= isLogin;
        fancyboxOpen(isLogin);
        //return false
        document.querySelector('#menuLogOut .nav-text').innerText = config.values.logOut;
        document.getElementById("signedInEmail").textContent = `Signed in account: ${await storageGet(config.keys.userEmail)}`;
      } else {
        fancyboxOpen(isLogin,'first')
      }

      if (onEvent) {
        document.getElementById("connect_etsy_btn").addEventListener("click", (e) => connectEtsy(e));
        document.getElementById("strat_now_btn").addEventListener("click", (e) => startEtsyTool(e)); // make the subscribe
        document.getElementById("get_started_btn").addEventListener("click", openEtsyLoginModal);
        document.getElementById("loginLinkModal").addEventListener("click", openLoginModal);
        // document.getElementById("signUplinkModal").addEventListener("click", openSignUpModal);
        document.getElementById("exportButtonAct").addEventListener("click", exportAction);
        document.getElementById("btn-g-trends").addEventListener("click", openGoogleTrends);
        onEvent = false;
      }

      $('body').off('change','#select-all-check-mark').on('change', '#select-all-check-mark', function (e) {
        e.preventDefault();
        var eleSelectAll = $(this).is(':checked');
        if (eleSelectAll == true) {
          $(this).addClass('w--redirected-checked');
        } else {
          $(this).removeClass('w--redirected-checked');
        }

        $('.ebe-table-expandable-row [name="checkbox-2"]').each(function(){
          if (eleSelectAll == true) {
            if (!$(this).prop('checked')) {$(this).click();}

          } else {
            $(this).prop('checked');
            if ($(this).prop('checked')) {$(this).click();}
          }
        });
      });
    })();
  });

  let ghost_footer = document.createElement('div');

  ghost_footer.classList.add('ghost-footer');
  document.getElementById("analyticsResizeBtn").onclick = resizeWindow;
  document.getElementById("favoritesResizeBtn").onclick = resizeWindow;
  document.getElementById("keywordResizeBtn").onclick = resizeWindow;
  document.getElementById("categoriesResizeBtn").onclick = resizeWindow;

  function resizeWindow() {
    if (minimize) {

      document.getElementById('main-everBee-ext').classList.add('everbee-minimized');
      window.setTimeout(function(){
        document.getElementById('main-everBee-ext').classList.add('everbee-minimized-top');
        document.querySelector("body").appendChild(ghost_footer);
      }, 200);
      
      localStorage.setItem(config.keys.everbeeWindowSize, 'min');

      document.querySelectorAll('.resizeBtn').forEach(btn => btn.innerText = 'Maximize');
      document.querySelectorAll('.resizeBtn').forEach(btn => btn.insertAdjacentHTML('beforeend', reziseMaximizeArrows));
      // this.innerText = "Maximize";
      // this.insertAdjacentHTML('beforeend', reziseMaximizeArrows);
    } else {
      document.getElementById('main-everBee-ext').classList.remove('everbee-minimized-top');
      document.getElementById('main-everBee-ext').classList.remove('everbee-minimized');
      document.querySelector("body").removeChild(document.querySelector(".ghost-footer"));

      document.querySelectorAll('.resizeBtn').forEach(btn => btn.innerText = 'Minimize');
      document.querySelectorAll('.resizeBtn').forEach(btn => btn.insertAdjacentHTML('beforeend', reziseMinimizeArrows));

     // this.innerText = "Minimize";
     // this.insertAdjacentHTML('beforeend', reziseMinimizeArrows);
      localStorage.setItem(config.keys.everbeeWindowSize, 'max');
    }

    minimize = !minimize;
  }
}


function fancyboxOpen(isLogin, type)  {

  $('.modal-dilog-everBee1').show();
  $('[data-show="false"]').attr('data-show','true');
  $('#main-everBee-ext').addClass('everbee-extension');
  if (type=='first' && isLogin != 'false' && isLogin != false) {
    applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');
  } else if (type=='first' &&!isLogin){
    applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');
  } else if (popOver_action=='login' || (isLogin != true && isLogin != 'true')){
    document.getElementById('overlay').style.display = 'none';
    // applyFadeAction('ebe-onboarding','ebe-page-onbarding-login');

    // let userEmail = await storageGet('userEmail');

    // console.log('userEmail', userEmail);

    if (isLogin) {
      applyFadeAction('ebe-onboarding','ebe-page-onbarding-etsyconnect');
    }

    // document.getElementById('loginBtn').addEventListener('click', function(e){
    //   e.preventDefault();
    //   login(e);
    // });

     // login();
  }else{
    if (animation_action != 'show') {
      return false;
    }
      getDataByCategory(isLogin,'load');

    $(function () {
      var inverse = false;
      $('body').off('click','.ebe-table-body .ebe-table-header-col-title .ebe-table-toggle-icon').on('click','.ebe-table-body .ebe-table-header-col-title .ebe-table-toggle-icon', function(elm){

        const rows = Array.from(document.querySelectorAll('.ebe-table-row-parent'));
        rows.forEach(row => {
          
          let isExpand = row.getAttribute('alt');

          if (isExpand != 'Expand') {
            let pid = row.getAttribute('pid');
            var expandedelem = $('.ebe-table-row-child[summary-pid="' + pid + '"]');
            applyFadeActionForExpandRow('', expandedelem, 'hide');
          }
      
        });


        var thIndex = $(this).closest('.ebe-table-header-col-title').index();
        var type = $(this).closest('.ebe-table-header-col-title').attr('sortby');
        var table = $(this).closest('.ebe-table-body');

        if (!(this.className == 'ch-box-th') && this.getAttribute('isPremium')!='yes') {
          table.find('.ebe-table-expandable-row [id^="w-node-"]').filter(function(){
            return $(this).index() === thIndex;
          }).sortElements(function(a, b){
            if(type == "numeric") {
              return parseFloat($(a).text().replace(/,/g,"").match(/[\d\.]+/)) < parseFloat($(b).text().replace(/,/g,"").match(/[\d\.]+/)) ? inverse ? -1 : 1 :
              inverse ? 1 : -1;
            } else if(type == "auto") {
              a = $(a).text();
              b = $(b).text();
              return (
                isNaN(a) || isNaN(b) ?
                a > b : +a > +b
                ) ?
              inverse ? -1 : 1 :
              inverse ? 1 : -1;
            } else {
              return $(a).text() > $(b).text() ? 1 : -1;
            }
          }, function(){
        // parentNode is the element we want to move
        return this.closest('.ebe-table-row-parent');
      });
          inverse = !inverse;
        }
      });

    });
  }
};

function openGoogleTrends(e) {
  e.preventDefault();
  trackButtonClickEverBeeExt(e);

  const iframe = document.getElementById("trendsHtml");

  iframe.src = chrome.extension.getURL("trends.html");
  iframe.frameBorder = 0;
  applyFadeAction('ebe-modals','ebe-page-trend');
  modal.querySelector(".ebe-page-trending-exit").addEventListener("click", () => {
      $.fancybox.close();
  });
};


function checkEtsyCredentialFromUserMeta() {
  return new Promise((resolve) => {
    var crede_obj = {};
    if (userMeta && userMeta.etsy_access_token != undefined && userMeta.etsy_access_secret != undefined && userMeta.etsy_access_token != '' && userMeta.etsy_access_secret != '') {
      crede_obj['etsy_access_token'] = userMeta.etsy_access_token;
      crede_obj['etsy_access_secret'] = userMeta.etsy_access_secret;
      updateEtsyCredential(crede_obj, "sender", function(){
        resolve();
      });
    } else {
      resolve();
    }
  });
};

function exportAction(e) {
  e.preventDefault();
  trackButtonClickEverBeeExt(e);
  exportTableToCSV(null, 'etsy-product.csv')
};

function exportTableToCSV(html, filename) {
  var csv = [];
  var rows = [];
  var Headers = 'listing title, listing link,shop Name,price,favorites,total sales,sales,revenue,tag1,tag2,tag3,tag4,tag5,tag6,tag7,tag8,tag9,tag10,tag11,tag12,tag13';
  csv.push(Headers);
  var trs = document.querySelectorAll(".ebe-table-expandable-row");
  var ii = 0;

  for (var i = 0; i < trs.length; i++) {
    if (trs[i].querySelector('input[type="checkbox"]').checked) {
      rows.push(trs[i]);
    }
  };

  if (rows.length == 0) {
    rows = trs;
    ii = 0;
  }

  for (ii; ii < rows.length; ii++) {
    var row = [],
    cols = rows[ii].querySelectorAll('[id^="w-node"]');

    for (var j = 2; j < cols.length - 1; j++) {
      if (j === 2) {
        var productTitle = cols[2].getElementsByTagName("pdt")[0].innerHTML;
        row.push('\"' + productTitle.trim() + '\"');

        var productlink = cols[2].getElementsByTagName("span")[0].innerHTML;
        row.push('\"' + productlink.trim() + '\"');
      }

      else if (j ===9) {

        var tagtxt = cols[2].getElementsByTagName("tspan")[0].innerHTML;
        var nameArr = tagtxt.split(',');
        var len = nameArr.length;
        for (var x = 0; x < len; x++) {
          row.push('\"' + nameArr[x].trim() + '\"');
        }
      }

      else {

        if ((cols[j].innerText.trim()).includes("$"))
          row.push('\"' + cols[j].innerText.trim().replace("$", "") + '\"');
        else {
          row.push('\"' + cols[j].innerText.trim() + '\"');

        }
      }
    }
    csv.push(row.join(","));
  }

  downloadCSV(csv.join("\n"), filename);
};

function downloadCSV(csv, filename) {
  var csvFile;
  var downloadLink;

  if (window.Blob == undefined || window.URL == undefined || window.URL.createObjectURL == undefined) {
    alert("Your browser doesn't support Blobs");
    return;
  }

  csvFile = new Blob([csv], { type: "text/csv" });
  downloadLink = document.createElement("a");
  downloadLink.download = filename;
  downloadLink.href = window.URL.createObjectURL(csvFile);
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);
  downloadLink.click();
};

async function updateEtsyCredential(data, sender, sendResponse) {
  console.log(data);
  // localStorage.setItem('isLoginUser', true);
  await storageSet({key: 'isLoginUser', value: true})

  await storageSet({key: config.keys.etsy_access_token, value: data.etsy_access_token})
  await storageSet({key: config.keys.etsy_access_secret, value: data.etsy_access_secret})

  sendMessage({"command": config.keys.saveTokens, "data": {email: await storageGet(config.keys.userEmail),
  token: await storageGet(config.keys.etsy_access_token), secret: await storageGet(config.keys.etsy_access_secret)}},function(dataResponse){
    console.log(dataResponse.result);
    sendResponse(true);
  });

  (async () => {
    let loggedInEmail = await general.get_data_by_key_value_pair_general('loginId', 1);
    if (loggedInEmail) {
      var email= loggedInEmail.email;
      // localStorage.setItem(config.keys.userEmail, email); //wtf
      let isUserExist = await idb.get_data_by_key_value_pair('email', email);
      if (isUserExist) {
        var uid= isUserExist.uid;
        var loginUserObj={"uid":uid,"email":email,"etsy_access_token":data.etsy_access_token,"etsy_access_secret":data.etsy_access_secret,"searchOverAction":false,"estyDefaultCount":10};
        await idb.insert_update_operation(loginUserObj);
        sendResponse(true);
      }
    }
  })();
};