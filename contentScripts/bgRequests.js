function checkInFav(listingId, userEmail) {
  return new Promise(async (resolve) => {
    sendMessage({"command": config.keys.checkInFav ,"data":{listingId: listingId, userEmail: userEmail}},function(dataResponse){
      resolve(dataResponse)
    });
  })
}

function checkInFolders(userEmail, folderName){
  return new Promise(async (resolve) => {
    if (folderName) {
      sendMessage({"command": config.keys.getFolderListings ,"data":{folderName: folderName, userEmail: userEmail}},function(dataResponse){
        resolve(dataResponse.result.data)
      });
    } else {
      sendMessage({"command": config.keys.checkInFolders ,"data":{userEmail: userEmail}},function(dataResponse){
        resolve(dataResponse)
      });
    }
  
  })
}

async function getFolderListings(folderName) {
 //  let userEmail = localStorage.getItem(config.keys.userEmail);
  let userEmail = await storageGet(config.keys.userEmail);
  return new Promise(async (resolve) => {
    sendMessage({"command": config.keys.getFolderListings ,"data":{folderName: folderName, userEmail: userEmail}},function(dataResponse){
      resolve(dataResponse.result.data)
    });
  })
}

async function getAllFavListings() {
  // let userEmail = localStorage.getItem(config.keys.userEmail);
  let userEmail = await storageGet(config.keys.userEmail);
  return new Promise(async (resolve) => {
    sendMessage({"command": config.keys.getAllFavListings ,"data":{userEmail: userEmail}},function(dataResponse){
      resolve(dataResponse.result.data)
    });
  })
}

// GetFavoritesIdsFromServer

async function getFavoritesIdsFromServer(folderName) {
  // let userEmail = localStorage.getItem(config.keys.userEmail);
  let userEmail = await storageGet(config.keys.userEmail);

  if (folderName) {
    return new Promise(async (resolve) => {
      sendMessage({"command": config.keys.getFavoritesIdsFromServer ,"data":{userEmail: userEmail, folderName: folderName}},function(dataResponse){
        resolve(dataResponse.result.data)
      });
    })
  } else {
    return new Promise(async (resolve) => {
      sendMessage({"command": config.keys.getFavoritesIdsFromServer ,"data":{userEmail: userEmail}},function(dataResponse){
        resolve(dataResponse.result.data)
      });
    })
  }
 
}