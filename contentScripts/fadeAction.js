const imgURLDialogBg = chrome.runtime.getURL("images/dialog-bg.png");
const imgURLDialogBgRed = chrome.runtime.getURL("images/dialog-bg-red.png");

function applyFadeAction(modalBox, targetModal) {
  applyFadeOutAction();
  setTimeout(function(){
    $('.'+targetModal).css("display", "flex").hide().fadeIn(300);
      //$('.'+targetModal).css('opacity',1).fadeIn(300);
      $('.'+targetModal).animate({
        opacity: 1
      }, 300);
      $('.'+modalBox).css("display", "flex").hide().fadeIn(300);
      //$('.'+modalBox).css('opacity',1).fadeIn("slow");
      $('.'+modalBox).animate({
        opacity: 1
      }, 300);
    },200)

    document.getElementById('ebe-page-new-folder__add-folder-bg').src = imgURLDialogBg;
    document.getElementById('ebe-page-add-to-folder__add-folder-bg').src = imgURLDialogBg;
    document.getElementById('ebe-page-delete-folder__add-folder-bg').src = imgURLDialogBgRed;
}

function applyFadeOutActionForLoader(loading,metric) {
  setTimeout(function(){
    //$('.'+loading).fadeOut(500);
    $('[class^="'+loading+'"]').fadeOut(500);
      //$('.'+).fadeOut(500);
      $('.'+metric).css("display", "flex").hide().fadeIn(300);
      $('.'+metric).animate({
        opacity: 1
      }, 300);

      $('.ebe-loading-btn').fadeOut(200);
      // $('#exportButtonAct').css("display", "flex").hide().fadeIn(100);
      $('#exportButtonAct').animate({
        opacity: 1
      }, 100);
    },200)
}

function applyFadeInActionForLoader(loading,metric){
  setTimeout(function(){
    $('.'+metric).fadeOut(1500);
    $('[class^="'+loading+'"]').css("display", "flex").hide().fadeIn(1300);
    $('[class^="'+loading+'"]').animate({
      opacity: 1
    }, 300);

    // $('#exportButtonAct').fadeOut(500);
    $('.ebe-loading-btn').css("display", "flex").hide().fadeIn(300);
    $('.ebe-loading-btn').animate({
      opacity: 1
    }, 300);
  },200)
}

function applyFadeOutAction() {
  $('.ebe-modals').fadeOut(500);
  $('.ebe-page-trend').fadeOut(500);
  $('.ebe-page-settings').fadeOut(500);
  $('.ebe-page-new-folder').fadeOut(500);
  $('.ebe-page-add-to-folder').fadeOut(500);
  $('.ebe-page-delete-folder').fadeOut(500);
  $('.ebe-page-welcome').fadeOut(500);;
  $('.ebe-page-onbarding-login').fadeOut(500);
  $('.ebe-onboarding').fadeOut(500);
  $('.ebe-page-onbarding-signup').fadeOut(500);
  $('.ebe-page-onbarding-etsyconnect').fadeOut(500);
  $('.ebe-page-onbarding-success').fadeOut(500);
  $('.ebe-page-onbarding-subscribe').fadeOut(500);

  document.querySelector('.main-menu').classList.remove('disabled');
}

function applyFadeActionForExpandRow(type,expandedelem,action,id) {
  setTimeout(function(){
    var expCon=$(expandedelem).find('.ebe-table-row-expanded-container');

    if (action=='show'&& type=='') {
      $(expandedelem).css("height", '265px')
      $(expandedelem).css("width", '1199.2px')
      $(expandedelem).css("display", "flex").hide().fadeIn(100);
      $(expCon).css("display", "flex").hide().fadeIn(100);
      $(expCon).animate({
        opacity: 1
      }, 300);
    }

    if (action=='hide'&& type=='') {
      $(expandedelem).css("height", '0px')
      $(expandedelem).css("display", "none").hide().fadeOut(100);
      $(expCon).css("display", "none").hide().fadeOut(100);
      $(expCon).animate({
        opacity: 0
      }, 300);
      return
    }

    var expConLoader=$(expandedelem).find('.ebe-table-row-expanded-loader');
    var loader__small=$(expandedelem).find('.ebe-table-row-expanded-loader').find('.loader__small');
    
    if (type == 'loader') {
      $(expandedelem).css("height", '265px')
      $(expandedelem).css("width", '1199.2px')
      $(expandedelem).css("display", "flex").hide().fadeIn(100);
      $(expConLoader).css("display", "flex").hide().fadeIn(100);
      $(loader__small).animate({
        opacity: 1
      }, 300);
      $(expConLoader).animate({
        opacity: 1
      }, 300);
      $(expCon).css("display", "flex").hide().fadeIn(200);
      $(expCon).animate({
        opacity: 1
      }, 400);
    }

    if (type=='loaderHide') {
      $(expConLoader).fadeOut(300);
      $(loader__small).animate({
        opacity: 0
      }, 300);
      $(expConLoader).animate({
        opacity: 0
      }, 300);
    }
  }, 200);
};

function applyFadeToExpandRows(rowId, dummy) {
  return new Promise((resolve) => {
    $(dummy).animate({opacity: 0.25},10);
    $(dummy).animate({opacity: 0}, 20);
    $('#'+rowId).animate({opacity: 0.25},30);
    $('#'+rowId).css("display", "block").hide().fadeIn();
    $('#'+rowId).animate(
    {
      opacity: 0.60
    },
    30,
    function() {
      $('#'+rowId).animate({
        opacity: 1
      }, 50);
    }
    );
    setTimeout(function(){resolve(true);})
  })
}
