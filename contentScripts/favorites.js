// let favIds;

// getFavoritesIds();

// async function getFavoritesIds() {
//     favIds = await getFavoritesIdsFromServer();
// }

let changeActiveFav = false;

function switchListingTopBtn() {
  const addFavToFolderButton = document.querySelector('#addFavToFolderButton');
  const deleteFavFolderButton = document.querySelector('#deleteFavFolderButton');
  const isAnyActiveFolder = Array.from(document.querySelector('.add-favorites__wrapper').children).some(slide => slide.classList.contains('active'));

  if (isAnyActiveFolder) {
    addFavToFolderButton.classList.add('hide');
    deleteFavFolderButton.classList.remove('hide');
  } else {
    addFavToFolderButton.classList.remove('hide');
    deleteFavFolderButton.classList.add('hide');
  }
};

//----------add-new folder-----------------//

function addNewFolder(folderName) {
  const slider = document.querySelector('.add-favorites__wrapper');
  const svg = generateFolderIcon();

  const isAnyDoubleFolder = Array.from(document.querySelector('.add-favorites__wrapper').children).some(slide => slide.querySelector('p').innerText.trim() === folderName.trim());

  if (!isAnyDoubleFolder) {
    if (slider.children.length) {
      // const leftDistance = +getComputedStyle(slider.lastChild).left.replace('px', '');

      slider.insertAdjacentHTML('beforeend', `<div class="add-favorites__slide" id="c${slider.childElementCount + 1}">
      ${svg}
      <p style="padding: 0 5px 0 4px">${folderName}</p>
      </div>`);
      // slider.lastChild.style.left = (leftDistance + 110) + 'px';
    } else {
      slider.insertAdjacentHTML('beforeend', `<div class="add-favorites__slide" id="c${slider.childElementCount + 1}">
      ${svg}
      <p style="padding: 0 5px 0 4px">${folderName}</p>
      </div>`);
    }

    if (slider.children.length > 3) {
      slidersOverDisplayed++;

      document.getElementById('add-favorites__next-slide').style.display = 'block';
    }

    applyFadeOutAction();
  }
};

function generateFolderIcon() {
  const colors = ['#F697B7', '#F6BE97', '#97CFF6', '#F198F5', '#043872', '#58B2F1', '#DC4E45', '#F19758', '#737373'];

  Array.prototype.random = function () {
    return this[Math.floor((Math.random() * this.length))];
  }

  const color = colors.random();

  const folderIcon = `<svg viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M33.3334 8.33333H19.0234L16.1784 5.48833C16.0238 5.33334 15.8401 5.21041 15.6379 5.1266C15.4357 5.0428 15.2189 4.99977 15 5H6.66671C4.82837 5 3.33337 6.495 3.33337 8.33333V31.6667C3.33337 33.505 4.82837 35 6.66671 35H33.3334C35.1717 35 36.6667 33.505 36.6667 31.6667V11.6667C36.6667 9.82833 35.1717 8.33333 33.3334 8.33333ZM6.66671 31.6667V11.6667H33.3334L33.3367 31.6667H6.66671Z" fill="${color}"/>
  </svg><svg style="display:none" width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M33.3334 8.33333H19.0234L16.1784 5.48833C16.0238 5.33334 15.8401 5.21041 15.6379 5.1266C15.4357 5.0428 15.2189 4.99977 15 5H6.66671C4.82837 5 3.33337 6.495 3.33337 8.33333V31.6667C3.33337 33.505 4.82837 35 6.66671 35H33.3334C35.1717 35 36.6667 33.505 36.6667 31.6667V11.6667C36.6667 9.82833 35.1717 8.33333 33.3334 8.33333ZM6.66671 31.6667V11.6667H33.3334L33.3367 31.6667H6.66671Z" fill="${color}"/>
  <path d="M37.942 17C39.3843 17 40.3524 18.4803 39.7743 19.8016L34.5243 31.8016C34.2058 32.5296 33.4866 33 32.692 33H6.80241C5.42942 33 4.4647 31.6483 4.91103 30.3498L9.03603 18.3498C9.31365 17.5422 10.0734 17 10.9274 17H37.942Z" fill="${color}"/>
  </svg>`;

  return folderIcon;
};

async function displayFavFolderListings(folder) {
  let folderName = folder.querySelector('p').innerText.trim();
  const folderListings = await getFolderListings(folderName);
  const folderListingsId = folderListings.length > 0 ? folderListings.map(listing => listing.ListingId) : [];
  const allFavListingOnList = Array.from(document.querySelectorAll('.ebe-table-body.favorites .ebe-table-expandable-row'));

  if (folderListingsId.length > 0) {

    allFavListingOnList.forEach(listing => {
      const listingId = listing.querySelector('.ebe-table-row-parent').getAttribute('pid');

      if (folderListingsId.includes(listingId)) {
        listing.style.display = 'block';
      } else {
        listing.style.display = 'none';
      }
    })
  } else {
    allFavListingOnList.forEach(listing => listing.style.display = 'none');
  }
};

async function displayAllFavListings() {
  const allFavListingOnList = Array.from(document.querySelectorAll('.ebe-table-body.favorites .ebe-table-expandable-row'));
  const allFavListings = await getAllFavListings();
  const allFavListingsIds = allFavListings.map(fav => fav.ListingId);

  for (let i = 0; i < allFavListingOnList.length; i++) {
    let favListingId = allFavListingOnList[i].querySelector('.ebe-table-row-parent').getAttribute('pid');

    if (allFavListingsIds.includes(favListingId)) {
      allFavListingOnList[i].style.display = 'block';
    } else {
      allFavListingOnList[i].style.display = 'none';
    }
  }
}

async function addExistingFavFolder() {
  const slider = document.querySelector('.add-favorites__wrapper');
  const svg = generateFolderIcon();
  let em = await storageGet(config.keys.userEmail)
  if(em){
      let s = await storageGet('isSubsribed');
      if(s){
        let resultInFolders = await checkInFolders(em);

        resultInFolders.result.forEach(folderName => addNewFolder(folderName));
      }
  }
}

$(document).off('click', '#add-new-folder').on('click', '#add-new-folder', () => {
  applyFadeAction('ebe-modals', 'ebe-page-new-folder');
  document.getElementById('new-folder-input').addEventListener("keydown", (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      document.getElementById('new-folder-create-btn').click();
    }
  });

  document.getElementById('new-folder-create-btn').onclick = addNewFavFolder;

  async function addNewFavFolder() {

    let folderNameInput = document.getElementById('new-folder-input');
    let folderName = folderNameInput.value;
    let userEmail = await storageGet(config.keys.userEmail);

    console.log('userEmail', userEmail);

    sendMessage({
      "command": config.keys.addNewFolder,
      "data": {
        folderName: folderName,
        userEmail: userEmail
      }
    }, (dataResponse) => {
      if (dataResponse.result.success) {

        addNewFolder(folderName);

        document.querySelector('#new-folder-input').value = '';

      } else {
        simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
      }
    });
  };

  document.getElementById('new-folder-cancel-btn').onclick = function () {
    applyFadeOutAction();
  };
});

let next = 1;

$(document).off('click', '#add-favorites__next-slide').on('click', '#add-favorites__next-slide', function () {
  const slider = document.querySelector('.add-favorites__wrapper');
  const sliderLength = slider.children.length;

  if (sliderLength <= 3) {
    return;
  }

  if (slidersOverDisplayed > 0) {
    slidersOverDisplayed -= 1;
    // Array.from(slider.children).forEach(slide => slide.style.transform = `translateX(-${next * 110}px)`);
    slider.style.transform = `translateX(-${next * 110}px)`
    next += 1;
    console.log('slidersOverDisplayed > ', slidersOverDisplayed);
  } else if (slidersOverDisplayed === 0) {
    slidersOverDisplayed = slider.children.length - 3;
    // Array.from(slider.children).forEach(slide => slide.style.transform = `translateX(0px)`);
    slider.style.transform = `translateX(0px)`
    console.log('slidersOverDisplayed > ', slidersOverDisplayed);
    next = 1;
  }
});

$(document).off('click', '.add-favorites__wrapper .add-favorites__slide').on('click', '.add-favorites__wrapper .add-favorites__slide', (e) => {
  e.preventDefault();

  const openFolder = (target) => {
    
    document.querySelectorAll('.add-favorites__wrapper .add-favorites__slide').forEach(element => {
      Array.from(element.children)[1].style.display = 'none';
      Array.from(element.children)[0].style.display = 'block';
    });
    $('.add-favorites__wrapper .add-favorites__slide').removeClass("active")


    target.classList.add('active');
    Array.from(target.children)[1].style.display = 'block';
    Array.from(target.children)[0].style.display = 'none';
    displayFavFolderListings(target);
  };

  const defultFolder = (target) => {
    if (target.classList.contains('active')) {
      target.classList.remove('active');
      Array.from(target.children)[1].style.display = 'none';
      Array.from(target.children)[0].style.display = 'block';
    }
  };

  if(e.target.nodeName == "DIV"){
    if (!e.target.classList.contains('active')) {
      openFolder(e.target);
      switchListingTopBtn();
    } else {
      defultFolder(e.target);
      switchListingTopBtn();
      displayAllFavListings();
    }
  }
  else{
    if (!e.target.closest("div").classList.contains('active')) {
      openFolder(e.target.closest("div"));
      switchListingTopBtn();
    } else {
      defultFolder(e.target.closest("div"));
      switchListingTopBtn();
      displayAllFavListings();
    }
  }
});

$(document).off('click', '#addFavToFolderButton').on('click', '#addFavToFolderButton', (e) => {
  e.preventDefault();

  const select = document.querySelector('#add-to-folder-select');
  const favoriteFolders = Array.from(document.querySelector('.add-favorites__wrapper').children);
  const favListing = Array.from(document.querySelectorAll('.ebe-table-body.favorites .w-checkbox-input.w-checkbox-input--inputType-custom.checkbox'));
  favListing.shift();
  const isAnyFavListingSelected = favListing.some(checkbox => checkbox.classList.contains('w--redirected-checked'));

  if (favoriteFolders.length === 0) {
    simpleNotify.notify("No folders created", "warning", 3);
  } else if (isAnyFavListingSelected === false) {
    simpleNotify.notify("No listings selected", "warning", 3);
  } else {
    addListingToFolder()
  }

  document.getElementById('add-to-folder-select').addEventListener("keydown", (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      document.getElementById('add-to-folder-confirm-btn').click();
    }
  });

  function addListingToFolder() {
    applyFadeAction('ebe-modals', 'ebe-page-add-to-folder');

    select.innerHTML = '';

    favoriteFolders.forEach(folder => {
      select.insertAdjacentHTML('beforeend', `<option class="add-to-folder-option" value=${folder.getAttribute('id')}>${folder.textContent.trim()}</option>`);
    });

    document.getElementById('add-to-folder-cancel-btn').onclick = function () {
      applyFadeOutAction();
    }

    confrmAddListing();
  };

  function confrmAddListing() {
    document.getElementById('add-to-folder-confirm-btn').addEventListener('click', async function addFavs(e) {

      // let userEmail = localStorage.getItem(config.keys.userEmail);
      let userEmail = await storageGet(config.keys.userEmail);

      const folderId = document.querySelector('#add-to-folder-select').value;
      const folderName = document.querySelector(`#${folderId}`).innerText;

      const favListingCheckedId = favListing.map(listing => {
        if (listing.classList.contains('w--redirected-checked')) {
          return listing.closest('.ebe-table-row-parent').getAttribute('pid');
        }
      }).filter(value => value !== undefined);

      sendMessage({
        "command": config.keys.addListingsToFavFolder,
        "data": {
          arrOfListingsId: favListingCheckedId,
          folderId: folderName,
          userEmail: userEmail
        }
      });

      applyFadeOutAction();
      e.target.removeEventListener('click', addFavs, false);
      Array.from(document.querySelectorAll(".favorites .w-checkbox input")).forEach((el) => {
        if(el.checked){
          el.click()
        }
      });
    });
  };
});

$(document).off('click', '#deleteFavFolderButton').on('click', '#deleteFavFolderButton', (e) => {
  e.preventDefault();
  
  applyFadeAction('ebe-modals', 'ebe-page-delete-folder');

  document.getElementById('delete-folder-confirm-btn').onclick = confrmDeleteFolder;

  document.getElementById('delete-folder-cancel-btn').onclick = function () {
    applyFadeOutAction();
  };

  async function confrmDeleteFolder() {
    let userEmail = await storageGet(config.keys.userEmail);
    const favoriteFolders = Array.from(document.querySelector('.add-favorites__wrapper').children);

    let folderToDeleteName;
    let folderToDeleteIndex;

    const folderToDelete = favoriteFolders.forEach((folder, i) => {
      if (folder.classList.contains('active')) {
        folderToDeleteIndex = i;
        folderToDeleteName = folder.querySelector('p').innerText;
      }
    });

    const slider = document.querySelector('.add-favorites__wrapper');

    document.querySelector('#add-favorites__next-slide').click();

    if (slider.children.length <= 4) {
      document.querySelector('#add-favorites__next-slide').style.display = 'none';
    }

    sendMessage({
      "command": config.keys.deleteFavFolder,
      "data": {
        folderName: folderToDeleteName,
        userEmail: userEmail
      }
    });

    favoriteFolders.forEach(folder => {
      if (folder.classList.contains('active')) {
        folder.remove();
      }
    });

    simpleNotify.notify("Folder has been deleted", "good", 3);
    applyFadeOutAction();
    switchListingTopBtn();
    displayAllFavListings();
  };
});

async function handleResponseAsyncFavoritesIds(data, category, searchedKeyword, load_from) {
  console.log('data from fav', data);

  let dResponse = data;

  if (data != undefined && data.length > 0) {
    applyFadeInActionForLoader('metric-loading-animation', 'ebe-metric-value');
    var hasSearchCount = 0;
    alllength = data.average_count_length;
    allSearchesLength = data.totalSearches != '' ? data.totalSearches : alllength;

    var isExistLoader = $('.ebe-table-body.favorites').find('.ebe-table-expandable-row').find('.ebe-table-row-parent-dummy.favorites');

    if (isExistLoader.length != 0) {
      $('.ebe-table-body.favorites').addClass('overflowRemove');
    }

    const allFavListingOnList = Array.from(document.querySelectorAll('.ebe-table-body.favorites .ebe-table-expandable-row'));

    console.log(allFavListingOnList);

    if (allFavListingOnList.length != 7) {
      let favListingIds = allFavListingOnList.map(listing => listing.querySelector('.ebe-table-row-parent').getAttribute('pid'));
      console.log(favListingIds);
    }

    for (var obj = 0; obj < dResponse.length; obj++) {
      let activeFolder = document.querySelector('.add-favorites__slide.active');

      if (changeActiveFav) {
        changeActiveFav = false;
        clearTable('favorites');
        obj = 0;
        // break;
      }

      if (activeFolder != null) {
        let favPriorityName = activeFolder.innerText;

        let priorityFavFolder = await getFavoritesIdsFromServer(favPriorityName);

        console.log('priorityFavFolder', priorityFavFolder);

        // handleResponseAsyncFavoritesIds(priorityFavFolder);
      }

      var productObjDetail = await getProductInfoAsync(dResponse[obj]['ListingId']);
      category = dResponse[obj]['Category']

      console.log('productObjDetail from fav', productObjDetail);

      await pushProductDetailForExpandOption(productObjDetail);

      if (productObjDetail.state == 'active') {
        var uniqPrice = productObjDetail['price'];
        var currencyCode = productObjDetail['currency_code'];
        var priceInt = uniqPrice.replace(/[^\d.]/g, '');
        uniqPrice = +uniqPrice;
        var currencySymbol = dResponse[obj]['CurrencySymbol'];
        id = productObjDetail.listing_id;
        var productTitleStr = !!productObjDetail.title ? productObjDetail.title : '';
        var title = productTitleStr.length > 25 ? productTitleStr.substr(0, 24) + '...' : productTitleStr;
        listingtitle = productTitleStr;
        tags = productObjDetail.tags.toString();
        var prodTags = tags.length > 10 ? tags.substr(0, 9) + '...' : tags;
        var thisAve = parseFloat(priceInt) || 0;
        priceForStdArr.push(thisAve);

        sum = (parseFloat(sum) + parseFloat(thisAve));
        averageprice = parseFloat((sum / alllength)).toFixed(2);
        // document.getElementById('Avgprice').innerHTML = averageprice;

        var original_creation_tsz = productObjDetail.original_creation_tsz;
        var d = new Date();
        var secondsEpoch = d.getTime() / 1000;
        var differenceInSeconds = parseInt(secondsEpoch - original_creation_tsz);
        var viewsrange = (parseFloat(productObjDetail.views) / 100);

        var avgfav = parseFloat(productObjDetail.num_favorers) || 0;
        avgfavForStdArr.push(avgfav);
        avgfavsum = avgfavsum + avgfav;

        averagefav = parseFloat(Math.round((avgfavsum / alllength))).toFixed(2)
        // document.getElementById('AvgFav').innerHTML = numberWithCommas(parseInt(averagefav));

        var lsViews = parseFloat(productObjDetail.views) || 0;
        var lsFavorers = avgfav;

        var newFormula = parseFloat(((-0.1152) + lsViews) * (0.031)) + parseFloat(lsFavorers * 0.047);
        var avgsalesDivi = (parseFloat(newFormula) / differenceInSeconds) || 0;
        var avgsales = parseFloat(avgsalesDivi * 86400 * 30).toFixed(2) || 0;
        if (avgsales >= 59) {
          avgsales = Math.log2(avgsales) * 10;
        }

        var estimated_total_sales = (-0.1152 + lsViews * 0.031 + lsFavorers * 0.047) || 0;
        estimated_total_sales = Math.round(estimated_total_sales) > 0 ? numberWithCommas(Math.round(estimated_total_sales)) : 0;

        avgsalesForStdArr.push(parseFloat(avgsales));
        avgsalesum = (parseFloat(avgsalesum) + parseFloat(avgsales));
        averagesale = parseFloat(Math.round((avgsalesum / alllength))).toFixed(2);

        estrevenues = (parseFloat(priceInt) * Math.round(avgsales)).toFixed(2);
        var prodImg = productObjDetail['Images'][0]['url_170x135'];
      } else {
        NotAvailableClass = 'NotAvailableClass';
        title = 'Product Unavailable';
        productObjDetail.url = '';
        productObjDetail.taxonomy_path = [];
        productObjDetail.category_path = [];
        uniqPrice = '';
        productObjDetail.num_favorers = '';
        productObjDetail.views = '';
        itemReview = '';
        estrevenues = 0;
        prodTags = '';
        tags = '';
        currencySymbol = ''
        prodImg = chrome.extension.getURL('broken.png');
      }
      let shopObjDetail = {};
      shopObjDetail['shopId'] = productObjDetail['Shop']['shop_id'];
      shopObjDetail['shopname'] = productObjDetail['Shop']['shop_name'];
      shopObjDetail['shopheadline'] = productObjDetail['Shop']['title'];
      var estSalesNumber = Math.round(avgsales) > 0 ? numberWithCommas(Math.round(avgsales)) : 0;
      let estRevNumber = Math.round(estrevenues) > 0 ? numberWithCommas(Math.round(estrevenues)) : 0;
      let uniqeIdForRow = myContentPage.uniqeId();



      let ad = document.createElement("div");
      if (dResponse[obj]["isAd"]) {
        let p = document.createElement('p');
        p.textContent = "AD";
        p.classList = 'ad-sign';
        ad.appendChild(p);
      }

      if (+estimated_total_sales < +estSalesNumber) {
        estimated_total_sales = estSalesNumber;
      }

      productObjDetail.url = productObjDetail.url.replace('everBeechromeextension', 'everbee');

      let resultInFav = await checkInFav(productObjDetail.listing_id, await storageGet(config.keys.userEmail));

      const resultListing = getExpandableRow(
        productObjDetail,
        uniqeIdForRow,
        resultInFav,
        shopObjDetail,
        prodImg,
        title,
        ad,
        listingtitle,
        tags,
        currencyCode,
        uniqPrice.toFixed(2),
        estimated_total_sales,
        estSalesNumber,
        estRevNumber
      );

      let dummyRow = $('.ebe-table-body.favorites').find('.ebe-table-expandable-row:eq(' + obj + ')').find('.ebe-table-row-parent-dummy');

      if (dummyRow.length != 0) {
        dummyRow.parent().after(resultListing);
        dummyRow.parent().remove();

        await applyFadeToExpandRows(uniqeIdForRow, dummyRow.parent());

      } else {
        $('.ebe-table-body.favorites').removeClass('overflowRemove');
        $('.ebe-table-body.favorites').append(resultListing);

        let isFolderOpen = Array.from(document.querySelectorAll('.add-favorites__slide')).some(el => el.classList.contains('active') == true);

        if (!isFolderOpen) {
          await applyFadeToExpandRows(uniqeIdForRow, dummyRow.parent())
        } else {
          const folders = Array.from(document.querySelectorAll('.add-favorites__slide'));
          const activeFolder = folders.find(el => el.classList.contains('active'));
          const activeFolderName = activeFolder.innerText;
          const folderListings = await getFolderListings(activeFolderName);
          const folderListingsIdArr = folderListings.map(listing => listing.ListingId);

          if (folderListingsIdArr.includes('' + productObjDetail.listing_id)) {
            await applyFadeToExpandRows(uniqeIdForRow, dummyRow.parent())
          }

        }
      }

      $('.ebe-table-body .favoritesBtn:not(.processed)').on('click', async (event) => {
        let listingId = event.target.closest(".ebe-table-row-parent").getAttribute('pId');
        let userEmail = await storageGet(config.keys.userEmail);

        console.log('userEmail from remove', userEmail);

        if (event.target.classList.contains('inFavChecked')) {
          sendMessage({
            "command": config.keys.removeFavorite,
            "data": {
              listingId: listingId,
              userEmail: userEmail
            }
          }, function (dataResponse) {
            if (dataResponse.result.success) {
              console.log(dataResponse.result);
              let star = document.querySelector(".analytics").querySelector(`div.ebe-table-row-parent[pid="${listingId}"] .inFavChecked`)
              star?.classList.remove("inFavChecked")
              star?.classList.add("inFavUnchecked")
              event.target.closest(".ebe-table-expandable-row").remove()
            } else {
              simpleNotify.notify(dataResponse.result.message, dataResponse.status, 3);
              console.log(dataResponse.result);
            }
          });
        }
      });

      $('.ebe-table-body.favoritesBtn:not(.processed)').addClass("processed")

      if ((obj + 1) == (dResponse.length)) {
        $('.ebe-table-body.favorites').find('.ebe-table-expandable-row').find('.ebe-table-row-parent-dummy.favorites').parent().remove();
      }
    } // end of loop

    inLoop = false;

    var log_body = 'Finished process for showing data in table';
    log_body += "\n";
    log_body += 'Current location = ' + window.location.href;
    log_body += "\n";
    log_body += 'Done listing count = ' + dResponse.length;
    log_body += "\n";

    postSaveEverBeeProccessLogsToFile(log_body);

    var $results = $(".ebe-table-body.favorites");
    $results.removeData("loading");
    $('.ebe-table-body.favorites').removeClass('overflowRemove');
    applyFadeOutActionForLoader('metric-loading-animation', 'ebe-metric-value');

    $('body').off('click', '.ebe-table-body.favorites [open-node="true"]').on('click', '.ebe-table-body.favorites [open-node="true"]', function (elm) {
      if (elm.target.className == 'premetagTd' || elm.target.classList.contains('favoritesBtn')) {
        return false;
      }
      if (elm.target.classList.contains('w-checkbox-input') == true || elm.target.className == "ch-elem") {
        return;
      }
      var allExpandedelem = $('.ebe-table-body.favorites [open-node="true"]');
      var clickedId = $(this).attr("pid");

      $(allExpandedelem).each(function () {
        var currid = $(this).attr("pid");
        if ($(this).attr('alt') == 'Collapse' && clickedId != currid) {
          $(this).attr('alt', 'Expand');
          var expandedelem = $('.favorites .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
          $(expandedelem).hide();
        }
      });

      if ($(this).attr("alt") == "Expand") {
        $(this).attr("alt", "Collapse");
        if ($('.favorites .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]').length > 0) {
          var expandedelem = $('.favorites .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
          applyFadeActionForExpandRow('', expandedelem, 'show');
        } else {
          $(this).next().attr('summary-pid', $(this).attr("pid"));
          var expandedelem = $('.favorites .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
          applyFadeActionForExpandRow('loader', expandedelem, 'show');
          getShopInfo($(this).attr("pId"), $(this).attr("shopName"), expandedelem, category);
        }
      } else {
        var expandedelem = $('.favorites .ebe-table-row-child[summary-pid="' + $(this).attr("pid") + '"]');
        applyFadeActionForExpandRow('', expandedelem, 'hide');
        $(this).attr("alt", "Expand");
      }
    });

    $('.ebe-table-body.favorites [open-node="true"]').hover(
      function () {
        // $( this ).find('.ebe-table-row-arrow-icon').css("display", "flex");
      },
      function () {
        $(this).find('.ebe-table-row-arrow-icon').hide()
      }
    );


    $(".ebe-table-body").scroll(function (e) {
      var $this = $(this);
      var $results = $(".ebe-table-body");
      if (!$results.data("loading")) {
        $results.data('loading', true);
        isScroll = true;
        animation_action = 'show';
        getDataByCategory(isLogin, 'scroll');
        // $('.desc-demand-values').hide();
      }
    });

    debugger;

    animation_action = 'none';
    applySTDForProducts(priceForStdArr, avgsalesForStdArr, avgfavForStdArr, hasSearchCount);
  }
};