function sendMessage(msg, callbackfn) {
  if (callbackfn != null) {
    callback.push(callbackfn);
    msg.callback = "yes";
  }
  chrome.runtime.sendMessage(msg, callbackfn);
};

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

function roundNumbers(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

// function median(values) {
//   values.sort(function(a,b){
//     return a-b;
//   });
//   var half = Math.floor(values.length / 2);

//   if (values.length % 2)
//     return values[half];
//   else
//     return (values[half - 1] + values[half]) / 2.0;
// }

function standardDeviation(values){
  var avg = average(values);

  var squareDiffs = values.map(function(value){
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });

  var avgSquareDiff = average(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

function setProdImage(prodImage,productUrl,eleParent) {
  $(eleParent).find('.ebe-table-row-expanded-image-section img').attr('src', prodImage);
  $(eleParent).find('.ebe-table-row-expanded-image-section').attr('productUrl', productUrl);
}

function copySingleTag(r) {
  var from = r;
  var ff = r.innerHTML;
  var range = document.createRange();
  window.getSelection().removeAllRanges();
  range.selectNode(from);
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
    //r.style.color = 'green';
    r.innerHTML = '<div style="display:flex;opacity:1;" class="listing-copied-state"><div class="copied-text">copied!</div></div>'+ff;
    setTimeout(function () {
      r.innerHTML = ff;
    }, 1500);
};

function findtotalReviewOfshop(data) {
  var links = [];
  
  $(data).find('.display-inline-block.vertical-align-middle').each(function (k, v) {
    if (/\(.*\)/g.test($(this).text().trim())) {
     links.push($(this).text());
   }
 });
  return links;
}

function getStars(rating) { //listing

  // Round to nearest half
  rating = Math.round(rating * 2) / 2;
  let output = [];

  // Append all the filled whole stars
  for (var i = rating; i >= 1; i--)
    output.push('<i class="fa fa-star" aria-hidden="true" style="color: black;"></i>&nbsp;');

  // If there is a half a star, append it
  if (i == .5) output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: black;"></i>&nbsp;');

  // Fill the empty stars
  for (let i = (5 - rating); i >= 1; i--)
    output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: black;"></i>&nbsp;');

  return output.join('');
}

async function applySTDForProducts(priceForStdArr, avgsalesForStdArr, avgfavForStdArr, hasSearchCount) {
  const userCurrency = await storageGet('userCurrency');

  debugger;

  (async () => {
    if (priceForStdArr.length!=0 && avgsalesForStdArr.length!=0 && avgfavForStdArr.length!=0  ) {
      var finalAveragesale;
      var finalAveragefav;
      var priceMEAN = average(priceForStdArr);
      var priceSTD = standardDeviation(priceForStdArr);
      var priceCalArr=[];

      for (var price = 0; price < priceForStdArr.length; price++) {
        if (!((priceForStdArr[price] > (priceMEAN + (priceSTD * 2)))|| (priceForStdArr[price] < (priceMEAN - (priceSTD * 2))))) {
          priceCalArr.push(priceForStdArr[price]);
        }
      }

      if (priceCalArr.length !=0) {
        var typicalPrice = average(priceCalArr);
        if (typicalPrice != NaN) {
          document.getElementById('Avgprice').innerHTML = typicalPrice.toFixed(2) + ' ' + userCurrency;
        }
      }

      var favMEAN = average(avgfavForStdArr);
      var favSTD = standardDeviation(avgfavForStdArr);
      var favCalArr=[];

      for (var fav = 0; fav < avgfavForStdArr.length; fav++) {
        if (!((avgfavForStdArr[fav] > (favMEAN + (favSTD * 2)))|| (avgfavForStdArr[fav] < (favMEAN - (favSTD * 2))))) {
          favCalArr.push(avgfavForStdArr[fav]);
        }
      }

      if (favCalArr.length !=0) {
        var typicalFav = average(favCalArr);
        finalAveragefav=typicalFav;
        if (typicalFav != NaN){
          document.getElementById('AvgFav').innerHTML = numberWithCommas(parseInt(typicalFav));

        }
      }

      if (avgsalesForStdArr.length!=0) {
        // var averagesale = median(avgsalesForStdArr);
        var averagesale = average(avgsalesForStdArr);
        finalAveragesale=averagesale;
        document.getElementById('Avgsales').innerHTML = numberWithCommas(parseInt(averagesale));
      }

      if (hasSearchCount!=0) {
        var averagesaleCount=parseInt(finalAveragesale);
        var avgSale_Fav=roundNumbers(parseFloat(averagesaleCount/finalAveragefav),3);
        var score_count=await getTotalScoreResult(averagesaleCount,avgSale_Fav,hasSearchCount);
        document.getElementById('AvgScore').innerHTML = score_count;
        setDescTextDemand(descDemandCount, descCompCount)
      } else {
        $('.ebe-score').hide();
      }
    }
  })();
}

function getDemandAndComp(arr, val, cond) {
  var finalArr = [];
  var pointsArr = [];
  var condType = 'max';

  arr.filter(function(v){
    if (cond != '<=') {
      v.Criteria <= val ? finalArr.push(v.Criteria) : finalArr
    } else {
      condType = 'min';
      v.Criteria >= val || (val >= 100000 &&  v.Criteria == 100000 ) ? finalArr.push(v.Criteria):finalArr;
    }
  })

  var finalOutput=condType=='max'?Math.max.apply(null, finalArr):Math.min.apply(null, finalArr);
  arr.filter(function(v){
    v.Criteria === finalOutput ? pointsArr.push(v.Points):pointsArr
  });

  return pointsArr[0];
};

function setDescTextDemand(demand,comp) {
  console.log('demand>>>'+demand+ '&&  comp >>>>'+comp);
  $('[class^="ebe-score-icon-"]').hide();
  if (demand!=undefined) {
    if (demand < 1.88) {
      $(".demand-score .ebe-toggle-text").text('Low demand');
    }else  if(demand >= 1.88  && demand < 3.76 ){
      $(".demand-score .ebe-toggle-text").text('Medium demand');
    }
    else {
      $(".demand-score .ebe-toggle-text").text('High demand');
    }
  }
  if (comp!=undefined) {
    if (comp >= 3.71) {
      $(".competition-score .ebe-toggle-text").text('Low competition');

    }else  if(comp < 3.71 && comp >= 2.12 ){
      $(".competition-score .ebe-toggle-text").text('Medium competition');
    }
    else{
      $(".competition-score .ebe-toggle-text").text('High competition');
    }
  }
  // $('.desc-demand-values').css("display", "block").hide().fadeIn(600);
  //competition
};

function getTotalScoreResult(averagesale,avgSale_Fav,totalresult_count) {
  return new Promise((resolve) => {

    var totalDemandPoints=getDemandAndComp(MY_SCORE.DEMANDPOINTS_SAlES,averagesale,'>=');
    descDemandCount=totalDemandPoints;
    var totalCompetitionPoints=getDemandAndComp(MY_SCORE.COMPETITIONPOINTS_SALES,parseFloat(avgSale_Fav),'>=');
    var totalResults=getDemandAndComp(MY_SCORE.COMPETITIONPOINTS_TOTALRESULTS,totalresult_count,'<=');

    var totalPointsCount = totalDemandPoints +totalCompetitionPoints + totalResults;

    var totalPoints=parseFloat(Math.round(totalPointsCount));

    descCompCount=roundNumbers(parseFloat(totalCompetitionPoints+totalResults),3);

    resolve(totalPoints);
  })
};