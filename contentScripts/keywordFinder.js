let mostSearchedTags = [
  'personalized gifts for mom',
  'mothers day gift',
  'necklaces',
  'gift box for women',
  'rings',
  'home decor',
  'personalized gifts',
  'moldavite',
  'animal crossing',
  'crysta',
];
var inputValue;
var clicks;
var gfavorites;
let KeywordLimit = 10;

let counter = 0;


chrome.runtime.onMessage.addListener((message) => {
    switch (message.command) {
      case 'showRequestedKeywords':
        console.log('from keywordSearch', message.data);
        break;
    }
});


function callEtsyKeywordFinderApi(searchTerm) {
  return new Promise(async (resolve) => {
    sendMessage({"command": "callEtsyApi", "data": searchTerm}, (response) => {
      console.log('callEtsyApi', response.result.apiResponse.results);
      resolve(response.result.apiResponse)
    });
  })
};

function callEtsyKeywordFinderApi2(searchTerm) {
  return new Promise(async (resolve) => {
    sendMessage({"command": "callEtsyApi", "data": searchTerm}, (response) => {
      console.log('callEtsyApi2', response.result.apiResponse.results);
      resolve(response.result.apiResponse)
    });
  })
};

async function initMostSearchedTags(mostSearchedTagsArr) {
    document.querySelector('.table-loader').classList.add('active');

    const dataForMostSearchedArr = await makeTagpropertiesTable(mostSearchedTagsArr);
    await showKeywordResult(dataForMostSearchedArr);
    document.querySelector('.table-loader').classList.remove('active');
    // document.querySelector('.tags-rating__body');
};

async function initButtons() {
  let input = document.getElementById("search-keyword-input");
   
  if (document.querySelector('.tags-rating__body').children.length === 0) {
    initMostSearchedTags(mostSearchedTags);
  }

  input.addEventListener('keydown', (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      document.getElementById('search-keyword').click();
    }
  });

  document.getElementById("search-keyword").addEventListener('click', () => findTagProperties(input.value));
};

async function findTagProperties(value) {
  // requestBackground(new ExtensionMessage('abortEtsyApi'));

  clearKeywordTable();

  document.querySelector('keywordResearch')
  document.querySelector('#search-keyword-input').disabled = true;
  document.querySelector('.tags-rating__title').innerText = 'Keyword finder result:'

  $(".metric-keyword-loading-animation").removeClass('d-none');
  $(".metric-keyword-loading-animation-category").removeClass('d-none');
  $(".ebe-metric-keyword").text("");

  await callEtsyKeywordFinderApi2(value, 'overview-section',0).then( async (res) => {
    $(".desc-demand-values").hide();
    document.getElementById('overview-keyword').innerHTML = value;

    findCategoryEtsyKeywordFinderApi(value);
    
    let list = res.results || {};

    if (list.length == 0) {
      return false;
    }
    
    var count = res.count || 0;
    console.log('list', list);


    const allTagsArr = calculateKeywordsValuesSection(list, count, 'keyFromInput');

    const similarKeywordsObg = await calculateSimilarKeywordsAction(0, 10, allTagsArr.allTagsArr);

    console.log('similarKeywordsObg', similarKeywordsObg);

    $(".metric-keyword-loading-animation").addClass('d-none');

    calculateCTR(similarKeywordsObg.arrOfEntries, 'keyFromInput')

    const dataForTableArr = await makeTagpropertiesTable(similarKeywordsObg, 'fromInput');
    await showKeywordResult(dataForTableArr);
    document.querySelector('.table-loader').classList.remove('active');
  });
};

function calculateKeywordsValuesSection(allProducts, comp, source) {

  console.log('allProducts', allProducts);
  var active_list_arr = [];

  var initSettingsvalues = {
    searchTerm:'',
    isLogin : false,
    userMeta : {},
    allTagsArr : [],
    priceForStdArr : [],
    salesForStdArr : [],
    avgShopsCompetingArr : [],
    avgProcessingMaxArr : [],
    avgviewsForStdArr : [],
    avgfavForStdArr : []
  };
  
  if (allProducts.length > 0) {
    for (var i5 = 0; i5 < allProducts.length; i5++) {
      var productObjDetail = allProducts[i5];

      var original_creation_tsz = productObjDetail.original_creation_tsz;
      var sEpoch = (+new Date()) / 1000;
      var listing_age = ( sEpoch - original_creation_tsz) / 86400;
      listing_age = parseFloat(listing_age);

      if (listing_age < 30) {
          listing_age = 30;
      }
      var results_views = parseFloat(productObjDetail.views) || 0 ;
      var results_num_favorers = parseFloat(productObjDetail.num_favorers) || 0;

      var views = results_views / listing_age * 30;
      views = parseFloat(views) || 0;
      var favorites = results_num_favorers / listing_age * 30;
      favorites = parseFloat(favorites) || 0;

      var sales = 0;1
      sales = (-0.1152 + (results_views * 0.031) + (results_num_favorers * 0.047)) / listing_age * 30;
      sales = parseFloat(parseFloat(sales).toFixed(2));

      if (sales < 0 ) {
          sales = 0;
      }

      productObjDetail.sales = sales;
      productObjDetail.avgViews = views;
      productObjDetail.avgFav = favorites;

      active_list_arr.push(productObjDetail);

      // console.log('initSettingsvalues.arr', initSettingsvalues.allTagsArr);
      addTagsArr(productObjDetail.tags);
    }
  }

  active_list_arr.sort((a, b) => b.sales - a.sales);
  active_list_arr = active_list_arr.slice(0,100);

  for (var i3 = 0; i3 < active_list_arr.length; i3++) {
    var productObjDetail = active_list_arr[i3];
    var uniqPrice = productObjDetail.price;
    var currency_code = productObjDetail.currency_code;
    var original_creation_tsz = productObjDetail.original_creation_tsz;

    if (currency_code == "USD") {
      var itemPrice = parseFloat(uniqPrice) || 0;
      var finalPrice = itemPrice;
      initSettingsvalues.priceForStdArr.push(parseFloat(finalPrice));
    }

    var avgViews = productObjDetail.avgViews || 0;
    initSettingsvalues.avgviewsForStdArr.push(parseFloat(avgViews));

    var avgFav = productObjDetail.avgFav || 0;
    initSettingsvalues.avgfavForStdArr.push(parseFloat(avgFav));

    var sales = productObjDetail.sales || 0;
    initSettingsvalues.salesForStdArr.push(parseFloat(sales));

    var user_id = productObjDetail.user_id || 0;
  
    if (initSettingsvalues.avgShopsCompetingArr.indexOf(user_id) == -1) {
        initSettingsvalues.avgShopsCompetingArr.push(user_id);
    }

    var processing_max = productObjDetail.processing_max || 0;
    initSettingsvalues.avgProcessingMaxArr.push(processing_max);
  }

  function addTagsArr(tagsArr) {
    if (tagsArr.length > 0) {
    var tagsObject = tagsArr.map(item => {
      var obj = {};
      obj['tag'] = item;
      
      return obj;
    });
  } else {
    return false;
  }
  
    initSettingsvalues.allTagsArr.push(tagsObject);
  };

  if (source) {
    applySTDForProductsKeyword(
      initSettingsvalues.priceForStdArr,
      initSettingsvalues.salesForStdArr,
      initSettingsvalues.avgfavForStdArr,
      initSettingsvalues.avgviewsForStdArr,
      initSettingsvalues.avgShopsCompetingArr,
      initSettingsvalues.avgProcessingMaxArr,
      initSettingsvalues.allTagsArr,
      comp
    );
  }

  return calcMetrics(
    initSettingsvalues.priceForStdArr,
    initSettingsvalues.salesForStdArr,
    initSettingsvalues.avgfavForStdArr,
    initSettingsvalues.avgviewsForStdArr,
    initSettingsvalues.avgShopsCompetingArr,
    initSettingsvalues.avgProcessingMaxArr,
    initSettingsvalues.allTagsArr,
    comp
  );
};

function average(values) {
  var allvaluesTotal = values.reduce(function(a, b){
    
    return parseFloat(a) + parseFloat(b);
  }, 0);

  return allvaluesTotal / values.length;
}

function sumFunction(values){
    var allvaluesTotal = values.reduce(function(a, b){
        return parseFloat(a) + parseFloat(b);
    }, 0);
    return allvaluesTotal;
}

function applySTDForProductsKeyword ( // only for inputSearch
  priceForStdArr,
  salesForStdArr,
  avgfavForStdArr,
  avgviewsForStdArr,
  avgShopsCompetingArr,
  avgProcessingMaxArr,
  allTagsArr,
  comp
){

  let tagInfo = calcMetrics(
    priceForStdArr,
    salesForStdArr,
    avgfavForStdArr,
    avgviewsForStdArr,
    avgShopsCompetingArr,
    avgProcessingMaxArr,
    allTagsArr,
    comp
  );
  
  $(".ebe-metric-value").css("display", "block");
  $(".ebe-metric-value").css("opacity", "1");

  document.getElementById('kf-rs-competition').innerHTML = tagInfo.comp;
  document.getElementById('kf-rs-price').innerHTML = tagInfo.price;
  document.getElementById('kf-rs-favorites').innerHTML = tagInfo.favorites;
  document.getElementById('kf-rs-views').innerHTML = tagInfo.clicks;
  document.getElementById('kf-rs-sales').innerHTML = tagInfo.sales;
  document.getElementById('kf-rs-processing-time').innerHTML = tagInfo.avgProcessing;
  document.getElementById('kf-rs-shops-competing').innerHTML = tagInfo.shopsCompeting;
}

function calcMetrics(
  priceForStdArr,
    salesForStdArr,
    avgfavForStdArr,
    avgviewsForStdArr,
    avgShopsCompetingArr,
    avgProcessingMaxArr,
    allTagsArr,
    comp
  ) {

  let tagInfo = {};

  tagInfo.allTagsArr = allTagsArr;

  if (parseInt(comp) > 50000) {
    comp = 50000;
    comp = numberWithCommas(Math.round(comp));
    comp += '+';
  } else {
    comp = numberWithCommas(Math.round(comp));
  }

  if (comp.length > 5) {
    let formattedComp = comp.slice(0, 2) + 'k+';
    tagInfo.comp = formattedComp;
  } else {
    tagInfo.comp = comp;
  }


  var averagePrice = 0.0;
  if (priceForStdArr.length != 0) {
      averagePrice = average(priceForStdArr);
  }

  let formattedPrice = '$' + numberWithCommas(parseFloat(averagePrice).toFixed(2));
  tagInfo.price = formattedPrice;


  var averageFav = 0;
  if (avgfavForStdArr.length != 0) {
      averageFav = sumFunction(avgfavForStdArr);
      gfavorites = averageFav
  }
  
  let formattedFavorites = numberWithCommas(Math.round(averageFav))
  tagInfo.favorites = formattedFavorites

  var averageViews = 0;
  if (avgviewsForStdArr.length!=0) {
      averageViews = sumFunction(avgviewsForStdArr);
  }

  var views = numberWithCommas(Math.round(averageViews));
  clicks = views;
  tagInfo.clicks = clicks;

  var averagesale = 0;
  if (salesForStdArr.length!=0) {
      averagesale = sumFunction(salesForStdArr);
  }

  let sales  = numberWithCommas(parseInt(averagesale))
  tagInfo.sales = sales;

  var avgProcessingMax = 0;
  if (avgProcessingMaxArr.length != 0) {
      avgProcessingMax = average(avgProcessingMaxArr);
  }

  let avgProcessing = numberWithCommas(parseInt(avgProcessingMax));
  tagInfo.avgProcessing = avgProcessing;

  var avgShopsCompeting = 0;
  if (avgShopsCompetingArr.length!=0) {
      avgShopsCompeting = avgShopsCompetingArr.length;
  }

  let shopsCompeting = numberWithCommas(parseInt(avgShopsCompeting));
  tagInfo.shopsCompeting = shopsCompeting;

  return tagInfo;
}


async function findCategoryEtsyKeywordFinderApi(searchTerm) {
    callEtsyKeywordFinderApi2(searchTerm,'overview-category-section',0).then(cat_res => {
        var cat_res_list = cat_res.results || [];

        if (cat_res_list.length > 0) {
            let overview_main_category = "";
            for (let i = 0; i < cat_res_list.length; i++) {
                var productObjDetail = cat_res_list[i];
                if (1) {
                    try{
                        overview_main_category = productObjDetail.category_path[0];
                    } catch(err) {
                        if(typeof(productObjDetail.taxonomy_path)!="undefined")
                            overview_main_category = productObjDetail.taxonomy_path[0];
                        else
                            overview_main_category = "";
                    }
                }
            }
            document.getElementById('kf-rs-category').innerHTML = overview_main_category;
            $(".metric-keyword-loading-animation-category").addClass('d-none');
        }

        return cat_res_list;
    });

    let input = document.getElementById("search-keyword-input");

    input.disabled = false;
    input.value = "";
}

// grap all listings by tag and divide sum of views in all listings / all listing
 
function calculateCTR(arrOfEntries, action) {
  const adjustedClicks = +clicks.replace(',', '');
  const adjustedFavorites = Math.round(gfavorites);

  const ctr = (adjustedFavorites / adjustedClicks * 100) < 1
  ? 1
  : (adjustedFavorites / adjustedClicks * 100) > 10 
  ? 9.7
  : adjustedFavorites / adjustedClicks * 100;

  const highestEntries = arrOfEntries[0] > 10 ? 10 : arrOfEntries[0];
  const adjustedCtr =  Math.round(ctr * highestEntries);
  const searches = numberWithCommas(Math.round((adjustedClicks / adjustedCtr * 100)));

  if (action) {
    document.getElementById('kf-rs-ctr').innerHTML = adjustedCtr + '%';
    document.getElementById('kf-rs-searches').innerHTML = searches;
  }

  return {ctr: adjustedCtr, searches: searches};
};

function calculateSimilarKeywordsAction(start, limit, allTagsArr) {

  var flatTagsArr = allTagsArr.reduce((acc, it) => [...acc, ...it]);
    const groupByTagsObj = flatTagsArr.reduce((acc, it) => {
        var tag_str = String(it.tag);
        tag_str = tag_str.toLowerCase().replace(/\b[a-z]/g, (letter) => {
            return letter.toUpperCase();
        });
        acc[tag_str] = acc[tag_str] + 1 || 1;
        return acc;
    }, {});

    var sortableValues = [];
    for (var max in groupByTagsObj) {
        sortableValues.push([max, groupByTagsObj[max]]);
    }

    sortableValues.sort(function(a, b) {
        var occurences_a = a[1];
        var occurences_b = b[1];
        var occurences_ASC = occurences_b - occurences_a;
        return occurences_ASC;
    });

    for (var $index = 0; $index < sortableValues.length; $index++) {
        sortableValues[$index]['$index'] = $index;
    }

    var finalArr = sortableValues.slice(parseInt(start), parseInt(limit));

    const keywordsArr = finalArr.map(item => item[0]);
    const arrOfEntries = finalArr.map(item => item[1]);

    // initSettingsvalues = {
    //   allTagsArr : [],
    //   priceForStdArr : [],
    //   salesForStdArr : [],
    //   avgShopsCompetingArr : [],
    //   avgProcessingMaxArr : [],
    //   avgviewsForStdArr : [],
    //   avgfavForStdArr : []
    // };

    return {keywordsArr, arrOfEntries};
}

async function makeTagpropertiesTable(keywordsArr, source) {
  let finalArr = [];
  let destructuredArr = keywordsArr;

  if (keywordsArr.constructor.name === "Object") {
    destructuredArr = keywordsArr.keywordsArr;
  } 

  if (source) {
    for (let i = 0; i < destructuredArr.length; i++) {
      let tagInfo = {}
      let currentTag = destructuredArr[i];
      await callEtsyKeywordFinderApi2(destructuredArr[i]).then( async (res) => {
        let list = res.results;
        let count = res.count;
  
        const keywordsValues = calculateKeywordsValuesSection(list, count);
  
        console.log('keywordsValues', keywordsValues);
  
        const similarKeywordsObg = calculateSimilarKeywordsAction(0, KeywordLimit, keywordsValues.allTagsArr);
  
  
        console.log('similarKeywordsObg', similarKeywordsObg);
  
        const ctr = calculateCTR(similarKeywordsObg.arrOfEntries);
  
        tagInfo = {
          tagName: currentTag,
          avgProcessing: keywordsValues.avgProcessing,
          clicks: keywordsValues.clicks,
          comp: keywordsValues.comp,
          favorites: keywordsValues.favorites,
          price: keywordsValues.price,
          sales: keywordsValues.sales,
          shopsCompeting: keywordsValues.shopsCompeting,
          ctr: ctr.ctr,
          searches: ctr.searches,
        }
  
        finalArr.push(tagInfo);     
      })
    }
  } else {
    for (let i = 0; i < destructuredArr.length; i++) {
      let tagInfo = {}
      let currentTag = destructuredArr[i];
      await callEtsyKeywordFinderApi(destructuredArr[i]).then( async (res) => {
        let list = res.results;
        let count = res.count;
  
        const keywordsValues = calculateKeywordsValuesSection(list, count);
  
        console.log('keywordsValues', keywordsValues);
  
        const similarKeywordsObg = calculateSimilarKeywordsAction(0, KeywordLimit, keywordsValues.allTagsArr);
  
  
        console.log('similarKeywordsObg', similarKeywordsObg);
  
        const ctr = calculateCTR(similarKeywordsObg.arrOfEntries);
  
        tagInfo = {
          tagName: currentTag,
          avgProcessing: keywordsValues.avgProcessing,
          clicks: keywordsValues.clicks,
          comp: keywordsValues.comp,
          favorites: keywordsValues.favorites,
          price: keywordsValues.price,
          sales: keywordsValues.sales,
          shopsCompeting: keywordsValues.shopsCompeting,
          ctr: ctr.ctr,
          searches: ctr.searches,
        }
  
        finalArr.push(tagInfo);     
      })
    }
  }

  counter = 0;

  console.log('finalArr table arr', finalArr);

  return finalArr;  
}

  

function clearKeywordTable() {
  document.getElementById('overview-keyword').innerText = '';
  // document.querySelector('.tags-rating__body').style.opacity = '0';
  document.querySelector('.tags-rating__title').innerText = '';
  // document.querySelector('.tags-rating__header-wrapper').style.display = 'none';

  // setTimeout(() => {
  //     document.querySelector('.tags-rating__body').innerHTML = '';
  // }, 1000);

  document.querySelector('.tags-rating__body').innerHTML = '';

  document.querySelector('.tags-rating').style.opacity = '1';

  document.querySelector('.table-loader').classList.add('active');
}


function getKeywordSearch(keyword) {
  document.getElementById("keywordResearch").click();
  document.getElementById("search-keyword-input").value = keyword;
  document.getElementById('search-keyword').click();
};


function showKeywordResult(listingArr) {

  return new Promise((resolve) => {
    const tagsRatingBody = document.querySelector('.tags-rating__body');
    const tagsRating = document.querySelector('.tags-rating');
  
    tagsRatingBody.innerHTML = '';
    tagsRating.style.opacity = '1';
  
    listingArr.forEach((item) => {
      const listRow = makeListRow(item);
      tagsRatingBody.insertAdjacentHTML('beforeend', listRow);
    });
    resolve();
  })
}

function makeListRow(tag) {

  if (typeof tag !== 'object') {
    return`
      <div class="tags-rating__item">
        <div class="tags-rating__name">${tag}</div>
      </div>`
  } else {
  return `
    <div class="tags-rating__item">
      <div class="tags-rating__metric">${tag.tagName}</div>
      <div class="tags-rating__metric">${tag.clicks}</div>
      <div class="tags-rating__metric">${tag.favorites}</div>
      <div class="tags-rating__metric">${tag.comp}</div>
      <div class="tags-rating__metric">${tag.sales}</div>
      <div class="tags-rating__metric">${tag.shopsCompeting}</div>
      <div class="tags-rating__metric">${tag.avgProcessing}</div>
      <div class="tags-rating__metric">${tag.searches}</div>
      <div class="tags-rating__metric">${tag.ctr +'%'}</div>
    </div>`
  }
};
