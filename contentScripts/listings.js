var id;
var sum = 0;
var avgfavsum = 0;
var averagefav;

// перенести aouth на сервер

function getProductInfoAsync(listing_id, type) {       
  return new Promise((resolve) => {(async () => {
    var listingId = listing_id
    var etsy_user_credential_obj = await getUserSetting();
    var etsy_user_credential = etsy_user_credential_obj.etsy_user_credential;
    var etsy_access_token = etsy_user_credential['etsy_access_token'];
    var etsy_access_secret =etsy_user_credential['etsy_access_secret'];

    console.log('etsy_user_credential_obj', etsy_user_credential_obj);

    const token = {
      key: etsy_access_token,
      secret: etsy_access_secret,
    }

    sendMessage({"command": "callEtsyOpenApi","data":{token:token, listing_id:listingId }},function(apiResponse) {
    if (type != undefined && type=='hover') {
      console.log('apiResponse', apiResponse);
      resolve(apiResponse);
    } else {
      console.log('apiResponse', apiResponse);
      resolve(apiResponse.result.results[0]);
    }
    })
   })();
 });
};

function pushProductDetailForExpandOption(productObjDetail) {
  return new Promise((resolve) => {
    var detailObj = {};
    var title = productObjDetail.title.length > 72 ? productObjDetail.title : productObjDetail.title;
    listingtitle = title;
    var productUrl=productObjDetail.url;
    listingtags = productObjDetail.tags.toString();
    var listingtagsArr = listingtags.split(',');
    var arrLenHalf = Math.ceil(listingtagsArr.length / 2);
    var prodImg=productObjDetail['Images'][0]['url_170x135'];
    var listing_id = productObjDetail.listing_id;
    detailObj['listingtitle']=listingtitle;
    detailObj['productUrl']=productUrl;
    detailObj['listingtags']=listingtagsArr;
    detailObj['arrLenHalf']=arrLenHalf;
    detailObj['prodImg']=prodImg;
    detailObj['listing_id']=listing_id;
    pushProductDetailForExpandOptionArr.push(detailObj);
    resolve(true);
  })
};

function postSaveEverBeeProccessLogsToFile(log_body){
  if (isLogin == true && userMeta.user_email != undefined) {
    var user_email = userMeta.user_email;
    sendMessage({"command": config.keys.postSaveEverBeeProccessLogsToFile, "log_body" : log_body, "user_email" : user_email});
  }
};


function getExpandableRow(
  productObjDetail,
  uniqeIdForRow,
  resultInFav,
  shopObjDetail,
  prodImg,
  title,
  ad,
  listingtitle,
  tags,
  currencyCode,
  uniqPrice,
  estimated_total_sales,
  estSalesNumber,
  estRevNumber
  ) {
  return `
     <div
      class="ebe-table-expandable-row"
      id="${uniqeIdForRow}"
      style="display:none"
      >
      <div
        class="ebe-table-row-parent"
        style="display: block;"
        pId="${productObjDetail.listing_id}"
        product-list-url="${productObjDetail.url}"
        shopName="${shopObjDetail['shopname']}"
        open-node="true"
        alt="Expand"
      >
        <div class="w-layout-grid ebe-table-row-parent-grid">
          <div id="w-node-167b30d98d6d-a6ca24f6" class="ebe-table-row-checkbox everBee__custom_view1">
            <div class="w-form">
              <form id="email-form" name="email-form" data-name="Email Form">
                <div class="form-item">
                  <label class="w-checkbox checkbox-wrap">
                    <div class="w-checkbox-input w-checkbox-input--inputType-custom checkbox"></div>
                    <input
                      type="checkbox"
                      id="checkbox-2"
                      name="checkbox-2"
                      class="ch-elem"
                      data-name="Checkbox 2"
                      style="opacity:0; position:absolute; z-index:-1"
                    >
                    <span class="checkbox-label-3 w-form-label">Checkbox</span>
                  </label>
                </div>
                <input
                  type="submit"
                  value="Submit"
                  data-wait="Please wait..."
                  class="submit-button-hidden w-button"
                >
              </form>
              <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
              </div>
              <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
              </div>
            </div>
          </div>
          <img
            src="${prodImg}"
            width="32"
            height="32"
            alt="listing-image"
            id="w-node-167b30d98d7c-a6ca24f6"
            class="ebe-table-row-listing-image"
          >
          <a
            id="w-node-167b30d98d7d-a6ca24f6" 
            href="${productObjDetail.url}"
            target="_blank"
            class="link-block w-inline-block"
            style="display:flex"
          >
            <div class="product-title-ad">
              <div class="ebe-table-row-text">
                ${title}
              </div>
              ${ad.innerHTML}
            </div>
            <pdt hidden>${listingtitle}</pdt>
            <span hidden>${productObjDetail.url}</span>
            <tspan hidden>${tags}</tspan>
          </a>
          <a
            id="w-node-167b30d98d80-a6ca24f6"
            href="https://www.etsy.com/shop/${shopObjDetail['shopname']}"
            target="_blank"
            class="link-block w-inline-block"
          >
            <div class=" ebe-table-row-text-center">${shopObjDetail['shopname']}</div>
          </a>
          <div id="w-node-167b30d98d85-a6ca24f6" class="ebe-table-row-number">${uniqPrice + ' ' + currencyCode}</div>
          <div id="w-node-167b30d98d87-a6ca24f6" class="ebe-table-row-number ebe-table-row-number-padding">${numberWithCommas(productObjDetail.num_favorers)}</div>
          <div id="w-node-167b30d98d89-a6ca24f6" class="ebe-table-row-number ebe-table-row-number-padding">${estimated_total_sales}</div>
          <div id="w-node-167b30d98d84-a6ca24f6" class="ebe-table-row-number ebe-table-row-number-padding">${estSalesNumber}</div>
          <div id="w-node-167b30d98d8b-a6ca24f6" class="ebe-table-row-number ebe-table-row-number-padding">${estRevNumber + ' ' + currencyCode}</div>
          <div class="${resultInFav.result.success ? 'inFavChecked' : 'inFavUnchecked'} favoritesBtn ebe-table-row-number ebe-table-row-number-padding"></div>
          <div id="w-node-arrfdsv434fds-a6ca24f6" class="ebe-table-row-number ebe-table-row-number-padding"></div>
          <div
            id="w-node-167b30d98d8d-a6ca24f6"
            id="${productObjDetail.listing_id}"
            shopName="${shopObjDetail['shopname']}"
            class="ebe-table-row-arrow-icon"
          >
            <img
              src="https://uploads-ssl.webflow.com/5ea9712755ee48bd64ca24f7/5ea9712755ee48a244ca2504_arrow-down.svg"
              width="16"
              height="8"
              alt=""
              class="ebe-table-row-arrowdown"
            >
          </div> 
        </div>
      </div>
      <div style="height: 0px; display: none;" class="ebe-table-row-child">
        <div style="display: none; opacity: 0;" class="ebe-table-row-expanded-container">
          <div style="display: none; opacity: 0;" class="ebe-table-row-expanded-loader">
            <div style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(360deg) skew(0deg, 0deg); transform-style: preserve-3d; opacity: 0;width: 35px;height: 35px;" class="loader__small">
            </div>
          </div>
          <div class="ebe-table-row-expanded-image-section">
            <img src="#" alt="listing-image-large" class="listing-image-large">
          </div>
          <div class="ebe-table-row-expanded-listing-info-section">
            <a href="#" class="ebe-table-row-expanded-listing-link w-inline-block" display="none">
              <div class="listing-title-text"></div>
            </a>
            <div class="ebe-table-row-expanded-listings-shop-info">
              <a href="#" class="shop-card w-inline-block" target="_blank">
                <div class="shop-name"></div>
                <div class="shop-title"></div>
                <div class="shop-details">
                  <img
                    src="https://uploads-ssl.webflow.com/5ea9712755ee48bd64ca24f7/5ea9712755ee484f80ca2509_location-icon.svg"
                    width="8"
                    height="12"
                    alt=""
                    class="location-icon"
                  >
                  <div class="shop-details-text">
                    <span class="ShopRegion"></span>
                    <span class="ShopCity"></span>
                  </div>
                  <div class="shop-seperator location-seperator"></div>
                  <div class="shop-details-text ShopSale"></div>
                  <div class="shop-seperator"></div>
                  <div class="shop-details-text year"></div>
                </div>
                <div class="shop-reviews">
                  <b>
                    <span class=ShopRating></span>
                  </b>
                  <div class="shop-review-count">
                    <div class="shop-review-count-text"></div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <div class="ebe-extended-row-seperator"></div>
          <div class="ebe-table-row-expanded-tags-section">
            <div class="ebe-tags-title">
              <div class="ebe-tags-title-text">Tags</div>
              <div class="tooltip-label-wrap">
                <div class="tooltip-wrap">
                  <a data-w-id="ef5a7d70-74c1-9af0-028c-167b30d98dc2" href="#" class="tooltip-trigger w-inline-block">
                    <div class="help-icon">
                      <img
                        src="https://uploads-ssl.webflow.com/5ea9712755ee48bd64ca24f7/5ea9712755ee486102ca250b_info.svg"
                        width="14"
                        height="14"
                        alt=""
                        class="image-2"
                      >
                    </div>
                  </a>
                  <div style="display:none" class="assistive-text__wrap">
                    <div class="assistive-text">Click on a tag to copy it</div>
                    <div class="assistive-text__carrot"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ebe-tags"></div>
            <div class="all-tags-copy copyallButton" style="margin-top:35px !important">
              <div data-w-id="ef5a7d70-74c1-9af0-028c-167b30d98e21" class="copy-clipboard-text">Copy all to clipboard</div>
            </div>
          </div>
        </div>
      </div>
    </div>`;
};
