class ExtensionMessage{
  constructor(context, data){
      this.context = context;
      this.data = data;
  }
};

//To Save Data From storage
function storageSet({key, value}) {
  return new Promise((resolve) => {
      chrome.storage.local.set({[key]: value}, function() {
          resolve();
        });
  });
}

//To Read Data From Storage
function storageGet(key){
  return new Promise((resolve) => {
      chrome.storage.local.get([key], function(result) {
          resolve(result[key]);
          });
  });
}

function loading(active){
  let overlay = document.getElementsByClassName('loading-overlay')[0];
  if(overlay){
      overlay.classList.toggle('is-active');
  }
}

function sendPageMessage(data, specificID = null){
  return new Promise((resolve) =>{
      chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
          if(tabs && tabs.length){
              tabID =tabs[0].id;
              chrome.tabs.sendMessage(specificID || tabID, data);
              console.log(data);
              resolve();
          }
      });
  });
}

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function forSeconds(seconds){
  return new Promise((resolve) =>{setTimeout(function(){resolve()}, seconds * 1000)});
}

function activateContextMenu(menuId, activate){
  chrome.contextMenus.update(menuId, {
      visible: activate ? true : false,
});
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function floor(value, decimals = 0){
  return Math.floor(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
}

function contentSlideInit(){
  let content = document.querySelector("#content");
  let slides = document.querySelectorAll("nav .nav-item");
  slides.forEach(function(el, index){
      el.onclick = function(){
          if(!el.classList.contains("active")){
              let active = el.closest("nav").querySelector(".active");
              active.classList.remove("active");

              active.querySelector("img").src = `./images/${active.querySelector("img").dataset["name"]}.svg`;

              el.classList.add("active");
              el.querySelector("img").src = `./images/${el.querySelector("img").dataset["name"]}-active.svg`;
              content.style.transform = `translateX(-${index * 100/ slides.length}%)`;
          }
      }
  });
}

function elementAppear(selector, cyclesCount){
  var cycles = cyclesCount || 0;
  console.log(selector);
  return new Promise((resolve) => {
      let id = setInterval(function(){
          cycles++;
          let element = document.querySelector(selector);

          if(element){
              clearInterval(id);
              resolve(element);
          }
      }, 100);
  });
}

function requestBackground(request){
  // request is {context, data}
  return new Promise((resolve) =>{
      var handler = ({context, data}) => {
        if(context == request.context){
          chrome.runtime.onMessage.removeListener(handler);
          resolve(data);
        }
      };
      chrome.runtime.onMessage.addListener(handler);
        chrome.runtime.sendMessage(request);
    });
}

function copyToClipboard(text){
  const input = document.createElement("input");
  input.style.opacity = 0;
  document.body.appendChild(input);
  input.value = text;
  input.select();
  document.execCommand("copy");
  document.body.removeChild(input);
}

function parseJwt (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
};

//drag with mouse
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.querySelector("#" + elmnt.id + " header")) {
    // if present, the header is where you move the DIV from:
    document.querySelector("#" + elmnt.id + " header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

const sleep = m => new Promise(r => setTimeout(r, m))

function parseDate(s) {
  var months = {jan:0,feb:1,mar:2,apr:3,may:4,jun:5,
                jul:6,aug:7,sep:8,oct:9,nov:10,dec:11};
  var p = s.split('-');
  return new Date(p[2], months[p[1].toLowerCase()], p[0]);
}

async function sendMessageCustom(key, value, type, c) { // need to fix
  var evt = document.createEvent("CustomEvent");
  evt.initCustomEvent("sendMessageCustom", true, true, [key, value, type, c]);
  document.dispatchEvent(evt);
}
