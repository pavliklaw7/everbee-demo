var callback=[];
var sub;

function closeFrame() {
  console.log('closeFrame');
  const wrapper = document.getElementById('iframe-wrapper');
  const frame = document.getElementById('lightboxFrame');
  frame.parentNode.removeChild(frame);
  wrapper.parentNode.removeChild(wrapper);
};


function closeFrame2() {
  console.log('closeFrame2');
  const wrapper2 = document.getElementById('iframe-wrapper2');
  const frame2 = document.getElementById('lightboxFrame2');
  frame2.parentNode.removeChild(frame2);
  wrapper2.parentNode.removeChild(wrapper2);
}




const everBeeHover = {
  uiSettings:{},
  current_elm:'',
  isDetailsPage:false,
  current_productId:'',

  onExtMessage: function(message){
    everBeeHover.message = message;
    switch (message.command) {
      case "reload":
      break;
    }
    return true;
  },

  load: function() {
    var fullUrl = window.location.href;
    if (fullUrl.indexOf('/listing/') !== -1) {
      everBeeHover.makeHoverEverBeebtnsDetailPage();
    } else {
      everBeeHover.makeHoverEverBeebtnsListings();
    }

    everBeeHover.addEvents();
  },
  
  addEvents:function(){

  $(".v2-listing-card__img").mouseenter(function () {

    var status= $(this).find('.everBee-hover-img').attr('status');
    if (status ==undefined || status=='undefined') {
      // $(this).find('.inactive-everBee').removeClass('display-none').addClass('display-block');
    }
  }).mouseleave(function () {
    var status= $(this).find('.everBee-hover-img').attr('status');
    if (status!=undefined && status=='isActive') {
     $(this).find('.active-everBee').removeClass('display-none').addClass('display-block');
     // $(this).find('.inactive-everBee').removeClass('display-block').addClass('display-none');
   }else{
    // $(this).find('.inactive-everBee').removeClass('display-block').addClass('display-none');
    // $(this).find('.active-everBee').removeClass('display-block').addClass('display-none');
  }
});
  $('.everBee-hover-img')
  .mouseenter(function () {
    $(this).find('.active-everBee').removeClass('display-none').addClass('display-block');
    // $(this).find('.inactive-everBee').removeClass('display-block').addClass('display-none');
  })
  .mouseleave(function () {
    var status = $(this).attr('status');
    if (status != undefined && status == 'isActive') {
     $(this).find('.active-everBee').removeClass('display-none').addClass('display-block');
     // $(this).find('.inactive-everBee').removeClass('display-block').addClass('display-none');
   } else {
    $(this).find('.active-everBee').removeClass('display-block').addClass('display-none');
    $(this).find('.inactive-everBee').removeClass('display-none').addClass('display-block');
  }
});

  $(document).off('click','.everBee-hover-img').on('click', '.everBee-hover-img', function (e) {
    e.preventDefault();

    // document.querySelector('.main-menu .active').classList.remove('active');
    // document.querySelector(".main-window.active").classList.add('d-none');
    // document.querySelector(".main-window.active").classList.remove('active');

    document.querySelector('#main-everBee-ext').setAttribute('data-show', 'false');
    if ($(this).attr('isDetailsPage')!=undefined && $(this).attr('isDetailsPage')!=true) {
      everBeeHover.isDetailsPage = true;
    }
    $('.everBee-hover-img').removeAttr('status');
    $('.everBee-hover-img:not([status])').find('.active-everBee').removeClass('display-block').addClass('display-none');
    $('#hidden-content-hover').remove();
    everBeeHover.current_elm=this;
    $(everBeeHover.current_elm).attr("status", 'isActive');
    sendMessageCustom('getMemberPop',1);

  });

  $(document).on('click', '.box-close-icon', function (e) {
    $('.everBee-hover-img').removeAttr('status');
    $('#hidden-content-hover').remove();
  });

},
checkMSForDetailPage:function(current_productId){
  everBeeHover.current_productId = current_productId;
  sendMessageCustom('getMemberPop',1);
},
getPriceValFromProduct:function(str){
  var regex = /[+-]?\d+(\.\d+)?/g;
  var floats = str.match(regex).map(function(v) { return parseFloat(v); });
  return floats[0];

},
openEverBeePopOverDetailPageAction:function(){
  console.log('openEverBeePopOverDetailPageAction');
  $("#lightboxFrame").remove();
  var productPriceStr=$('[data-buy-box-region="price"]').find('p.wt-text-title-03').text().trim();
  var productPrice = everBeeHover.getPriceValFromProduct(productPriceStr);
  var lightBoxIframe = chrome.runtime.getURL('popover_iframe/index.html?iframe=true&productPrice='+productPrice);
  if ($("#lightboxFrame").length == 0) {
    $("#iframe-wrapper").remove();
    $(top.document.body).first().append(`<div id="iframe-wrapper" style='position:fixed; z-index:100; bottom:0; left:0; right:0; width:100%;'><button id="closeiframeBtn" class="everbee-extension__close-btn" style="position:absolute; top:0; right:0; width:60px; height:60px; outline:none; z-index:2000; background-color:transparent;"></button><iframe src="${lightBoxIframe}"  id="lightboxFrame"></iframe></div>`);

    document.getElementById('closeiframeBtn').addEventListener('click', () => closeFrame());
  }
  everBeeHover.addEvents();
},

openEverBeePopOverAction:function(){
  $("#lightboxFrame2").remove();

  var productId = "";
  
  if ($(everBeeHover.current_elm).closest('li').find('a').attr('data-listing-id') != undefined) {
    productId = $(everBeeHover.current_elm).closest('li').find('a').attr('data-listing-id');
  } else if ($(everBeeHover.current_elm).closest('.v2-listing-card').find('a').attr('data-listing-id')!= undefined) {
    productId = $(everBeeHover.current_elm).closest('.v2-listing-card').find('a').attr('data-listing-id');
  }
  var productPriceStr = "";

  if ($('[data-listing-id="'+productId+'"]').find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim() != "") {
    productPriceStr = $('[data-listing-id="'+productId+'"]').find('span.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
  } else if ( $('[data-listing-id="'+productId+'"]').find('div.n-listing-card__price').find('.currency-value:eq(0)').text().trim() != "") {
    productPriceStr = $('[data-listing-id="'+productId+'"]').find('div.n-listing-card__price').find('.currency-value:eq(0)').text().trim();
  }
  var productPrice = everBeeHover.getPriceValFromProduct(productPriceStr);
  var productUrl = $('a[data-listing-id="'+productId+'"]').attr('href');
  var lightBoxIframe = chrome.runtime.getURL('popover_iframe/index.html?iframe=true&productId='+productId+'&productUrl='+productUrl+'&productPrice='+productPrice);
    
  if ($("#lightboxFrame").length == 0) {
    $("#iframe-wrapper2").remove();
    $(top.document.body).first().append(`<div id="iframe-wrapper2" style='position:fixed; z-index:100; bottom:0; left:0; right:0; width:100%;'><button id="closeiframeBtn2" class="everbee-extension__close-btn" style="position:absolute; top:0; right:0; width:60px; height:60px; outline:none; z-index:2000; background-color:transparent;"></button><iframe src="${lightBoxIframe}"  id="lightboxFrame2"></iframe></div>`);

    document.getElementById('closeiframeBtn2').addEventListener('click', () => closeFrame2());

  }
},
makeHoverEverBeebtnsDetailPage:async function(){
  let em = await storageGet("userEmail")
  if(em){
    let s = await isSubscriptionActiveHv()
    if(s){
      var imgURL = chrome.runtime.getURL("images/listing-icon.svg");
      $('.image-carousel-container').append(`<button  class="custom-everBee-icon-hover1"
       data-ui="favorite-listing-button" ><div isDetailsPage="true" class="everBee-hover-img"><span><span class="display-block inactive-everBee"><svg viewBox="0 0 92 91" fill="none"
        xmlns="http://www.w3.org/2000/svg" style="width: 30px;height: 30px; z-index: 10 !important">
        
        <circle cx="46" cy="45.5" r="45.5" fill="transparent"></circle>
        </svg>
        <img src=${imgURL} style="position: absolute; width: 40px; height: 40px; margin-top: -35px; margin-left: -11px; opacity: 0.6 transition: opacity 0.3s easy"/>
        </span></span>
        </button>`)
    }
  }
},

makeHoverEverBeebtnsListings:async function(){
  let em = await storageGet("userEmail")
  if(em){
    let s = await isSubscriptionActiveHv()
    console.log("sub", s)
    if(s){
      $('.v2-listing-card__img').each(function () {
        var img_tag = $(this).find("img").parent();
        var imgURL = chrome.runtime.getURL("images/listing-icon.svg");
    
        var newHover = `<button  class="position-absolute has-hover-state z-index-1 btn-transparent custom-everBee-icon-hover" data-ui="favorite-listing-button">
        
        <div class="everBee-hover-img"><span><span class=inactive-everBee"><svg viewBox="0 0 92 91" fill="none" xmlns="http://www.w3.org/2000/svg" 
        style=" width: 30px;height: 30px; z-index: 10 !important""><circle cx="46" cy="45.5" r="45.5" fill="transparent"></circle>
        </svg>
        <img src=${imgURL} style="position: absolute; z-index: -1; width: 30px; height: 30px; margin-top: -35px; margin-left: -11px;"/>
        </span></span></div></button>`;
    
        img_tag.prepend(newHover);
      });
    
      everBeeHover.addEvents();
    }
  }
}

};

chrome.runtime.onMessage.addListener(everBeeHover.onExtMessage);
$(document).ready(function () {
  everBeeHover.load();

});

function sendMessage(msg, callbackfn) {
  if(callbackfn!=null) {
    callback.push(callbackfn);
    msg.callback = "yes";
  }
  chrome.runtime.sendMessage(msg,callbackfn);
}
document.addEventListener('checkMemberstackMemberPopover', async function (e){
   // isLogin=localStorage.getItem(config.keys.isLoginUser);
   isLogin = await storageGet('isLoginUser')

   firstName=e.detail[0]['firstName'];
   if (e.detail[0]['membership']!=undefined) {
    membership=e.detail[0]['membership'];
  }

  console.log(isLogin);
  
  if (isLogin == 'true' ||  isLogin == true) {
   if ((membership != undefined && membership.status == 'active' && membership.id == '5e4fd9afac49de0017548365') || (membership != undefined && membership.status=='trialing' && membership.id == '5ed3af7c9518650004bf51a8')) {
  } 

  if (everBeeHover.current_productId != undefined && everBeeHover.current_productId != '' || everBeeHover.isDetailsPage == true) {
    everBeeHover.openEverBeePopOverDetailPageAction(everBeeHover.current_productId);
    console.log('openEverBeePopOverDetailPageAction');
  } else {
    everBeeHover.openEverBeePopOverAction();
    console.log('openEverBeePopOverAction');
  }
} else {
  addIframeToContent('loginPop');
}

});


async function isSubscriptionActiveHv() {
  return new Promise(async (resolve) => {
    const userEmail = await storageGet('userEmail');

    if (userEmail) {
      sendMessage({ "command": "checkSubscription", "data": { userEmail: userEmail } }, (response) => {
        resolve(response.result.active)
      });
    } 
  })
};