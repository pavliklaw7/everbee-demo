function DB(table_name) {
    db = new Dexie("UsersLogins");
    db.version(1).stores({
       MsUsers: '++uid,email,etsy_access_token,etsy_access_secret,searchOverAction',
       general:'loginId,email'
   });
    this.db = db;
}
DB.prototype.get_data_by_key_value_pair = function(key, value) {
  return new Promise((resolve, reject) => {
    (async () => {
      db=this.db;  
      const res = await db.MsUsers.where(key).equals(value).toArray();
      if (res.length!=0) {
        resolve(res[0]);
    } else {
        resolve(false);
    }      
})(); 
});
}

DB.prototype.insert_update_operation = function (updated_object_value) {
  return new Promise((resolve) => {
   (async () => {
    let _this=this;
    db=this.db;
    const res = await db.MsUsers.put(updated_object_value);
    resolve(true);
})(); 
});
}

DB.prototype.insertCurrentLoginId = function (loginObj) {
  return new Promise((resolve) => {
   (async () => {
    let _this=this;
    db=this.db;
    const res = await db.general.put(loginObj);
    resolve(true);
})(); 

});

}

DB.prototype.get_data_by_key_value_pair_general = function(key, value) {
  return new Promise((resolve, reject) => {
    (async () => {
      db=this.db;  
      const res = await db.general.where(key).equals(value).toArray();
      if (res.length!=0) {
        resolve(res[0]);
    } else {
        resolve(false);
    }      
})(); 

});

}





