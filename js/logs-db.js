/*
/ Storage Functions
*/

var DbName = 'EverBeeLogsDB';

var users_log = {
	Name: "users_log",
	Columns: [{
		Name: 'user_log_id',
		PrimaryKey: true,
		AutoIncrement: true
	}, {
		Name: "log_date",
		NotNull: true,
		DataType: JsStore.Data_Type.String
	},{
		Name: "created_at",
		NotNull: true,
		DataType: JsStore.Data_Type.String
	}
	]
}

var logs_list = {
	Name: "logs_list",
	Columns: [{
		Name: "log_id",
		PrimaryKey: true,
		AutoIncrement: true
	}, {
		Name: "user_log_id",
		NotNull: true,
		DataType: JsStore.Data_Type.Number
	}, {
		Name: "created_at",
		NotNull: true,
		DataType: JsStore.Data_Type.String
	}, {
		Name: "log_body",
		NotNull: true,
		DataType: JsStore.Data_Type.String
	},
	]
}

var Db = {
	Name: DbName,
	Tables: [users_log, logs_list]
}

var Connection = new JsStore.Instance();
JsStore.isDbExist(DbName, function (isExist) {
	if (isExist) {
		Connection.openDb(DbName);
	} else {
		Connection.createDb(Db);
	}
}, function (err) {
	console.log(err);
})

function addLogInDB(log_body){
	var log_date = getFormattedDate();
	insertUserLogInDB(log_date).then((insertedResults) => {
		console.log('insertedResults >> ', insertedResults);
		if (insertedResults && insertedResults.status == "200") {
			var result = insertedResults.result;
			if (result && result.length > 0) {
				var user_log_id = result[0].user_log_id;
				insertLogsInDB(user_log_id,log_body).then((insertedlogsResult) => {
					console.log('insertedlogsResult >> ', insertedlogsResult);
				});
			}
		}
	});
}

function insertUserLogInDB(log_date){
	return new Promise((resolve, reject) => {
		Connection.select({
			From: 'users_log',
			Where: {
				log_date : log_date
			},
			OnSuccess: function (logResults) {
				if (logResults.length > 0) {
					resolve({ status: "200", result: logResults });
				} else {
					var created_at = getDateTimeFormatted();
					var Value = {
						log_date : log_date,
						created_at : created_at
					}
					Connection.insert({
						Into: 'users_log',
						Values: [Value],
						OnSuccess: function (insertlog) {
							if (insertlog > 0) {
								Connection.select({
									From: 'users_log',
									Where: {
										log_date : log_date
									},
									OnSuccess: function (logResults) {
										if (logResults.length > 0) {
											resolve({ status: "200", result: logResults });
										}
									},
									OnError: function (error) {
										console.log(error);
									}
								});
							}
						},
						OnError: function (error) {
							console.log(error);
						}
					});
				}
			},
			OnError: function (error) {
				console.log(error);
			}
		});
	});
}

function insertLogsInDB(user_log_id,log_body){
	return new Promise((resolve, reject) => {
		var created_at = getDateTimeFormatted();
		var Value = {
			user_log_id : user_log_id,
			log_body : log_body,
			created_at : created_at
		}
		Connection.insert({
			Into: 'logs_list',
			Values: [Value],
			OnSuccess: function (insertlog) {
				if (insertlog > 0) {
					resolve({ status: "200", result: insertlog });
				}
			},
			OnError: function (error) {
				console.log(error);
			}
		});
	});
}

function getUserLogByDateFromDB(log_date){
	return new Promise((resolve, reject) => {
		Connection.select({
			From: 'users_log',
			Where: {
				log_date : log_date
			},
			OnSuccess: function (logResults) {
				if (logResults.length > 0) {
					resolve({ status: "200", result: logResults });
				} else {
					resolve({ status: "400", result: [] });
				}
			},
			OnError: function (error) {
				console.log(error);
			}
		});
	});
}

function getLogsByIdFromDB(user_log_id){
	return new Promise((resolve, reject) => {
		Connection.select({
			From: 'logs_list',
			Where: {
				user_log_id : user_log_id
			},
			OnSuccess: function (logResults) {
				if (logResults.length > 0) {
					resolve({ status: "200", result: logResults });
				} else {
					resolve({ status: "400", result: [] });
				}
			},
			OnError: function (error) {
				console.log(error);
			}
		});
	});
}

/* get current Formatted Date */
function getFormattedDate() {
	var today = new Date();
	var y = today.getFullYear();
	var m = pad(today.getMonth() + 1);
	var d = pad(today.getDate());
	return d + "-" + m + "-" + y;
}

/* get current Formatted Time */
function getDateTimeFormatted() {
	var today = new Date();
	var y = today.getFullYear();
	var m = pad(today.getMonth() + 1);
	var d = pad(today.getDate());
	var h = pad(today.getHours());
	var mi = pad(today.getMinutes());
	var s = pad(today.getSeconds());
	return d + "-" + m + "-" + y + " " + h + ":" + mi + ":" + s;
}
function pad(number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
}
