var MemberStack;
var userMetaDataObj = {};


function refresh(f) {
  if( (/in/.test(document.readyState)) || (typeof Member === undefined) ) {
    setTimeout('refresh(' + f + ')', 10);
  } else {
    f();
  }
}

var main = function(){

  sessionStorage.setItem('redirect',window.location.href);
}

var checkMemberstackMember = function (key, value,type,searchOver) {
  try{
     (async()=>{

     var evt = document.createEvent("CustomEvent");
     var obj={};
     var key = {
       'loggedIn': false,
       'first-name': 'unknown',
       'membership': 'unknonwn'
     }
 
     var metadata = {
      etsy_access_token: "",
      etsy_access_secret: ""
     }

     obj['loggedIn']=key['loggedIn'];
     obj['firstName']=key['first-name'];
     obj['membership']=key['membership'];
     obj['userMeta']=metadata;
     evt.initCustomEvent("checkMemberstackMember", true, true, [obj, value,type]);
     document.dispatchEvent(evt);
   })();

}catch(e){
  console.log(e)
}

}

var checkMemberstackMemberPopover = async function (key, value) {
  try{
    var evt = document.createEvent("CustomEvent");
    var obj={};

    var key = {
      // 'loggedIn': localStorage.getItem(config.keys.isLoginUser),
      'loggedIn':await storageGet(config.keys.isLoginUser),
      'first-name': 'unknown',
      'membership': 'unknonwn'
    }

    console.log(key);

    var metadata = {
      etsy_access_token: "",
      etsy_access_secret: ""
    }

    obj['loggedIn']=key['loggedIn'];
    obj['firstName']=key['first-name'];
    obj['membership']=key['membership']
    evt.initCustomEvent("checkMemberstackMemberPopover", true, true, [obj, value]);
    document.dispatchEvent(evt);
  } catch(e) {
    console.log(e)
  }

}

var updateEtsyCredentialToMemberStack = function (key,etsy_user_credential) {
  try{
    if (!isPromise(key.onReady)) {
      var etsy_access_token = etsy_user_credential.etsy_access_token;
      var etsy_access_secret = etsy_user_credential.etsy_access_secret;
      if (etsy_access_token && etsy_access_secret) {
        saveEtsyUserCredentialToMemberStack(etsy_access_token,etsy_access_secret);
      }
    }
  }catch(e){
    console.log(e)
  }
}

function saveEtsyUserCredentialToMemberStack(etsy_access_token,etsy_access_secret){
  (async()=>{
    var metadataResObj = {};
    metadataResObj = metadataResObj || {};
    console.log("metadataResObj >> ", metadataResObj);
    metadataResObj['etsy_access_token'] = etsy_access_token;
    metadataResObj['etsy_access_secret'] = etsy_access_secret;
    var updateMetaDataResObj = await MemberStack.updateMetaData(metadataResObj);
    console.log("updateMetaDataResObj >> ", updateMetaDataResObj);
  })()
}

setTimeout(function(){
  refresh(main);
},500)

document.addEventListener('sendMessageCustom', async (e) => {
  const isLogged = await storageGet('userEmail');

  if (e.detail[0] == 'getMember') {
    checkMemberstackMember(MemberStack, 1, e.detail[2], e.detail[3]);
  }
  if (e.detail[0] == 'noSearch' && !!isLogged) {
    simpleNotify.notify('Please use keyword search', 'good', 3);
  }
  if (e.detail[0]=='getMemberPop') {
    console.log(MemberStack);
    checkMemberstackMemberPopover(MemberStack,1)
  }
  if (e.detail[0]=='updateEtsyCredentialToMemberStack') {

    updateEtsyCredentialToMemberStack(MemberStack,e.detail[1]);
  }
  if (e.detail[0]=='setSessionValue' && e.detail[1]=='trending_now') {
    sessionStorage.setItem('redirect','https://www.etsy.com/market/trending_now?action=login');
    window.location.replace('https://www.etsy.com/market/trending_now');
  }
  // if (e.detail[0]=='setHiddenInput' && e.detail[1]=='rewardFul') {
  //   /*const signupForm = document.getElementById('sign123');
  //   Rewardful.Forms.add(signupForm);*/
  // }
  if (e.detail[0]=='updateMetaData') {
    (async()=>{
     var isOuathPage = await isLocationOuthScreen();
     if (!isOuathPage) {
       var metadata = await getmemberMetaData('post');
       userMetaDataObj = metadata;
     }
   })()
 }
});


function isPromise(object){
  if(Promise && Promise.resolve){
    return Promise.resolve(object) == object;
  }else{
    throw "Promise not supported in your environment"
  }
}

function isLocationOuthScreen(){
  return new Promise((resolve, reject) => {
    var currLocation = window.location.href;
    if (currLocation.indexOf('signin?from_page=')!==-1 || currLocation.indexOf('oauth/signin?')!==-1 || currLocation.indexOf('?from_page=')!==-1) {
      resolve(true);
    }else{
      resolve(false);
    }
  })
}

function saveLogin(loginEmail){
  return new Promise((resolve, reject) => {
    (async()=>{
      var general = new DB("general");
      var currentLogin={'loginId':1,'email':loginEmail};
      await general.insertCurrentLoginId(currentLogin);
      var isSaved= await saveLoggedInUserDetailsMS(loginEmail);
      resolve(true);
    })()
  })
}

function saveLoggedInUserDetailsMS(loginEmail){
  return new Promise((resolve) => {
    (async()=>{
      var idb = new DB("MsUsers");
      let isUserExist = await idb.get_data_by_key_value_pair('email', loginEmail);
      if (isUserExist) {
        var uid= isUserExist.uid;
        var loginUserObj={"uid":uid,"email":loginEmail,"etsy_access_token":'',"etsy_access_secret":'',"searchOverAction":false,"estyDefaultCount":10};
        await idb.insert_update_operation(loginUserObj);
      }else{
        var loginUserObj={"email":loginEmail,"etsy_access_token":'',"etsy_access_secret":'',"searchOverAction":false,"estyDefaultCount":10};
        await idb.insert_update_operation(loginUserObj);
      }
      resolve(true);

    })()
  })
}

function getmemberMetaData() {
  return new Promise((resolve) => {
    (async()=>{
      var resolveobj={};
      resolveobj['user_email'] = localStorage.getItem(config.keys.userEmail);
      resolveobj['estySearchCount']= 100;
      resolveobj['etsy_access_token'] = localStorage.getItem(config.keys.etsy_access_token) != undefined ? localStorage.getItem(config.keys.etsy_access_token) : await storageGet(config.keys.etsy_access_token);
      resolveobj['etsy_access_secret'] = localStorage.getItem(config.keys.etsy_access_secret) != undefined ? localStorage.getItem(config.keys.etsy_access_secret) : await storageGet(config.keys.etsy_access_secret);
      resolve(resolveobj);
    })();
  });
}
