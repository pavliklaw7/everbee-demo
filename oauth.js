// Initialize
const oauth = OAuth({
	consumer: {
		key: config.keys.app_key,
		secret: config.keys.app_secret,
	},
	signature_method: 'HMAC-SHA1',
	hash_function(base_string, key) {
		return CryptoJS.HmacSHA1(base_string, key).toString(CryptoJS.enc.Base64)
	},
})

const request_data = {
	url: 'https://openapi.etsy.com/v2/oauth/request_token?scope=email_r',
	method: 'GET',
	data: { oauth_callback:chrome.runtime.getURL('callback.html')},
}

$.ajax({
	url: request_data.url,
	type: request_data.method,
	data: oauth.authorize(request_data),	
}).done(function(apiResponse) {
	apiResponse = unescape(apiResponse);
	console.log('apiResponse from oauth >> ', apiResponse);

	var auth_login_url = "";
	var request_token_secret = "";	
	if (apiResponse && apiResponse.indexOf("login_url=") != -1) {
		auth_login_url = apiResponse.split("login_url=")[1];
		request_token_secret = getParameterByName('oauth_token_secret', apiResponse);		
	}

	if (auth_login_url) {
		localStorage.setItem("auth_login_url", auth_login_url);
		localStorage.setItem("request_token_secret", request_token_secret);		
		location.href = auth_login_url;
	}
});

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, '\\$&');
	var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}