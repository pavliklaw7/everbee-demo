var callback=[];

const imgURLWindowBg = chrome.runtime.getURL("./images/keywords_bg.png");


const everBeeHoverFrame = {
  uiSettings:{},
  current_elm:'',
  current_productId:'',
  productPrice: '',
  onExtMessage: function(message, sender, sendResponse){
    everBeeHoverFrame.message = message;
    switch (message.command) {
      case "reload":
      break;
    }
    return true;
    },
    load: function(){
      everBeeHoverFrame.addEvents();
      everBeeHoverFrame.checkMainPageAction();
    },
    checkMainPageAction:function(){
      sendMessage({"command": "checkMainPageAction"},function(response){
        if (response!=undefined) {
        var location=window.location.href;
        var estyTabId=response['estyTabId'];
        var estyTabUrl=response['estyTabUrl'];
        if (estyTabUrl.indexOf('/listing/')!==-1) {
          var current_productId=estyTabUrl.split('/listing/')[1].split('/')[0];
          everBeeHoverFrame.openEverBeePopOverDetailPage(current_productId,estyTabUrl);
        }else {
        var location=window.location.href;
        var current_productId=location.split('&productId=')[1].split('&productUrl=')[0];
        var estyTabUrl=location.split('&productUrl=')[1].split('&productPrice=')[0];
        everBeeHoverFrame.productPrice = location.split('&productUrl=')[1].split('&productPrice=')[1];
        everBeeHoverFrame.openEverBeePopOverDetailPage(current_productId,estyTabUrl);
      }
    }
  });
    },
    addEvents:function(){
      $(document).on('click', '#main-logo-everBee', function (e) {
        e.preventDefault();
        chrome.tabs.create({ url: '#' });

      });

      $(document).on('click', '.shop-name,.shop-title,.shop-details', function (e) {
        e.preventDefault();
        var newUrl=$('.shop-name').attr('shopurl');
        if (newUrl!=undefined) {
          chrome.tabs.create({ url: newUrl });
        }

      });

      $(document).on('click', '.shop-reviews', function (e) {
        e.preventDefault();
        var newUrl=$(this).attr('shopurl');
        if (newUrl!=undefined) {
          chrome.tabs.create({ url: newUrl });
        }
      });

      $(document).on('click', '.listing-title', function (e) {
        e.preventDefault();
        var newUrl=$(this).attr('producturl');
        if (newUrl!=undefined) {
          chrome.tabs.create({ url: newUrl.replace('everBeechromeextension', 'everbee') });
        }
      });
      $(document).on('click', '.everBee-hover-img', function (e) {
        e.preventDefault();
        $('.everBee-hover-img').removeAttr('status');
        $('#hidden-content-hover').remove();
        everBeeHoverFrame.current_elm=this;
        sendMessageCustom('getMemberPop',1);

      });

      $(document).on('click', '.box-close-icon', function (e) {
        $('.everBee-hover-img').removeAttr('status');
        $('#hidden-content-hover').remove();
      });
    },
    openEverBeePopOverDetailPage:function(current_productId,estyTabUrl){
      var productId=current_productId;
      var productUrl=estyTabUrl;
      var all_push_data_arr = [];
      var promise_all_urls_arr = [];
      for (var i = 0; i < 2; i++) {
      if (i==0) {
        promise_all_urls_arr.push(
          getProductInfoAsync(productId,'hover').then((apiResponse) => {
            if (apiResponse && apiResponse.apiResponse && apiResponse.apiResponse != undefined) {
              all_push_data_arr.push(apiResponse);
            }
          })
          .catch(error => {
            console.error("error", error);
          })
          );
      }
      if (i==1) {
        promise_all_urls_arr.push(
          getProductReviewAsync(productUrl,'hover').then((apiResponse) => {
            all_push_data_arr.push(apiResponse);
          })
          .catch(error => {
            console.error("error", error);
          })
          );
      }
    }
    Promise
    .all(promise_all_urls_arr)
    .then(() => {
      apiResponseData = all_push_data_arr.reduce(function (r, a) {
        return Object.assign(r, a);
      }, {});
      (async () => {
        var itemReview=apiResponseData['itemReview'];
        var shopReviewCount=apiResponseData['shopReviewCount'];
        var productObjDetail=apiResponseData['apiResponse'];
        var productPrice = productObjDetail.price;

        if (productObjDetail && productObjDetail['Shop'] != undefined) {
          var shopNmae = productObjDetail['Shop']['shop_name'];
          await GetShopInfo(productId,shopNmae,'#hidden-content-hover',productObjDetail,shopReviewCount);
          everBeeHoverFrame.setMatrixDetails(productObjDetail, productPrice, itemReview);
          everBeeHoverFrame.setDetailsTohoverDiv(productObjDetail);
          $('.loading-section').hide();
        }
    })();
  });

  },
  openEverBeePopOver:function(){
    var lightBoxIframe = chrome.runtime.getURL('popover_iframe/index.html?iframe=true');
    if ($("#lightboxFrame").length == 0) {
      // $("#lightboxFrame").remove();
      // $(top.document.body).first().append('</button><iframe src="'+lightBoxIframe+'"  id="lightboxFrame"></iframe>');
    }
  },
  setMatrixDetails:function(productObjDetail,productPrice,itemReview){
    return new Promise((resolve, reject) => {
      var original_creation_tsz=productObjDetail.original_creation_tsz;
      var d = new Date();
      var secondsEpoch = d.getTime() / 1000;
      var differenceInSeconds=parseInt(secondsEpoch-original_creation_tsz);
      var viewsrange=(parseFloat(productObjDetail.views)/100);
      document.getElementById('review-h-box').innerHTML = itemReview;

      var avgsalesDivi = (parseFloat(viewsrange)/ differenceInSeconds)|| 0;

      var lsViews=parseFloat(productObjDetail.views) || 0 ;
      var lsFavorers=parseFloat(productObjDetail.num_favorers) || 0;

      var newFormula=parseFloat(((-0.1152)+lsViews)*(0.031)) + parseFloat(lsFavorers * 0.047);
      var avgsalesDivi = (parseFloat(newFormula)/ differenceInSeconds)|| 0;
      var avgsales=parseFloat(avgsalesDivi*86400*30).toFixed(2) || 0;
      if(avgsales >= 59) {
        avgsales = Math.log2(avgsales)*10;
      }


      var avgfav = parseFloat(productObjDetail.num_favorers) || 0;

      document.getElementById('favcount-h-box').innerHTML = numberWithCommas(parseInt(avgfav));
      estrevenues = (parseFloat(productPrice)*Math.round(avgsales)).toFixed(2);

      var estSalesNumber=Math.round(avgsales) > 0 ? numberWithCommas(Math.round(avgsales)) : 0;
      var estRevNumber=Math.round(estrevenues) > 0 ? numberWithCommas(Math.round(estrevenues)) : 0;

      document.getElementById('revenuecount-h-box').innerHTML = estRevNumber + ' ' + productObjDetail.currency_code;
      document.getElementById('salescount-h-box').innerHTML = estSalesNumber;

      $('.copyallButtonH').on('click', function () {
        copyToClipboardComaSeparted()
        var from = $(this);
        var ff = $(this).html();
        $(this).html('<div style="display:flex;opacity:1" class="copied-clipboard-text">Copied to clipboard!</div>');
        var _this=$(this);
        setTimeout(function () {
          $(_this).html( ff);
        }, 1500);
      })

      $('.copyallButtonTitle').on('click', function () {
        copyToClipboardTitle()
        var from = $(this);
        var ff = $(this).html();
        $(this).html('<div style="display:flex;opacity:1; margin-top: -10px !important;" class="copied-clipboard-text">Copied to clipboard!</div>');
        var _this=$(this);
        setTimeout(function () {
          $(_this).html( ff);
        }, 1500);
      })
      resolve(true);
    })
  },

  setDetailsTohoverDiv:function(productObjDetail){
    if (productObjDetail.title!=undefined && productObjDetail.title!='') {
      $('#hidden-content-hover').find('.listing-title').attr('productUrl',productObjDetail.url.replace('everBeechromeextension', 'everbee'));
      $('#hidden-content-hover').find('.listing-title').text(productObjDetail.title);
    } else {
      $('#hidden-content-hover').find('.listing-title').text('');
    }
    if(productObjDetail.Images.length > 0){
      $('#hidden-content-hover').find('.listing-img').attr("src", productObjDetail.Images[0][Object.keys(productObjDetail.Images[0])[0]])
    }
    else{
      $('#hidden-content-hover').find('.listing-img').remove()
    }
  }
};

// document.querySelector('#everbee-extension__bg').src = imgURLWindowBg;
document.getElementById("closeIndividial").src = chrome.runtime.getURL("/images/close-colored-new.png")

// document.getElementById("closeIndividial").onclick = closeEverBee;

function closeEverBee() {
  const frame = document.getElementById("lightboxFrame");
  // frame.parentNode.removeChild(frame);
}



chrome.runtime.onMessage.addListener(everBeeHoverFrame.onExtMessage);
$(document).ready(function () {
  everBeeHoverFrame.load();

});
function copyToClipboardComaSeparted() {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val(allCopidTagsGlobalArr.join(',')).select();
  document.execCommand("copy");
  $temp.remove();
}

function copyToClipboardTitle() {
  var $temp = $("<input>");
  $("body").append($temp);
  console.log(document.getElementById("lil-popup-product-title").innerText);
  console.log(document.getElementById("lil-popup-product-title").textContent);
  console.log(document.getElementById("lil-popup-product-title").value);
  $temp.val(document.getElementById("lil-popup-product-title").innerText).select();
  document.execCommand("copy");
  $temp.remove();
}
// This is a function to get detail using product listing ID
function GetProductInfoDetail(eleParent, productObjDetail) {
  var arr =productObjDetail.tags.slice(0,13);
  allCopidTagsGlobalArr=[];

  console.log(arr);

  arr.forEach(tag => {
    document.querySelector('.tags-results').insertAdjacentHTML('beforeend', `<div class="listing-tag-text">${tag}</div>`);
  });

  var tagClass = document.querySelectorAll(".listing-tag-text");
  tagClass.forEach(function (element) {
    element.addEventListener("click", function () {
      sendMessage({"command": "openKeywordResearch", "data": this.textContent});
      copySingleTag(element);
      });
  });
}

function GetShopInfo(id,shop_name,eleParent,productObjDetail,shopReviewCount) {
  return new Promise((resolve, reject) => {
    var listingId = id;
    var shopName = shop_name;
    $.ajax({
      url: "https://www.etsy.com/shop/"+shopName,
      success: function( data ) {
        $('.shop-stars').remove();
        var headline=$(data).find('.trust-signals').find('[data-key="headline"]').get(0).innerText.trim();
        var location=$(data).find('.trust-signals').find('[data-key ="user_location"]').text().trim();
        var totalSales = $(data).find('.trust-signals').find('a[rel ="nofollow"]').text().trim();
        var estySince = $(data).find('.trust-signals').find('.etsy-since').text().trim();
        var totalReview =$(data).find('.trust-signals').find('.reviews-wrapper .total-rating-count').text().trim();
        var ratingval=$(data).find('.trust-signals').find('input[name="initial-rating"]').val();
        var shopImage=$(data).find('.trust-signals').find('img:eq(0)').attr('src');
        shopname = shopName;
        shopheadline = headline;
        rating = ratingval;
        ratingcount = totalReview || shopReviewCount;
        if (location.indexOf(',')!==-1) {
          shopregion = location.split(',')[0];
          shopcity = location.split(',')[1];
        }else{
          shopcity = location;
          shopregion = '';
        }

        totalSales = totalSales // ? totalSales:$(data).find('.trust-signals span:contains( Sales)').get(0).textContent.trim();
        shopsale = totalSales!=undefined?totalSales.split('Sales')[0]:'';
        ExactYear=estySince!=undefined?estySince.replace ( /[^\d.]/g, '' ):'';

        if (!shopname == '') {

          $(eleParent).find('.shop-name').attr('shopUrl',"https://www.etsy.com/shop/" + shopname);

          $(eleParent).find('.shop-name').text(shopname)
        } else {
          $(eleParent).find('.shop-name').text('');
        }

        $(eleParent).find('.shop-reviews').attr('shopUrl',"https://www.etsy.com/shop/"+shopname+"#reviews")

        if (!ratingcount == '') {
          $(eleParent).find('.shop-review-count-text').text('('+String(numberWithCommas(parseInt(ratingcount)))+')');
        }
        else {
          $(eleParent).find('.shop-review-count-text').text('');
        }

        if (!shopheadline == '') {
          $(eleParent).find('.shop-title').text(shopheadline);
        }
        else {
          $(eleParent).find('.shop-title').text('');
        }

        if (!rating == '') {
          $(eleParent).find('.ShopRating').html(getStars(rating));
        }
        else {
          $(eleParent).find('.ShopRating').html('');
        }

        if (!ExactYear == '') {
          $(eleParent).find('.shop-details-text.year').text('On Etsy since '+ExactYear);
        }
        else {
          $(eleParent).find('.shop-details-text.year').text('');
        }

        if (!shopregion == '') {
          $(eleParent).find('.shop-details-text').find('.ShopRegion').text(shopregion + ", ");
        }else{
          $(eleParent).find('.shop-details-text').find('.ShopRegion').text('');
        }

        if (!shopcity == '') {
          $(eleParent).find('.shop-details-text').find('.ShopCity').text(shopcity);
        }else{
          $(eleParent).find('.shop-details-text').find('.ShopCity').text('');
        }
        if (shopregion=='' && shopcity=='' ) {
          $('.location-icon').hide();
          $('.location-seperator').hide();

        }
        if (!shopsale == '') {

          $(eleParent).find('.shop-details-text.ShopSale').text(shopsale+' Sales');
        }
        else {

          $(eleParent).find('.shop-details-text.ShopSale').text('');
        }

        GetProductInfoDetail(eleParent, productObjDetail);
        resolve(true);

      },
      cache: true
    });

  });
}

function getStars(rating) {

    // Round to nearest half
    rating = Math.round(rating * 2) / 2;
    let output = [];

    // Append all the filled whole stars
    for (var i = rating; i >= 1; i--)
      output.push('<i class="fa fa-star" aria-hidden="true" style="color: black;"></i>&nbsp;');

    // If there is a half a star, append it
    if (i == .5) output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: black;"></i>&nbsp;');

    // Fill the empty stars
    for (let i = (5 - rating); i >= 1; i--)
      output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: black;"></i>&nbsp;');

    return output.join('');

  }
  function getProductReviewAsync(productUrl, type) {
    return new Promise((resolve) => {
      $.get( productUrl, function( data ) {
        var reviewObj={};
        if ($(data).find('button[id="same-listing-reviews-tab"]').length!=0) {
          var review=$(data).find('button[id="same-listing-reviews-tab"]').text().trim().replace ( /[^\d.]/g, '' );
          reviewObj['itemReview']=parseInt(review);
        } else {
          reviewObj['itemReview']=0;
        }
        var productPriceStr=$(data).find('h3.wt-text-title-03.wt-mr-xs-2').text().trim()
        reviewObj['productPrice']= productPriceStr.replace ( /[^\d.]/g, '' );
        reviewObj['shopReviewCount']= $(data).find('#shop-reviews-tab').text().trim().replace ( /[^\d.]/g, '' );

        if (type=='hover') {
          resolve(reviewObj);
        }
      });
    });
  }
  

  function getProductInfoAsync(listing_id,type) {
    return new Promise((resolve, reject) => {
      (async () => {
        var listingId = listing_id
        var etsy_user_credential_obj= await getUserSetting();
        var etsy_user_credential=etsy_user_credential_obj.etsy_user_credential;
        var etsy_access_token=etsy_user_credential['etsy_access_token'];
        var etsy_access_secret=etsy_user_credential['etsy_access_secret'];

       const token = {
         key: etsy_access_token,
         secret: etsy_access_secret,
       }
       sendMessage({"command": "callEtsyOpenApi","data":{token:token, listing_id:listingId }},function(apiResponse){
        if (type!=undefined && type=='hover') {
          resolve({apiResponse : apiResponse.result.results[0]});
        } else {
          resolve({apiResponse : apiResponse.result.results[0]});
        }
        console.log('apiResponse >> ', apiResponse);
      })
     })();
   });
  }
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  function copySingleTag(r) {
    var from = r;
    var ff = r.innerHTML;
    var range = document.createRange();
    window.getSelection().removeAllRanges();
    range.selectNode(from);
    window.getSelection().addRange(range);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
    //r.style.color = 'green';
    r.innerHTML = '<div style="display:flex;opacity:1;" class="listing-copied-state"><div class="copied-text">copied!</div></div>'+ff;
    setTimeout(function () {
      r.innerHTML = ff;
    }, 1500);

  };
  function sendMessage(msg, callbackfn) {
    if(callbackfn!=null) {
      callback.push(callbackfn);
      msg.callback = "yes";
    }
    chrome.runtime.sendMessage(msg,callbackfn);
  }

  const getUserSetting = async () => {
    return new Promise((resolve, reject) => {
      (async () => {
        sendMessage({"command": "getSettingsDB"},function(settings){
          var etsy_user_credential_obj=settings;
          resolve(etsy_user_credential_obj);
        })
      })();
    });
  };
  