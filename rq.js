// const { createRequire } = require('module');

const require = createRequire(import.meta.url);
const fetch = require('node-fetch');
const AbortController = require('node-abort-controller');
const axios = require('axios');
const config = require('./config/config.json');

const cookieParser = require('cookie-parser')


//const express = require('express');
// const { userController } = require('./Controllers/userController.js');
// const { favoriteController } = require('./Controllers/favoriteController.js');

import express from 'express';
import { userController } from './Controllers/userController.js';
import { favoriteController } from './Controllers/favoriteController.js';
import { folderController } from './Controllers/folderController.js';
import { stripeController } from './Controllers/stripeController.js';

import { createRequire } from 'module';


const CryptoJS = require("crypto-js");
const crypto = require('crypto')
const request = require('request');
const OAuth = require('oauth-1.0a')
var session = require('express-session');

function hash_function_sha1(base_string, key) {
    return crypto
        .createHmac('sha1', key)
        .update(base_string)
        .digest('base64')
}
 
const oauth = OAuth({
    consumer: { key: config.auth_app_key, secret: config.auth_app_secret },
    signature_method: 'HMAC-SHA1',
    hash_function: hash_function_sha1,
})

const path = require('path');

const __dirname = path.resolve();
 
const app = express();

// our default array of dreams
const dreams = [
  "Find and count some sheep",
  "Climb a really tall mountain",
  "Wash the dishes"
];


// make all the files in 'public' available
// https://expressjs.com/en/starter/static-files.html
app.use(express.static("public"));
app.use(express.json());
app.use(session({secret: "example secret"}));
app.use(cookieParser());

// https://expressjs.com/en/starter/basic-routing.html
app.get("/Subscribe", (request, response) => {
  response.sendFile(__dirname + "/views/index.html");
});

app.get("/LoginEtsy", (request, response) => {
    var request_data = {
        url: 'https://openapi.etsy.com/v2/oauth/request_token?scope=email_r',
        method: 'GET',
        data: { oauth_callback: "https://proud-luxurious-wandflower.glitch.me/etsyCallback"},
    }

    request_data.data = oauth.authorize(request_data)
    request(
        {
            url: request_data.url,
            method: request_data.method,
            form: request_data.data,
        },
        function(error, response, body) {
            console.log(body)
        }
    )
});
  

// send the default array of dreams to the webpage
app.get("/dreams", (request, response) => {
  // express helps us take JS objects and send them as JSON
  response.json(dreams);
});

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});

// import { DB } from './models/DB.js';
// let database = new DB();

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


// --- userController --- //

var userContr = new userController();

// app.post('/User/Register', async function(request, response) {
//   var result = await userContr.registration(request.body);
//   response.json(result);
// });

// app.get( entry, function( req, res ) {
//     o.getRequestToken( req, res );
// });

app.post('/User/Login', async function(request, response) {
  var result = await userContr.login(request.body);
  response.json(result);
});

app.post('/User/SaveTokens', async function(request, response) {
  var result = await userContr.saveTokens(request.body);
  response.json(result);
});


// --- favController --- //

var favController = new favoriteController();

app.post('/Favorite/GetFavoritesIdsFromServer', async function(request, response) {
  var result = await favController.displayFavorites(request.body);
  response.json(result);
});

app.post('/Favorite/SaveFavorite', async function(request, response) {
  var result = await favController.saveFavorite(request.body);
  response.json(result);
});

app.post('/Favorite/RemoveFavorite', async function(request, response) {
  var result = await favController.removeFavorite(request.body);
  response.json(result);
});

app.post('/Favorite/CheckFavorite', async function(request, response) {
  var result = await favController.checkFavorite(request.body);
  response.json(result);
});

app.post('/Favorite/GetAllFavListings', async function(request, response) {
  var result = await favController.getAllFavListings(request.body);
  response.json(result);
});


// --- folderController --- //

var folderContr = new folderController();

app.post('/Favorite/AddNewFolder', async function(request, response) {
  var result = await folderContr.addNewFolder(request.body);
  response.json(result);
});

app.post('/Favorite/CheckFolders', async function(request, response) {
  var result = await folderContr.checkFolders(request.body);
  response.json(result);
});

app.post('/Favorite/DeleteFavFolder', async function(request, response) {
  var result = await folderContr.deleteFavFolder(request.body);
  response.json(result);
});

app.post('/Favorite/AddListingsToFavFolder', async function(request, response) {
  var result = await folderContr.addListingsToFavFolder(request.body);
  response.json(result);
});

app.post('/Favorite/GetFolderListings', async function(request, response) {
  var result = await folderContr.getFolderListings(request.body);
  response.json(result);
});


// --- Stripe --- //

let stripe = new stripeController();
app.post('/stripe/checkoutPage', async (req, res, next) => {
    let result = await stripe.generateCheckoutPage(req.body)
    res.json(result)
})

app.post('/Stripe/CheckSubscription', async (req, res, next) => {
    let result = await stripe.checkSubscription(req.body)
    res.json(result)
})

app.post('/Stripe/CancelSubscription', async (req, res, next) => {
    let result = await stripe.cancelSubscription(req.body)
    res.json(result)
})


// --- HTTP Requests --- /


