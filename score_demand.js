var MY_SCORE = {

DEMANDPOINTS_SAlES:[
  { Criteria: 28, Points: 4.7},
  { Criteria: 22, Points: 4.23},
  { Criteria: 11, Points: 3.76},
  { Criteria: 8, Points: 3.29},
  { Criteria: 6, Points: 2.82},
  { Criteria: 4, Points: 2.35},
  { Criteria: 3, Points: 1.88},
  { Criteria: 2, Points: 1.41},
  { Criteria: 1, Points: 0.94},
  { Criteria: 0, Points: 0.47}
],
COMPETITIONPOINTS_SALES:[
  { Criteria: 0.0948, Points: 2.2},
  { Criteria: 0.0417, Points: 1.98},
  { Criteria: 0.0322, Points: 1.76},
  { Criteria: 0.0222, Points: 1.54},
  { Criteria: 0.0152, Points: 1.32},
  { Criteria: 0.0115, Points: 1.1},
  { Criteria: 0.0067, Points: 0.88},
  { Criteria: 0.0038, Points: 0.66},
  { Criteria: 0.0005, Points: 0.44},
  { Criteria: 0.0000, Points: 0.22}
  
],
COMPETITIONPOINTS_TOTALRESULTS:[
  { Criteria: 456, Points: 3.1},
  { Criteria: 1101, Points: 2.79},
  { Criteria: 2228, Points: 2.48},
  { Criteria: 3853, Points: 2.17},
  { Criteria: 5431, Points: 1.86},
  { Criteria: 9402, Points: 1.55},
  { Criteria: 17650, Points: 1.24},
  { Criteria: 47661, Points: 0.93},
  { Criteria: 75000, Points: 0.62},
  { Criteria: 100000, Points: 0.31}
 
]

}